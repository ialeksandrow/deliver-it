import decodeToken from 'jwt-decode';

const tokenDecoder = token => {
  try {
    const userData = decodeToken(token);

    return userData;
  } catch {
    localStorage.removeItem('token');

    return null;
  }
};

export default tokenDecoder;
