import { makeStyles } from '@material-ui/styles';
import { indigo, amber } from '@material-ui/core/colors';

const fieldsStyle = styles => makeStyles(styles);

const InputLabelStyle = fieldsStyle({
  root: {
    padding: '0 5px',
    fontSize: '0.875rem',
    backgroundColor: indigo[500],
    '&$focused': {
      color: amber[500]
    }
  },
  focused: {}
});

const InputLabelStyleLight = fieldsStyle({
  root: {
    padding: '0 5px',
    fontSize: '0.875rem',
    backgroundColor: '#FAFAFA',
    color: '#333333',
    '&$focused': {
      color: '#333333'
    }
  },
  focused: {}
});

const InputStyle = fieldsStyle({
  root: {
    color: '#FFFFFF',
    '& $notchedOutline': {
      borderWidth: '1px',
      borderColor: indigo[400]
    },
    '&:hover $notchedOutline': {
      borderWidth: '1px',
      borderColor: amber[500]
    },
    '&$focused $notchedOutline': {
      borderWidth: '1px',
      borderColor: amber[500]
    }
  },
  notchedOutline: {},
  focused: {}
});

const InputStyleLight = fieldsStyle({
  root: {
    '& $notchedOutline': {
      borderWidth: '1px',
      borderColor: '#DCDCDC'
    },
    '&:hover $notchedOutline': {
      borderWidth: '1px',
      borderColor: amber[500]
    },
    '&$focused $notchedOutline': {
      borderWidth: '1px',
      borderColor: amber[500]
    }
  },
  notchedOutline: {},
  focused: {}
});

const IconButtonStyle = fieldsStyle({
  root: {
    color: amber[500]
  }
});

const FormHelperTextStyle = fieldsStyle({
  root: {
    marginTop: '10px',
    fontSize: '0.875rem'
  }
});

const SelectStyle = fieldsStyle({
  icon: {
    fill: amber[500]
  }
});

const styles = {
  InputLabelStyle,
  InputLabelStyleLight,
  InputStyle,
  InputStyleLight,
  IconButtonStyle,
  FormHelperTextStyle,
  SelectStyle
};

export default styles;
