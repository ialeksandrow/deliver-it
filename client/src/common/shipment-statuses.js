const SHIPMENT_STATUSES = {
  1: 'Awaiting',
  2: 'Received',
  3: 'In Transit',
  4: 'Delivered'
};

export default SHIPMENT_STATUSES;
