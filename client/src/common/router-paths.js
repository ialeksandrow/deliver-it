const ROUTER_PATHS = {
  HOMEPAGE: '/',
  PARCELS: '/parcels',
  VIEW_PARCEL: '/parcels/details/:id',
  CREATE_PARCEL: '/parcels/create',
  SHIPMENTS: '/shipments',
  USER_PROFILE: '/user-profile/:id',
  USERS_LIST: '/users-list',
  SIGN_IN: '/sign-in',
  SIGN_UP: '/sign-up'
};

export default ROUTER_PATHS;
