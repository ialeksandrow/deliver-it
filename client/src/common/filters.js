export const filters = [
  { id:0, name: 'ID', key: 'parcelId', value: '', options: [] },
  { id:1, name: 'Name', key: 'parcelName', value: '', options: [] },
  { id:2, name: 'First Name', key: 'customerFirstName' , value: '', options: [] },
  { id:3, name: 'Last Name', key: 'customerLastName' , value: '', options: [] },
  { id:4, name: 'E-mail', key: 'customerEmail' , value: '', options: [] },
  { id:5, name: 'Weight', key: 'weight' , value: '', options: [] },
  { id:6, name: 'Status', key: 'status' , value: '', options: [ { id: '', text: '' }, { id: 1, text: 'Awaiting' }, { id: 2, text: 'Received' }] },
  { id:7, name: 'Delivery to', key: 'deliveryType' , value: '', options: [ { id: '', text: '' }, { id: 1, text: 'Address' }, { id: 2, text: 'Warehouse' }] },
];

export const userFilters = [
  { id:0, name: 'ID', key: 'parcelId', value: '', options: [] },
  { id:1, name: 'Name', key: 'parcelName', value: '', options: [] },
  { id:2, name: 'Weight', key: 'weight' , value: '', options: [] },
  { id:3, name: 'Status', key: 'status' , value: '', options: [ { id: '', text: '' }, { id: 1, text: 'Awaiting' }, { id: 2, text: 'Received' }] },
  { id:4, name: 'Delivery to', key: 'deliveryType' , value: '', options: [ { id: '', text: '' }, { id: 1, text: 'Address' }, { id: 2, text: 'Warehouse' }] },
]