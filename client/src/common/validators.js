export const userValidator = {
  email: (value) => !value || !RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/).test(value),
  firstName: (value) => !value || value.length < 2 || !RegExp(/^[a-zA-Z]*$/).test(value),
  lastName: (value) => !value || value.length < 2 || !RegExp(/^[a-zA-Z]*$/).test(value),
  address: (value) => !value || value.length < 2 || !RegExp(/^[A-Za-z0-9. ]*$/).test(value),
  password: (value) => !value || value.length < 6 || !RegExp(/^[a-z0-9_]+$/i).test(value),
  passwordConfirmation: (value) => !value || value.length < 6 || !RegExp(/^[a-z0-9_]+$/i).test(value),
  country: (value) => !value,
  cityId: (value) => !value
}