const NAVIGATION_LINKS = [
  { route: '/', name: 'Homepage' },
  { route: '/parcels', name: 'Parcels' },
  { route: '/shipments', name: 'Shipments' },
  { route: '/user-profile', name: 'User Profile' },
  { route: '/users-list', name: 'Users List' }
];

export default NAVIGATION_LINKS;
