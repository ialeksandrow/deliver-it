import { API_URL } from '../common/constants.js'
import { getHeaders } from './headers-functions.js';


export const getCountries = () => {
  return fetch(`${API_URL}global/countries`, {
    headers: getHeaders()
  })
  .then(x => x.json());
}

export const getCities = () => {
  return fetch(`${API_URL}global/cities`, {
    headers: getHeaders()
  })
  .then(x => x.json());
}

export const warehouses = () => {
  return fetch(`${API_URL}global/cities`, {
    headers: getHeaders()
  })
  .then(x => x.json());
}