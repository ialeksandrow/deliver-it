import { API_URL } from '../common/constants.js';
import { getAuthorization, getHeaders } from './headers-functions.js';

/**
 * Login user to system
 * @param { object } userData 
 * @returns { JSON }
 */
export const userLoginToSystem = (userData) => {
  return fetch(`${API_URL}users/login`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(userData),
  }).then(x => x.json());
}

export const registerUser = (userData) => {
  return fetch(`${API_URL}users/register`, {
    method: 'POST',
    body: userData
  }).then(x => x.json());
}

export const getUserById = (id) => {
  return fetch(`${API_URL}users/${id}`, {
    headers: getHeaders(),
  }).then(x => x.json());
}

export const getUserAvatar = (address) => {
  return fetch(address, {
    headers: getAuthorization(),
  }).then(x => x.blob());
}

export const updateUserInfo = (userId, data) => {
  return fetch(`${API_URL}users/${userId}/update`, {
    method: 'PUT',
    headers: getAuthorization(),
    body: data,
  }).then(x => x.json());
}


export const changeUserPassword = (userId, data) => {
  return fetch(`${API_URL}users/${userId}/password`, {
    method: 'PUT',
    headers: getHeaders(),
    body: JSON.stringify(data),
  }).then(x => x.json());
}


export const getCustomersCount = () => {
  return fetch(`${API_URL}users/customersCount`)
  .then(x => x.json());
}


export const searchInUsers = (params = `email=.`) => {
  return fetch(`${API_URL}users/search?${params}`, {
    headers: getHeaders()
  }).then(x => x.json());
}


export const searchByKeyWord = (keyWord) => {
  return fetch(`${API_URL}users/?keyWord=${keyWord}`, {
    headers: getHeaders()
  }).then(x => x.json());
}