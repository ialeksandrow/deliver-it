export const getHeaders = () => {
  return {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
  }
}


export const getAuthorization = () => {
  return {
    'Authorization': `Bearer ${localStorage.getItem('token')}`,
  }
}