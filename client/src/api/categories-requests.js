import { API_URL } from '../common/constants.js'
import { getHeaders } from './headers-functions.js';


export const getCategories = () => {
  return fetch(`${API_URL}categories`, {
    headers: getHeaders()
  })
  .then(x => x.json());
}


export const updateCategoryById = (id, body) => {
  return fetch(`${API_URL}categories/${id}`, {
    method: 'PUT',
    headers: getHeaders(),
    body: JSON.stringify(body),
  })
  .then(x => x.json());
}


export const addCategory = (body) => {
  return fetch(`${API_URL}categories`, {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify(body),
  })
  .then(x => x.json());
}


export const deleteCategoryById = (id) => {
  return fetch(`${API_URL}categories/${id}`, {
    method: 'DELETE',
    headers: getHeaders(),
  })
  .then(x => x.json());
}