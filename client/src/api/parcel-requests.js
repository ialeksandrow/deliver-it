import { API_URL } from '../common/constants.js'
import { getHeaders } from './headers-functions.js';


export const getNotices = () => {
  return fetch(`${API_URL}parcels/user/unseen`, {
    headers: getHeaders()
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
}

export const getParcels = (params = '') => {
  return fetch(`${API_URL}parcels/?${params}`, {
    headers: getHeaders()
  })
  .then(x => x.json());
}

export const getParcelById = (id) => {
  return fetch(`${API_URL}parcels/${id}`, {
    headers: getHeaders()
  })
  .then(x => x.json());
}

export const changeParcelStatus = (data) => {
  return fetch(`${API_URL}parcels/status/`, {
    method: 'PUT',
    headers: getHeaders(),
    body: JSON.stringify(data),
  })
  .then(x => x.json());
}

export const createParcel = (data) => {
  return fetch(`${API_URL}parcels`, {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify(data),
  })
  .then(x => x.json());
}

export const deleteParcel = (id) => {
  return fetch(`${API_URL}parcels/${id}`, {
    method: 'DELETE',
    headers: getHeaders(),
  })
  .then(x => x.json());
}

export const updateParcel = (id, data) => {
  return fetch(`${API_URL}parcels/${id}`, {
    method: 'PUT',
    headers: getHeaders(),
    body: JSON.stringify(data),
  })
  .then(x => x.json());
}

export const getUserParcels = (id) => {
  return fetch(`${API_URL}parcels/user/${id}`, {
    headers: getHeaders(),
  })
  .then(x => x.json());
}