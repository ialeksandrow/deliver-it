import { API_URL } from '../common/constants.js'
import { getHeaders } from './headers-functions.js';


export const getWarehouses = () => {
  return fetch(`${API_URL}warehouses`, {
    headers: getHeaders()
  })
  .then(x => x.json());
}