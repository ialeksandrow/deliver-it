import { createContext } from 'react';

const AuthenticationContext = createContext({
  accountState: null,
  setAccountState: () => {}
});

export default AuthenticationContext;
