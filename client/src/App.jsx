import Theme from './providers/Theme';
import { useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import AuthenticationContext from './contexts/AuthenticationContext';
import tokenDecoder from './utils/token-decoder';
import ROUTER_PATHS from './common/router-paths';
import Header from './components/Header/Header';
import Homepage from './views/Homepage/Homepage';
import Parcels from './views/Parcels/Parcels';
import ParcelDetails from './views/ParcelDetails/ParcelDetails';
import NewParcel from './views/NewParcel/NewParcel';
import Shipments from './views/Shipments/Shipments';
import UserProfile from './views/UserProfile/UserProfile';
import UsersList from './views/UsersList/UsersList';
import SignIn from './views/SignIn/SignIn';
import SignUp from './views/SignUp/SignUp';

const App = () => {
  const [accountState, setAccountState] = useState(
    tokenDecoder(localStorage.getItem('token'))
  );

  return (
    <Theme>
      <AuthenticationContext.Provider value={{ accountState, setAccountState }}>
        <Router>
          <Header />
          <Switch>
            <Route exact path={ROUTER_PATHS.HOMEPAGE} component={Homepage} />
            <Route exact path={ROUTER_PATHS.PARCELS} component={Parcels} />
            <Route
              exact
              path={ROUTER_PATHS.VIEW_PARCEL}
              component={ParcelDetails}
            />
            <Route
              exact
              path={ROUTER_PATHS.CREATE_PARCEL}
              component={NewParcel}
            />
            <Route exact path={ROUTER_PATHS.SHIPMENTS} component={Shipments} />
            <Route
              exact
              path={ROUTER_PATHS.USER_PROFILE}
              component={UserProfile}
            />
            <Route exact path={ROUTER_PATHS.USERS_LIST} component={UsersList} />
            <Route exact path={ROUTER_PATHS.SIGN_IN} component={SignIn} />
            <Route exact path={ROUTER_PATHS.SIGN_UP} component={SignUp} />
          </Switch>
        </Router>
      </AuthenticationContext.Provider>
    </Theme>
  );
};

export default App;
