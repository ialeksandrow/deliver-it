import '@fontsource/assistant';
import {
  unstable_createMuiStrictModeTheme as createTheme,
  ThemeProvider
} from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import indigo from '@material-ui/core/colors/indigo';
import amber from '@material-ui/core/colors/amber';

const theme = createTheme({
  typography: {
    fontFamily: 'Assistant'
  },
  palette: {
    primary: indigo,
    secondary: amber,
    text: {
      primary: '#333333',
      secondary: '#FFFFFF'
    }
  },
  overrides : {
    MuiTableSortLabel: {
      root: {
        '&$active': {
          color: null,
          '&& $icon': {
            opacity: 1,
            color: null,
          },
        },      
      }
    }
  }
});

const Theme = ({ children }) => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};

export default Theme;
