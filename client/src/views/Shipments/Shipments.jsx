import ShipmentsView from './../../components/ShipmentsView/ShipmentsView';
import { Container } from '@material-ui/core';

const Shipments = () => {
  return (
    <Container>
      <ShipmentsView />
    </Container>
  );
};

export default Shipments;
