import React from 'react';
import { useState, useEffect } from 'react';
import { useStyles } from './UsersList.styles';
import { searchByKeyWord, searchInUsers } from '../../api/user-requests';
import { Container, Typography } from '@material-ui/core';
import { getComparator, stableSort } from '../../common/sort-functions';
import { useHistory } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ArrowBack';
import TuneIcon from '@material-ui/icons/Tune';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import TextField from '@material-ui/core/TextField';
import Zoom from '@material-ui/core/Zoom';
import Button from '@material-ui/core/Button';
import ROUTER_PATHS from '../../common/router-paths';


const tableHeads = [
  { id: 'firstName', label: 'First Name', align: 'left', width: '15%' },
  { id: 'lastName', label: 'Last Name', align: 'left', width: '15%' },
  { id: 'email', label: 'E-mail', align: 'left', width: '25%' },
  { id: 'country', label: 'Country', align: 'left', width: '20%' },
  { id: 'city', label: 'City', align: 'left', width: '15%' },
  { id: 'role', label: 'Role', align: 'right', width: '10%' },
];

const filtersPrototype = {
  firstName: '',
  lastName: '',
  email: '',
  country: '',
  city: '',
  role: '',
}

const UsersList = () => {
  const styles = useStyles();
  const history = useHistory();

  const [orderBy, setOrderBy] = useState('firstName');
  const [order, setOrder] = useState('asc');
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [page, setPage] = useState(0);
  const [users, updateUsers] = useState([]);
  const [filters, updateFilters] = useState(filtersPrototype);
  const [showFilters, setShowFilters] = useState(false);
  const [keyWord, setKeyWord] = useState('');

  useEffect(() => {
    let queryString;

    if(Object.values(filters).every(x => x === '')) {
      queryString = `email=.`;
    } else {
      queryString = Object.keys(filters).map(x => `${x}=${filters[x]}`).join('&');
    }
    
    searchInUsers(queryString).then(x => updateUsers(x));
  }, [filters]);
  
  const changeData = (field, value) => updateFilters({...filters, [field]: value.trim()})

  const createSortHandler = (property) => (event) => requestSort(event, property);

  const checkForActiveFilter = () => Object.values(filters).some(x => x !== '');

  const createLink = (id) => ROUTER_PATHS.USER_PROFILE.replace(':id', id);

  const requestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const changePage = (event, newPage) => {
    setPage(newPage);
  };

  const ChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const keyWordSearch = () => {
    if (keyWord.trim()) {
      searchByKeyWord(keyWord).then(x => updateUsers(x));
      return;
    }

    searchInUsers().then(x => updateUsers(x));
  }

  return (
    <Container>
      <Paper variant='outlined' square elevation={5} className={styles.paperStyle}>
        <div className={styles.hLine}>
          <div className={styles.titleStyle}>
          <Typography variant='h4' style={{display:'flex'}}>Users list
              {
                !showFilters && checkForActiveFilter()
                ? (
                    <Tooltip
                    arrow
                    TransitionComponent={Zoom}
                    title='Have active filters'>
                      <AnnouncementIcon fontSize='small' color='primary' style={{marginLeft:5}}/>
                    </Tooltip>
                  )
                : null
              }
              </Typography>
          </div>
          <div className={styles.iconList}>
            <TextField
              className={styles.searchField}
              variant='outlined'
              size='small'
              placeholder='Search by keyword'
              inputProps={{ style:{paddingTop:0, paddingBottom: 0} }}
              value={keyWord}
              onChange={(e) => setKeyWord(e.target.value)}
              onKeyPress={(e) => e.key === 'Enter' ? keyWordSearch() : null}
            />
          <Tooltip title='Search' arrow TransitionComponent={Zoom}>
            <Button
            variant='contained'
            color='primary'
            className={styles.searchButton}
            endIcon={<SearchIcon className={styles.searchIcon}/>}
            onClick={keyWordSearch}
            >
            </Button>
          </Tooltip>
            <Tooltip title='Filters' arrow TransitionComponent={Zoom}>
              <IconButton onClick={() => setShowFilters(!showFilters)} ><TuneIcon color='primary'/></IconButton>
            </Tooltip>
            {
              checkForActiveFilter()
              ? (
                  <Tooltip title='Clear all filters' TransitionComponent={Zoom} arrow>
                    <IconButton onClick={() => updateFilters(filtersPrototype)} ><ClearIcon color='primary'/></IconButton>
                  </Tooltip>
                )
              : null
            }
            <Tooltip title='Back to previous page' arrow TransitionComponent={Zoom}>
              <IconButton onClick={() => history.goBack()} color='primary'>
                <BackIcon className={styles.primaryColorIcon} />
              </IconButton>
            </Tooltip>
          </div>
        </div>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                {
                  tableHeads.map(x => 
                    (
                      <TableCell key={x.id} align={x.align} width={x.width} >
                        <TableSortLabel
                          classes={{
                            root: styles.tableRoot,
                            active: styles.tableActive,
                            icon: styles.tableIcon,
                          }}
                          active={orderBy === x.id}
                          direction={orderBy === x.id ? order : 'asc'}
                          onClick={createSortHandler(x.id)}
                        >
                          {x.label}
                        </TableSortLabel>
                      </TableCell>
                    )
                  )
                }
              </TableRow>
              {
                !showFilters
                ? null
                : (
                    <TableRow>
                      {
                        Object.keys(filtersPrototype).map((field, index, array) => 
                        (                         
                          <TableCell key={field} className={styles.filterCell} align='left'>
                            <Input
                            fullWidth
                            name={field}
                            disableUnderline
                            value={filters[field]}
                            onChange={(e) => changeData(e.target.name, e.target.value)}
                            endAdornment = {
                              <InputAdornment position='end'>
                                <Tooltip title='Clear' arrow TransitionComponent={Zoom}>
                                  <IconButton style={{padding:2}} onClick={() => changeData(field, '')}>
                                    <ClearIcon color='primary' fontSize='small'/>
                                  </IconButton>
                                </Tooltip>
                                {index < array.length - 1 ? <span className={styles.rightBorder}></span> : null }
                              </InputAdornment>}
                            />
                          </TableCell>
                        ))
                      }                  
                    </TableRow>
                  )
              }
            </TableHead>
            <TableBody>
              {
                stableSort(users, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(user => (
                  <TableRow hover key={user.userId} className={styles.rowStyle} onClick={() => history.push(createLink(user.userId))}>
                    <TableCell align='left'>{user.firstName}</TableCell>
                    <TableCell align='left'>{user.lastName}</TableCell>
                    <TableCell align='left'>{user.email}</TableCell>
                    <TableCell align='left'>{user.country}</TableCell>
                    <TableCell align='left'>{user.city}</TableCell>
                    <TableCell align='right'>{user.role}</TableCell>
                  </TableRow>
                ))
              }
            </TableBody>
          </Table>
          <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component='div'
          count={users.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={changePage}
          onRowsPerPageChange={ChangeRowsPerPage}
          />
        </TableContainer>
      </Paper>
    </Container>
  )
}

export default UsersList;