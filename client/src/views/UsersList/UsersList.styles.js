import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
  paperStyle: {
    padding:20,
    marginTop: 20,
    // backgroundColor: theme.palette.secondary[50],
    borderRadius: 2,
    // border: `1px solid ${theme.palette.secondary.light}`
  },

  rowStyle: {
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: `${theme.palette.primary[50]} !important`
    }
  },
  
  iconList: {
    display: 'flex',
    justifyContent: 'flex-end',
  },

  hLine: {
    borderBottom: `1px solid ${theme.palette.primary.main}`,
    color: theme.palette.primary.main,
    paddingBottom: 5,
    marginBottom: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  
  tableRoot: {
    color: theme.palette.primary.main,
    fontWeight: 'bold',
    
    '&:hover': {
      color: theme.palette.primary.dark,
    },

    '&:focus': {
      color: theme.palette.primary.dark,
    },
  },

  tableActive: { 
    color: theme.palette.primary.dark,
  },

  tableIcon: {
    color: theme.palette.primary.main,
  },

  filterCell: {
    backgroundColor: theme.palette.secondary[100],
    paddingBottom: 0,
    paddingTop: 0,
    paddingLeft: 10,
    paddingRight: 0,
    borderBottom: '1px solid #e0e0e0',
  },

  rightBorder: {
    borderRight: `1px solid ${theme.palette.primary[300]}`,
    width: 1,
    height: 20,
  },

  searchField: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: 'white',
    '& fieldset': {
      borderTopRightRadius: 0,
      borderBottomRightRadius: 0,
      borderRight: 0,
    },
  },

  searchButton: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
    marginRight: 10,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    padding: 0,
    minWidth: 40,
    "& *": {
      margin: 0,
      padding: 0,
    }
  },

  searchIcon: {
    margin: 0,
    padding: 0,
  }

}));
