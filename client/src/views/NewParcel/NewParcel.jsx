import { Container } from '@material-ui/core';
import { useContext } from 'react';
import AuthenticationContext from '../../contexts/AuthenticationContext';
import CreateParcel from '../../components/CreateParcel/CreateParcel';


const NewParcel = () => {
  const { accountState } = useContext(AuthenticationContext);

  if (!accountState) {
    return <div>SORRY YOU ARE NOT LOGIN</div>
  }
  
  return (
    <Container>
      <CreateParcel/>
    </Container>
  );
};

export default NewParcel;
