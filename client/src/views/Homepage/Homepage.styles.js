import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles({
  sectionMap: {
    height: '100%',
    width: '60%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingTop: '60px'
  },
  mapBlock: {
    height: '500px',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  sectionFreightTypes: {
    padding: '60px 0'
  },
  sectionTrackShipment: {
    height: '500px',
    width: '60%',
    position: 'relative',
    left: '40%',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  }
});
