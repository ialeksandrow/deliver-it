import sectionOne from './../../images/sections/section-one.png';
import sectionTwo from './../../images/sections/section-two.png';
import { useState, useEffect } from 'react';
import { getCustomersCount } from './../../api/user-requests';
import SectionHalfImage from './../../components/SectionHalfImage/SectionHalfImage';
import WarehousesMap from './../../components/WarehousesMap/WarehousesMap';
import FreightTypes from './../../components/FreightTypes/FreightTypes';
import TrackShipment from './../../components/TrackShipment/TrackShipment';
import { Container, Typography } from '@material-ui/core';
import { useStyles } from './Homepage.styles';

const Homepage = () => {
  const [statistics, setStatistics] = useState(null);
  const styles = useStyles();

  useEffect(() => {
    getCustomersCount().then(res => {
      if (res.message) {
        return console.error(res.message);
      }

      return setStatistics(res);
    });
  }, []);

  return (
    <main>
      <SectionHalfImage
        options={{ image: sectionOne, position: 'right top', width: '547px' }}
      >
        <Container>
          <div className={styles.sectionMap}>
            <div>
              <Typography variant="h4" component="h1">
                We Ship Worldwide
              </Typography>
              <Typography variant="h6" component="h2">
                Transit time quicker than Elon Musk's space shuttles ;) <br />
                {statistics &&
                  `${statistics.totalCustomers} customers actively use our services on a daily basis in over ${statistics.totalCountries} different countries.`}
              </Typography>
            </div>
            <div className={styles.mapBlock}>
              <WarehousesMap />
            </div>
          </div>
        </Container>
      </SectionHalfImage>
      <div className={styles.sectionFreightTypes}>
        <FreightTypes />
      </div>
      <SectionHalfImage
        options={{ image: sectionTwo, position: 'left bottom', width: '673px' }}
      >
        <Container>
          <div className={styles.sectionTrackShipment}>
            <TrackShipment />
          </div>
        </Container>
      </SectionHalfImage>
    </main>
  );
};

export default Homepage;
