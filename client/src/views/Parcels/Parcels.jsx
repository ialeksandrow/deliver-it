import { Container } from '@material-ui/core';
import { useContext } from 'react';
import ParcelOperatorView from '../../components/ParcelOperatorView/ParcelOperatorView';
import AuthenticationContext from '../../contexts/AuthenticationContext';
import ParcelUserTable from '../../components/ParcelUserView/ParcelUserView';
import USER_ROLES from '../../common/user-roles';

const Parcels = () => {
  const { accountState } = useContext(AuthenticationContext);

  if (!accountState) {
    return <div>SORRY YOU ARE NOT LOGIN</div>
  }
  
  return (
    <Container>
      {
        accountState.role === USER_ROLES.operator
        ? <ParcelOperatorView />
        : <ParcelUserTable />
      }
    </Container>
  );
};

export default Parcels;
