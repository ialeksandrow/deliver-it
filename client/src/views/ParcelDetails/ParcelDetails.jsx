import { Container } from '@material-ui/core';
import { useContext } from 'react';
import ParcelView from '../../components/ParcelView/ParcelView';
import AuthenticationContext from '../../contexts/AuthenticationContext';


const ParcelDetails = (props) => {
  const { accountState } = useContext(AuthenticationContext);
  const userId = props.match.params.id;

  if (!accountState) {
    return <div>SORRY YOU ARE NOT LOGIN</div>
  }
  
  return (
    <Container>
      <ParcelView id={userId} />
    </Container>
  );
};

export default ParcelDetails;
