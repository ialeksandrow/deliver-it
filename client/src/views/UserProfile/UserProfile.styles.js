import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
  paperStyle: {
    padding:20,
    marginTop: 20,
    borderRadius: 2,
  },

  large: {
    width: theme.spacing(25),
    height: theme.spacing(25),
    border: `1px solid ${theme.palette.primary.main}`,
    marginBottom: 60,
  },

  buttonsHolder: {
    width: theme.spacing(25),
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },

  leftSide: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  }, 

  hLine: {
    borderBottom: `1px solid ${theme.palette.primary.main}`,
    color: theme.palette.primary.main,
    paddingBottom: 5,
    marginBottom: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  iconList: {
    display: 'flex',
    justifyContent: 'flex-end',
  },  

  listNested: {
    paddingLeft: theme.spacing(6),
  },

  iconWidth: {
    minWidth:35,
    color: theme.palette.primary.main,
  },

  helpText: {
    backgroundColor: theme.palette.secondary[50],
    marginLeft: 0,
    marginRight: 0,
    paddingLeft:10,
  },
  
  labelPrimary: {
    color: theme.palette.primary.main,
  },

  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },

  dialogTitle: {
    textAlign: 'center',
    color: 'white',
    backgroundColor: theme.palette.primary.main
  },

  dialogText: {
    color: 'black',
    padding: 30
  },
  
  editStyle: {
    backgroundColor: theme.palette.primary[50],
  },

  titleStyle: {
    display: 'flex',
  },

  snackbarError: {
    backgroundColor: 'red',
    fontWeight: 'bold'
  },

  snackbarSuccess: {
    backgroundColor: 'green',
    fontWeight: 'bold'
  },  
}));
