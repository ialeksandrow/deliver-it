import React from 'react';
import { useState, useEffect, useContext } from 'react';
import { Container, Typography } from '@material-ui/core';
import { useStyles } from './UserProfile.styles';
import Paper from '@material-ui/core/Paper';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import LabelImportantIcon from '@material-ui/icons/LabelImportant';
import WorkIcon from '@material-ui/icons/Work';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ArrowBack';
import EditIcon from '@material-ui/icons/Edit';
import Tooltip from '@material-ui/core/Tooltip';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import ErrorIcon from '@material-ui/icons/Error';
import { Link, useHistory } from 'react-router-dom';
import AuthenticationContext from '../../contexts/AuthenticationContext.js';
import { getUserAvatar, getUserById, updateUserInfo } from '../../api/user-requests';
import { getUserParcels } from '../../api/parcel-requests';
import { getCountries, getCities } from '../../api/global-requests.js';
import ROUTER_PATHS from '../../common/router-paths';
import USER_ROLES from '../../common/user-roles';
import { userValidator } from '../../common/validators';
import ChangePasswordDialog from '../../components/ChangePasswordDialog/ChangePasswordDialog';
import Snackbar from '@material-ui/core/Snackbar';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import InfoDialog from '../../components/InfoDialog/InfoDialog';



const errorMessages = {
  firstName: 'must include only latin symbols, and length must be min. 2 characters',
  lastName: 'must include only latin symbols, and length must be min. 2 characters',
  address: 'must include only latin symbols, numbers, dots and spaces, and length must be min. 2 characters'
}

const UserProfile = (props) => {
  const { id } = props.match.params;
  const { accountState, setAccountState } = useContext(AuthenticationContext);
  const operator = accountState.role === USER_ROLES.operator;
  const isMine = accountState.id === parseInt(id);

  const history = useHistory();
  const styles = useStyles();
  
  const [backupInfo, setBackupInfo] = useState({});
  const [avatar, setAvatar] = useState(null);
  const [countries, updateCountries] = useState([]);
  const [cities, updateCities] = useState([]);
  const [editMode, setEditMode] = useState(false);
  const [parcelsOpen, setParcelsOpen] = useState(true);
  const [userData, updateUserData] = useState(null);
  const [userParcels, updateUserParcels] = useState([]);
  const [permissionError, setPermissionError] = useState(false);
  const [passDialog, setPassDialog] = useState(false);
  const [successMessage, setSuccessMessage] = useState({open: false, type: 'success', message: ''});
  
  
  useEffect(()=> {
    getUserParcels(id).then(x => updateUserParcels(x.slice(0, 3)));
    getUserById(id).then(x => {
      updateUserData(x);
      if(x.avatarURL) {
        getUserAvatar(x.avatarURL)
        .then(resData => setAvatar(URL.createObjectURL(resData)));
      }
    });
    
    Promise.all([getCountries(), getCities()])
    .then(x => {
      updateCountries(x[0].data);
      updateCities(x[1].data);
    });
  }, [id]);

  const enterEditMode = () => {
    if (userData.roleId === USER_ROLES.operator && !isMine) {
      setPermissionError(true);
      return;
    }
    setBackupInfo({...userData});
    setEditMode(true);
  }

  const changeData = (field, value) => updateUserData({...userData, [field]: value});

  const changeCountry = (value) => {
    const defaultCity = (cities.filter(x => x.country === value))[0].cityId;
    updateUserData({...userData, country: value, cityId: defaultCity});
  }

  const updateUser = (avatar = []) => {
    setSuccessMessage({open: false, type: 'error', message: ''});
    const currentData = editMode && avatar.length ? backupInfo : userData;

    const data = new FormData();
    data.append('firstName', currentData.firstName);
    data.append('lastName', currentData.lastName);
    data.append('address', currentData.address);
    const checkFields = [...data.keys()].map(x => userValidator[x](currentData[x]));
    data.append('city', currentData.cityId);
    data.append('avatar', avatar[0]);

    if (checkFields.some(x => x)) {
      return;
    }

    updateUserInfo(id, data).then(x => {
      if (x.message) {
        setSuccessMessage({open: true, type: 'error', message: `Error: ${x.message} !`});
        return;
      }

      if (avatar.length) {
        getUserAvatar(x.avatarURL).then(resData => setAvatar(URL.createObjectURL(resData)));
        
        if (isMine) {
          setAccountState({...accountState, avatar: x.avatarURL})
        }
        return;
      }

      if (isMine) {
        setAccountState({ ...accountState, firstName: x.firstName, lastName: x.lastName });
      }
      setEditMode(false);
      setSuccessMessage({open: true, type: 'success', message: 'Data was updated successful!'});
      updateUserData(x);
    });
  }

  const cancelChanges = () => {
    updateUserData({...backupInfo});
    setEditMode(false);
  }

  if (!operator && !isMine) {
    return (
      <InfoDialog title='ERROR' message='ACCESS DENIED' buttonFunc={() => history.goBack()}/>
    )
  }

  if (!userData || !userParcels || !cities.length || !countries.length) {
    return (
      <Backdrop className={styles.backdrop} open={true}>
        <div style={{position: 'relative'}}>LOADING</div>
        <CircularProgress style={{position: 'absolute'}} color='primary' size={100}/>
      </Backdrop>
    )
  }

  return (
    <Container>
      <Paper variant='outlined' square elevation={5} className={styles.paperStyle}>
      <div className={styles.hLine}>
        <div className={styles.titleStyle}>
          <Typography variant='h4' >
            {
              editMode
              ? `${backupInfo.firstName} ${backupInfo.lastName}`
              : `${userData.firstName} ${userData.lastName}`
            }
          </Typography>&nbsp;
          {editMode ? ' (edit mode)' : null}
        </div>
        <div className={styles.iconList}>
          {
            editMode
            ? (<>
                <Tooltip title='Confirm changes' arrow>
                  <IconButton onClick={updateUser} color='primary'>
                    <DoneIcon className={styles.primaryColorIcon} />
                  </IconButton>
                </Tooltip>
                <Tooltip title='Cancel changes' arrow>
                  <IconButton onClick={cancelChanges} color='primary'>
                    <CloseIcon className={styles.primaryColorIcon} />
                  </IconButton>
                </Tooltip>
              </>
              )
            : (<>
                <Tooltip title='Back to previous page' arrow>
                  <IconButton onClick={() => history.goBack()} color='primary'>
                    <BackIcon className={styles.primaryColorIcon} />
                  </IconButton>
                </Tooltip>
                <Tooltip title='Edit mode' arrow>
                  <IconButton onClick={enterEditMode}  color='primary'>
                    <EditIcon className={styles.primaryColorIcon} />
                  </IconButton>
                </Tooltip>
              </>
              )
          }
        </div>
      </div>
        <Grid container spacing={3}>
          <Grid item xs={3} className={styles.leftSide}>
            <Avatar className={styles.large} variant='rounded' src={avatar}></Avatar>
            <Container className={styles.buttonsHolder}>
              <input accept="image/*" id='avatarFile' multiple={false} type="file" style={{display: 'none'}}
                onChange={(e) => updateUser(e.target.files)} />
              <label htmlFor='avatarFile'>
              {
                userData.roleId === USER_ROLES.operator && !isMine
                ? null
                : <Button variant="contained" color="primary" component="span" fullWidth>Change Avatar</Button>
              }
                
              </label>
              {
                isMine
                ? <Button onClick={() => setPassDialog(!passDialog)} variant='contained' color='primary' style={{marginTop:10}}>Change Password</Button>
                : null
              }
            </Container>
          </Grid>
          <Grid item xs={5} >
            <Grid container spacing={2}>
              <Grid item xs={12} >
                <TextField
                  fullWidth
                  label='E-mail'
                  InputProps={{ readOnly: true }}
                  InputLabelProps={{ className: styles.labelPrimary }}
                  variant='outlined'
                  value={userData.email}
                />
              </Grid>
              <Grid item xs={12} >
                <TextField
                  className={editMode ? styles.editStyle : null}
                  fullWidth
                  label='First Name'
                  InputProps={{ readOnly: !editMode }}
                  InputLabelProps={{ className: styles.labelPrimary }}
                  FormHelperTextProps={{ className: styles.helpText }}
                  variant='outlined'
                  name='firstName'
                  value={userData.firstName}
                  onChange={(e) => changeData(e.target.name, e.target.value)}
                  error={ userValidator.firstName(userData.firstName) }
                  helperText={ userValidator.firstName(userData.firstName) ? errorMessages.firstName : null }
                />
              </Grid>
              <Grid item xs={12} >
                <TextField
                  className={editMode ? styles.editStyle : null}
                  fullWidth
                  label='Last Name'
                  InputProps={{ readOnly: !editMode }}
                  InputLabelProps={{ className: styles.labelPrimary }}
                  FormHelperTextProps={{ className: styles.helpText }}
                  variant='outlined'
                  name='lastName'
                  value={userData.lastName}
                  onChange={(e) => changeData(e.target.name, e.target.value)}
                  error={ userValidator.lastName(userData.lastName) }
                  helperText={ userValidator.lastName(userData.lastName) ? errorMessages.lastName : null }
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                {
                  editMode
                  ? (
                      <FormControl variant="outlined" fullWidth>
                        <InputLabel className={styles.labelPrimary}>Country</InputLabel>
                        <Select
                          native
                          className={styles.editStyle}
                          label='Country'
                          name='country'
                          value={userData.country}
                          onChange={(e) => changeCountry(e.target.value)}
                        >
                          { countries.map(el => <option key={el.name} value={el.name}>{el.name}</option>) }
                        </Select>
                      </FormControl>
                    )
                  : (
                      <TextField
                        fullWidth
                        label='Country'
                        InputProps={{ readOnly: true }}
                        InputLabelProps={{ className: styles.labelPrimary }}
                        variant='outlined'
                        value={userData.country}
                      />
                    )
                }
              </Grid>
              <Grid item xs={12} sm={6}>
                {
                  editMode
                  ? (
                      <FormControl variant="outlined" fullWidth>
                        <InputLabel className={styles.labelPrimary}>City</InputLabel>
                        <Select
                          native
                          className={styles.editStyle}
                          readOnly
                          label='City'
                          value={userData.cityId}
                          name='cityId'
                          onChange={(e) => changeData(e.target.name, e.target.value)}
                        >
                          {
                            cities.filter(x => x.country === userData.country)
                            .map(el => <option key={el.cityId} value={el.cityId}>{el.name}</option>)
                          }
                        </Select>
                      </FormControl>
                    )
                  : (
                      <TextField
                        fullWidth
                        label='City'
                        InputProps={{ readOnly: true }}
                        InputLabelProps={{ className: styles.labelPrimary }}
                        variant='outlined'
                        value={userData.city}
                      />
                    )
              }
              </Grid>
              <Grid item xs={12} >
                <TextField
                  className={editMode ? styles.editStyle : null}
                  fullWidth
                  label='Address'
                  InputProps={{ readOnly: !editMode }}
                  InputLabelProps={{ className: styles.labelPrimary }}
                  FormHelperTextProps={{ className: styles.helpText }}
                  variant='outlined'
                  value={userData.address}
                  name='address'
                  onChange={(e) => changeData(e.target.name, e.target.value)}
                  error={ userValidator.address(userData.address) }
                  helperText={ userValidator.address(userData.address) ? errorMessages.address : null }
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={4} >
            <List component="nav" style={{padding:0}}>
              <ListItem>
                <ListItemIcon className={styles.iconWidth}><AccountBoxIcon /></ListItemIcon>
                <ListItemText primary={`Role: ${userData.role}`} />
              </ListItem>
              <ListItem>
                <ListItemIcon className={styles.iconWidth}><WorkIcon /></ListItemIcon>
                <ListItemText primary={`Total Parcels: ${userData.associatedParcels}`} />
              </ListItem>
              <ListItem>
                <ListItemIcon className={styles.iconWidth}><LocalShippingIcon /></ListItemIcon>
                <ListItemText primary={`Shipments done: ${userData.associatedShipments}`} />
              </ListItem>              
              <ListItem button onClick={() => setParcelsOpen(!parcelsOpen)}>
                <ListItemIcon className={styles.iconWidth}>
                  <AccountTreeIcon />
                </ListItemIcon>
                <ListItemText primary="Last parcels" />
                {parcelsOpen ? <ExpandLess /> : <ExpandMore />}
              </ListItem>
              <Collapse in={parcelsOpen} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  { 
                    userParcels.map(x => (
                      <ListItem key={x.parcelId} className={styles.listNested}>
                        <ListItemIcon className={styles.iconWidth}><LabelImportantIcon /></ListItemIcon>
                        <Link
                        style={{textDecoration: 'none', color: 'inherit'}}
                        to={ROUTER_PATHS.VIEW_PARCEL.replace(':id', x.parcelId)}
                        >
                          {x.parcelName}&nbsp;({x.parcelStatus})
                        </Link>
                      </ListItem>))
                  }
                </List>
              </Collapse>
            </List>
          </Grid>
        </Grid>
      </Paper>
      <ChangePasswordDialog open={passDialog} viewFunc={setPassDialog} id={id} successMessage={setSuccessMessage}/>
      <Snackbar
        open={successMessage.open}
        onClose={() => setSuccessMessage({...successMessage, open: false})}
        autoHideDuration={5000}
        message={successMessage.message}
        ContentProps={{
          className: successMessage.type === 'error' ? styles.snackbarError : styles.snackbarSuccess
        }}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        action={successMessage.type === 'error' ? <ErrorIcon/> : <CheckCircleIcon />}
      />
      {
        permissionError
        ? <InfoDialog title='ERROR' message='YOU CANNOT CHANGE OTHER OPERATOR PERSONAL DATA' buttonFunc={() => setPermissionError(false)}/>
        : null
      }
    </Container>
  );
};

export default UserProfile;
