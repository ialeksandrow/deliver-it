import { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import AuthenticationContext from './../../contexts/AuthenticationContext';
import ROUTER_PATHS from './../../common/router-paths';
import tokenDecoder from './../../utils/token-decoder';
import { userLoginToSystem } from './../../api/user-requests.js';
import AuthenticationBlock from './../../components/AuthenticationBlock/AuthenticationBlock';
import {
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  IconButton,
  FormHelperText,
  Button
} from '@material-ui/core';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { useStyles } from './SignIn.styles';
import classes from './../../utils/use-design.styles';
import { userValidator } from '../../common/validators';

const SignIn = () => {
  const { setAccountState } = useContext(AuthenticationContext);
  const history = useHistory();
  const styles = useStyles();

  const [userEmail, setUserEmail] = useState({ value: '', error: false });
  const [userPassword, setUserPassword] = useState({ value: '', error: false });
  const [showPassword, setShowPassword] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  // Form Fields Styles
  const InputLabelStyle = classes.InputLabelStyle();
  const InputStyle = classes.InputStyle();
  const FormHelperTextStyle = classes.FormHelperTextStyle();
  const IconButtonStyle = classes.IconButtonStyle();
  
  const userLogin = () => {
    const checkError = {
      email: userValidator.email(userEmail.value),
      password: userValidator.password(userPassword.value),
    }

    setUserEmail({ ...userEmail, error: checkError.email });
    setUserPassword({ ...userPassword, error: checkError.password });
    
    if (checkError.email) {
      setErrorMessage('INVALID E-MAIL');
      return
    }
    
    if (checkError.password) {
      setErrorMessage('INVALID CREDENTIALS')
      return;
    }

    const userData = {
      email: userEmail.value,
      password: userPassword.value
    };

    userLoginToSystem(userData).then(result => {
      if (result.message) {
        setErrorMessage('INVALID CREDENTIALS');
        setUserEmail({ ...userEmail, error: true });
        setUserPassword({ ...userPassword, error: true });
        return;
      }

      setErrorMessage(null);
      const token = tokenDecoder(result.token);
      setAccountState(token);
      localStorage.setItem('token', result.token);
      history.push(ROUTER_PATHS.HOMEPAGE);
    });
  };

  return (
    <AuthenticationBlock>
      {errorMessage ? (
        <div className={styles.errorMessage}>{errorMessage}</div>
      ) : null}
      <form noValidate autoComplete="off" className={styles.formBlock}>
        <FormControl variant="outlined">
          <InputLabel htmlFor="email" classes={InputLabelStyle}>
            Email
          </InputLabel>
          <OutlinedInput
            type="text"
            id="email"
            classes={InputStyle}
            value={userEmail.value}
            error={userEmail.error}
            onChange={e =>
              setUserEmail({ value: e.target.value, error: userValidator.email(e.target.value) })
            }
          />
          <FormHelperText id="email" classes={FormHelperTextStyle}>
            Demo Login: <br />
            operator_demo@mail.com <br />
            customer_demo@mail.com
          </FormHelperText>
        </FormControl>
        <FormControl variant="outlined">
          <InputLabel htmlFor="password" classes={InputLabelStyle}>
            Password
          </InputLabel>
          <OutlinedInput
            type={showPassword ? 'text' : 'password'}
            id="password"
            classes={InputStyle}
            value={userPassword.value}
            error={userPassword.error}
            onChange={e =>
              setUserPassword({ value: e.target.value, error: userValidator.password(e.target.value) })
            }
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  classes={IconButtonStyle}
                  onClick={() => setShowPassword(!showPassword)}
                >
                  {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                </IconButton>
              </InputAdornment>
            }
          />
          <FormHelperText id="password" classes={FormHelperTextStyle}>
            Demo Password: <br />
            Operator123 <br />
            Customer123
          </FormHelperText>
        </FormControl>
      </form>
      <Button variant="contained" color="secondary" onClick={userLogin}>
        Log In
      </Button>
    </AuthenticationBlock>
  );
};

export default SignIn;
