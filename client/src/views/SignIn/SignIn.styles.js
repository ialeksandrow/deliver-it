import { makeStyles } from '@material-ui/styles';
import { red } from '@material-ui/core/colors';

export const useStyles = makeStyles({
  formBlock: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    '& > *': {
      flexBasis: '49%'
    }
  },
  errorMessage: {
    fontSize: '0.875rem',
    color: red[500]
  }
});
