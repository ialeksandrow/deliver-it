import { makeStyles } from '@material-ui/styles';
import { red } from '@material-ui/core/colors';

export const useStyles = makeStyles(theme => ({
  formBlock: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    '& > *': {
      flexBasis: '49%',
      marginBottom: '2%'
    }
  },

  errorMessage: {
    fontSize: '0.875rem',
    color: red[500]
  },

  dialogTitle: {
    textAlign: 'center',
    color: 'white',
    backgroundColor: theme.palette.primary.main
  },

  dialogText: {
    color: 'black',
    padding: 30
  },

}));
