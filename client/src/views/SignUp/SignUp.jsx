import { useState, useEffect, forwardRef, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import AuthenticationBlock from './../../components/AuthenticationBlock/AuthenticationBlock';
import {
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  IconButton,
  Select,
  MenuItem,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Slide
} from '@material-ui/core';
import { registerUser, userLoginToSystem } from '../../api/user-requests';
import { useStyles } from './SignUp.styles';
import { getCities, getCountries } from '../../api/global-requests';
import AuthenticationContext from './../../contexts/AuthenticationContext';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import classes from './../../utils/use-design.styles';
import Backdrop from '@material-ui/core/Backdrop';
import tokenDecoder from './../../utils/token-decoder';
import CircularProgress from '@material-ui/core/CircularProgress';
import ROUTER_PATHS from './../../common/router-paths';
import { userValidator } from '../../common/validators.js';


const fields = {}
const errors = {}
Object.keys(userValidator).forEach(x => fields[x] = '');
Object.keys(userValidator).forEach(x => errors[x] = false);

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const SignUp = () => {
  const styles = useStyles();
  const history = useHistory();

  const [showPassword, setShowPassword] = useState(false);
  const [countries, updateCountries] = useState([]);
  const [cities, updateCities] = useState([]);
  const [userData, updateUserData] = useState(fields);
  const [errorData, updateErrorData] = useState(errors);
  const [showDialog, updateShowDialog] = useState(false);
  const [successRegister, setSuccessRegister] = useState(false);
  const [dialogData, setDialogData] = useState({ title: 'SUCCESS', message: 'none' });
  
  const { setAccountState } = useContext(AuthenticationContext);

  useEffect(() => {
    getCountries().then(x => updateCountries(x.data));
    getCities().then(x => updateCities(x.data));
  }, []);

  const changeData = (field, data) => {
    updateUserData({...userData, [field]: data});
    updateErrorData({...errorData, [field]: userValidator[field](data)});
  };

  const changeCountry = (value) => {
    const defaultCity = (cities.filter(x => x.country === value))[0].cityId;
    updateUserData({...userData, country: value, cityId: defaultCity});
    updateErrorData({...errorData, country: userValidator.country(value), cityId: userValidator.cityId(defaultCity)});
  }

  const validateForm = (event) => {
    event.preventDefault();
    const data = new FormData(event.target)
    
    const checkObject = {}
    data.forEach((value, key) => data.forEach((value, key) => checkObject[key] = userValidator[key](value)));
    updateErrorData(checkObject);
    
    if (Object.values(checkObject).some(x => x)) {
      updateShowDialog(true);
      setDialogData({ title: 'ERROR', message: 'Have invalid field(s). Please check form and try again!' })
      return;
    }
    
    data.append('city', data.get('cityId'));
    createNewUser(data);
  }

  const createNewUser = (data) => {
    registerUser(data).then(x => {
      if (x.message) {
        updateShowDialog(true);
        setDialogData({ title: 'ERROR', message: x.message });
        return;
      }
        updateShowDialog(true);
        setSuccessRegister(true);
        setDialogData({ title: 'SUCCESS', message: 'Your account was successful created' });
    });
  }

  const logToSystem = () => {
    userLoginToSystem(userData).then(result => {
      if (result.message) {
        return;
      }    
      const token = tokenDecoder(result.token);
      setAccountState(token);
      localStorage.setItem('token', result.token);
      history.push(ROUTER_PATHS.HOMEPAGE);
    });
  }

  // Form Fields Styles
  const InputLabelStyle = classes.InputLabelStyle();
  const InputStyle = classes.InputStyle();
  const IconButtonStyle = classes.IconButtonStyle();
  const SelectStyle = classes.SelectStyle();

  if (!countries.length || !cities.length) {
    return (
      <Backdrop className={styles.backdrop} open={true}>
        <CircularProgress size={100} color="primary" />
      </Backdrop>
    )
  }

  return (
    <AuthenticationBlock minHeight="640px">
      <form noValidate autoComplete="off" className={styles.formBlock} id='registerForm' onSubmit={validateForm}>
        <FormControl variant="outlined">
          <InputLabel htmlFor="email" classes={InputLabelStyle}>
            Email
          </InputLabel>
          <OutlinedInput
            type="text"
            name='email'
            classes={InputStyle}
            value={userData.email}
            error={errorData.email}
            onChange={(e) => changeData(e.target.name, e.target.value)}
          />
        </FormControl>
        <FormControl variant="outlined">
          <InputLabel htmlFor="address" classes={InputLabelStyle}>
            Address
          </InputLabel>
          <OutlinedInput
            type="text"
            name="address"
            classes={InputStyle}
            placeholder='Latin symbols, dots, numbers and spaces'
            value={userData.address}
            error={errorData.address}
            onChange={(e) => changeData(e.target.name, e.target.value)}
          />
        </FormControl>
        <FormControl variant="outlined">
          <InputLabel htmlFor="firstName" classes={InputLabelStyle}>
            First Name
          </InputLabel>
          <OutlinedInput
            type="text"
            name="firstName"
            placeholder='Latin symbols only'
            classes={InputStyle}
            value={userData.firstName}
            error={errorData.firstName}
            onChange={(e) => changeData(e.target.name, e.target.value)}
          />
        </FormControl>
        <FormControl variant="outlined">
          <InputLabel htmlFor="lastName" classes={InputLabelStyle}>
            Last Name
          </InputLabel>
          <OutlinedInput
            type="text"
            name="lastName"
            placeholder='Latin symbols only'
            classes={InputStyle}
            value={userData.lastName}
            error={errorData.lastName}
            onChange={(e) => changeData(e.target.name, e.target.value)}
          />
        </FormControl>
        <FormControl variant="outlined">
          <InputLabel htmlFor="password" classes={InputLabelStyle}>
            Password
          </InputLabel>
          <OutlinedInput
            type={showPassword ? 'text' : 'password'}
            name="password"
            classes={InputStyle}
            value={userData.password}
            error={errorData.password}
            onChange={(e) => changeData(e.target.name, e.target.value)}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  classes={IconButtonStyle}
                  onClick={() => setShowPassword(!showPassword)}
                >
                  {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
        <FormControl variant="outlined">
          <InputLabel htmlFor="passwordConfirmation" classes={InputLabelStyle}>
            Confirm Password
          </InputLabel>
          <OutlinedInput
            type={showPassword ? 'text' : 'password'}
            name="passwordConfirmation"
            classes={InputStyle}
            value={userData.passwordConfirmation}
            error={errorData.passwordConfirmation || userData.password !== userData.passwordConfirmation}
            onChange={(e) => changeData(e.target.name, e.target.value)}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  classes={IconButtonStyle}
                  onClick={() => setShowPassword(!showPassword)}
                >
                  {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
        <FormControl variant="outlined">
          <InputLabel htmlFor="country" classes={InputLabelStyle}>
            Country
          </InputLabel>
          <Select
            labelId="country"
            name="country"
            input={<OutlinedInput classes={InputStyle} />}
            classes={SelectStyle}
            value={userData.country}
            error={errorData.country}
            onChange={(e) => changeCountry(e.target.value)}
          >
            { countries.map(x => <MenuItem key={x.name} value={x.name}>{x.name}</MenuItem>) }            
          </Select>
        </FormControl>
        <FormControl variant="outlined">
          <InputLabel htmlFor="city" classes={InputLabelStyle}>
            City
          </InputLabel>
          <Select
            labelId="city"
            name="cityId"
            input={<OutlinedInput classes={InputStyle} />}
            classes={SelectStyle}
            value={userData.cityId}
            error={errorData.cityId}
            onChange={(e) => changeData(e.target.name, e.target.value)}
          >
            { cities.filter(s => s.country === userData.country).map(x => <MenuItem key={x.cityId} value={x.cityId}>{x.name}</MenuItem>) }
          </Select>
        </FormControl>
      </form>
      <Button variant="contained" color="secondary" type='submit' form='registerForm'>
        Create New Account
      </Button>

      <Dialog open={showDialog} TransitionComponent={Transition} keepMounted>
        <DialogTitle className={styles.dialogTitle}>{dialogData.title}</DialogTitle>
        <DialogContent>
          <DialogContentText className={styles.dialogText}>{dialogData.message}</DialogContentText>
        </DialogContent>
        <DialogActions style={{justifyContent:'center'}}>
          <Button
            color="primary"
            variant='contained'
            onClick={successRegister ? () => logToSystem() : () => updateShowDialog(false)}
            >
              {successRegister ? 'continue' : 'ok'}
            </Button>
        </DialogActions>
      </Dialog>
    </AuthenticationBlock>
  );
};

export default SignUp;
