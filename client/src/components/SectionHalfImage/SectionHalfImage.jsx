import { useStyles } from './SectionHalfImage.styles';

const SectionHalfImage = ({ options, children }) => {
  const styles = useStyles(options);

  return <div className={styles.sectionHalfImage}>{children}</div>;
};

export default SectionHalfImage;
