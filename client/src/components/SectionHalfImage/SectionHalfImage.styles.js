import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles({
  sectionHalfImage: {
    height: '100%',
    width: '100%',
    backgroundImage: options => `url(${options.image})`,
    backgroundPosition: options => options.position,
    backgroundSize: options => `${options.width} 500px`,
    backgroundRepeat: 'no-repeat'
  }
});
