import { forwardRef } from 'react';
import { useStyles } from './InfoDialog.styles';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Slide from '@material-ui/core/Slide';


const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const InfoDialog = (props) => {
  const { title, message, buttonFunc } = props;
  const styles = useStyles();

  return (
    <Dialog open={true} TransitionComponent={Transition} keepMounted>
      <DialogTitle className={styles.dialogTitle}>{title}</DialogTitle>
      <DialogContent>
        <DialogContentText className={styles.dialogText}>{message}</DialogContentText>
      </DialogContent>
      <DialogActions style={{justifyContent:'center'}}>
        <Button onClick={buttonFunc} variant='contained' color='primary'>OK</Button>
      </DialogActions>
    </Dialog>
  )
}

InfoDialog.propTypes = {
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  buttonFunc: PropTypes.func.isRequired,
};

export default InfoDialog;