import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
  dialogTitle: {
    textAlign: 'center',
    color: 'white',
    backgroundColor: theme.palette.primary.main
  },

  dialogText: {
    color: 'black',
    padding: 30
  },
  
}));
