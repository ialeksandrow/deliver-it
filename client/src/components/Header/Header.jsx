import { useContext, useState, useEffect } from 'react';
import { useLocation, useHistory } from 'react-router';
import AuthenticationContext from './../../contexts/AuthenticationContext';
import ROUTER_PATHS from './../../common/router-paths';
import NAVIGATION_LINKS from './../../common/navigation-links';
import USER_ROLES from './../../common/user-roles';
import { UPDATE_TIME } from './../../common/constants';
import { getNotices } from './../../api/parcel-requests';
import Brand from './../Brand/Brand';
import HeaderSearch from './../HeaderSearch/HeaderSearch';
import Navigation from './../Navigation/Navigation';
import NavigationLink from './../Navigation/NavigationLink';
import UserAvatar from './../UserAvatar/UserAvatar';
import LogOutButton from './../LogOutButton/LogOutButton';
import {
  AppBar,
  Container,
  Toolbar,
  IconButton,
  Badge,
  Menu,
  MenuItem,
  Popover,
  Typography
} from '@material-ui/core';
import NotificationsIcon from '@material-ui/icons/Notifications';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { useStyles } from './Header.styles';

const Header = () => {
  const { accountState, setAccountState } = useContext(AuthenticationContext);
  const [anchorElement, setAnchorElement] = useState(null);
  const [notes, updateNotes] = useState(0);

  const styles = useStyles();
  const { pathname } = useLocation();
  const history = useHistory();

  const open = Boolean(anchorElement);

  const routerPathsWithOmittedHeader = Object.values(ROUTER_PATHS).filter(
    path => path === ROUTER_PATHS.SIGN_IN || path === ROUTER_PATHS.SIGN_UP
  );

  const getMessages = () => {
    getNotices().then(res => {
      if (res.message) {
        return console.error(res.message);
      }

      return updateNotes(res.unSeenParcels);
    });
  };

  useEffect(() => {
    if (accountState && accountState.role === USER_ROLES.customer) {
      getMessages();

      const checkForMessages = setInterval(() => getMessages(), UPDATE_TIME);

      return () => clearInterval(checkForMessages);
    }
  }, [accountState]);

  return (
    <>
      {!routerPathsWithOmittedHeader.includes(pathname) && (
        <>
          <AppBar elevation={3} color="primary" className={styles.toolbar}>
            <Container>
              <Toolbar disableGutters={true} className={styles.ToolbarBlock}>
                <Brand />
                {accountState ? (
                  <>
                    <HeaderSearch />
                    <Navigation>
                      {NAVIGATION_LINKS.map(link => {
                        if (link.name === 'User Profile') {
                          return (
                            <NavigationLink
                              key={`${link.route.slice(1)}/${accountState.id}`}
                              route={`${link.route}/${accountState.id}`}
                              name={link.name}
                              active={link.route === pathname}
                            />
                          );
                        }

                        if (accountState.role === USER_ROLES.customer) {
                          if (link.name === 'Shipments') {
                            return null;
                          }
                        }

                        if (accountState.role === USER_ROLES.customer) {
                          if (link.name === 'Users List') {
                            return null;
                          }
                        }

                        return (
                          <NavigationLink
                            key={link.route.slice(1)}
                            route={link.route}
                            name={link.name}
                            active={link.route === pathname}
                          />
                        );
                      })}
                    </Navigation>
                    {accountState.role === USER_ROLES.customer && (
                      <IconButton
                        onMouseEnter={e => setAnchorElement(e.currentTarget)}
                        onMouseLeave={() => setAnchorElement(null)}
                        onClick={() => history.push(ROUTER_PATHS.PARCELS)}
                        color="inherit"
                      >
                        <Badge badgeContent={notes} color="secondary">
                          <NotificationsIcon />
                        </Badge>
                      </IconButton>
                    )}
                    {notes ? (
                      <Popover
                        className={styles.popover}
                        classes={{ paper: styles.popoverPaper }}
                        open={open}
                        anchorEl={anchorElement}
                        anchorOrigin={{
                          vertical: 'bottom',
                          horizontal: 'center'
                        }}
                        transformOrigin={{
                          vertical: 'top',
                          horizontal: 'center'
                        }}
                        onClose={() => setAnchorElement(null)}
                      >
                        <Typography>{`${notes} ${
                          notes > 1
                            ? 'parcels of yours were'
                            : 'parcel of yours was'
                        } added to a shipment.`}</Typography>
                      </Popover>
                    ) : null}
                    <LogOutButton setAccountState={setAccountState} />
                    <UserAvatar accountState={accountState} />
                  </>
                ) : (
                  <>
                    <IconButton
                      color="inherit"
                      onClick={e => setAnchorElement(e.currentTarget)}
                    >
                      <AccountCircle />
                    </IconButton>
                    <Menu
                      classes={{ paper: styles.Paper, list: styles.List }}
                      anchorEl={anchorElement}
                      anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right'
                      }}
                      keepMounted
                      transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right'
                      }}
                      open={!!anchorElement}
                      onClose={() => setAnchorElement(null)}
                    >
                      <MenuItem
                        className={styles.MenuItem}
                        onClick={() => {
                          history.push('/sign-in');
                          return setAnchorElement(null);
                        }}
                      >
                        Sign In
                      </MenuItem>
                      <MenuItem
                        className={styles.MenuItem}
                        onClick={() => {
                          history.push('/sign-up');
                          return setAnchorElement(null);
                        }}
                      >
                        Sign Up
                      </MenuItem>
                    </Menu>
                  </>
                )}
              </Toolbar>
            </Container>
          </AppBar>
          <div className={styles.toolbar}></div>
        </>
      )}
    </>
  );
};

export default Header;
