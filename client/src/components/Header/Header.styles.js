import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  ToolbarBlock: {
    '& .MuiIconButton-root': {
      marginRight: '15px'
    }
  },
  Paper: {
    backgroundColor: 'transparent',
    boxShadow: 'none'
  },
  List: {
    display: 'flex',
    padding: '0',
    margin: '0'
  },
  MenuItem: {
    padding: '10px 30px',
    fontSize: '0.875rem',
    backgroundColor: '#FAFAFA',
    transition: theme.transitions.create(['background-color', 'color']),
    '&:hover': {
      backgroundColor: theme.palette.secondary.main
    },
    '& + &': {
      marginLeft: '5px'
    }
  },
  toolbar: theme.mixins.toolbar,
  popover: {
    pointerEvents: 'none'
  },
  popoverPaper: {
    padding: '10px 30px',
    backgroundColor: '#FAFAFA'
  }
}));
