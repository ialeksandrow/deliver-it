import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
  listMain: {
    width: '100%',
    border: `1px solid ${theme.palette.primary[100]}`,
    borderRadius: 3,
    overflow: 'auto',
    maxHeight: 300,
    
  },

  listItem: {
    '&:hover': {
      backgroundColor: theme.palette.secondary[100]
    }
  },

  primaryColorText: {
    color: theme.palette.primary.main,
  },

  dialogContent: {
    paddingTop: 0,
  },

  titleStyle: {
    fontWeight: 'bold',
    letterSpacing: 0.5,
    display: 'flex',
  },

  noteText: {
    fontSize: '0.8rem',
    marginLeft: 6,
    color: theme.palette.primary[300]
  },

  checkBoxes: {
    color: theme.palette.primary.main
  },

  dialogActions: {
    padding: 24,
    height: 90,
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'space-between'
  },

  titleDiv: {
    padding: 0,
    borderBottom: `1px solid ${theme.palette.primary.main}`,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    color: theme.palette.primary.main,
  },

  iconsDiv: {
    padding: 0,
    margin: 0,
    display: 'flex',
  },

  errorMessageDiv: {
    textAlign: 'center',
    color: 'red',
  }
}));
