import { useState, useEffect, forwardRef } from 'react';
import { useStyles } from './ShowCategories.styles';
import { getCategories, updateCategoryById, addCategory, deleteCategoryById } from '../../api/categories-requests';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Checkbox from '@material-ui/core/Checkbox';
import EditIcon from '@material-ui/icons/Edit';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import Tooltip from '@material-ui/core/Tooltip';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import AddBoxIcon from '@material-ui/icons/AddBox';
import DeleteIcon from '@material-ui/icons/Delete';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import DialogContentText from '@material-ui/core/DialogContentText';



const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

let backup = [];

const errorMessagePrototype = {visible: false, message: ''}

const ShowCategories = ({ setShowDialog }) => {
  const styles = useStyles();
  
  const [checked, setChecked] = useState([]);
  const [categories, updateCategories] = useState([]);
  const [editRow, setEditRow] = useState(0);
  const [editMode, setEditMode] = useState(false);
  const [addNew, setAddNew] = useState(false);
  const [newCategory, updateNewCategory] = useState('');
  const [haveError, setHaveError] = useState(false);
  const [confirmDialog, setConfirmDialog] = useState(false);
  const [errorMessage, setErrorMessage] = useState(errorMessagePrototype);


  useEffect(() => {
    getCategories().then(x => {
      backup = x;
      updateCategories(x);
    });
  }, []);

  const validateCategory = (value) => !RegExp(/^[A-Za-z]*$/).test(value);

  const changeData = (id, value) => {
    const newData = categories.map(x => x.categoryId === id ? {...x,  'categoryName': value} : x);
    updateCategories(newData);
    setErrorMessage(errorMessagePrototype);
    setHaveError(false)
  }

  const checkCurrent = (id) => {
    if (editMode) {
      return;
    }
    if (checked.includes(id)) {
      setChecked(checked.filter(x => x !== id));
      return;
    }

    setChecked([...checked, id]);
  }

  const markForEdit = (id) => {
    if (!editMode && !addNew) {
      setEditRow(id);
      return;
    }
  }

  const enterEditMode = () => {
    backup = [...categories];
    setEditMode(true);
    setChecked([]);
  }

  const cancelEdit = () => {
    updateCategories(backup);
    setEditMode(false);
    setHaveError(false);
    setErrorMessage(errorMessagePrototype);
  }

  const prepareToAddNew = () => {
    cancelEdit();
    setChecked([]);
    setEditRow(0);
    setAddNew(true);
  }

  const cancelNew = () => {
    updateNewCategory('');
    setAddNew(false);
    setHaveError(false);
    setErrorMessage(errorMessagePrototype);
  }

  const addNewCategory = () => {
    if (!newCategory || newCategory.length < 2 || validateCategory(newCategory)) {
      setHaveError(true);
      return;
    }

    if (categories.filter(x => x.categoryName.toLowerCase() === newCategory.toLowerCase()).length) {
      setErrorMessage({visible: true, message: 'This category already exists!'})
      return;
    }

    const body = { category: newCategory };

    addCategory(body).then(x => {
      if (x.message) {
        return
      }
      cancelNew();
      updateCategories([...categories, x]);
    })
  }

  const categoryUpdate = (id) => {
    const takeValue = (categories.filter(x => x.categoryId === id))[0].categoryName;
    const oldValue = (backup.filter(x=> x.categoryId === id))[0].categoryName;

    if (takeValue.toLowerCase() === oldValue.toLowerCase()) {
      cancelEdit();
      return;
    }
    
    if (backup.filter(x => x.categoryName.toLowerCase() === takeValue.toLowerCase()).length) {
      setErrorMessage({visible: true, message: 'This category already exists!'})
      return;
    }

    if (!takeValue || takeValue.length < 2 || validateCategory(takeValue)) {
      setHaveError(id);
      setErrorMessage({visible: true, message: 'Only latin symbols in range 2 - 50! Not numbers and spaces!'})
      return;
    }

    const body = {'category': takeValue};

    updateCategoryById(+id, body)
    .then(x => {
      if (x.message) {
        return;
      }
      const updatedData = categories.map(s => s.categoryId === x.categoryId ? x : s);
      updateCategories(updatedData);
      setEditMode(false);
    });
  }

  const deleteCategories = () => {
    setConfirmDialog(false);
    Promise.all(checked.map(el => deleteCategoryById(el)))
    .then(x => {
      const removed = x.map(s => s.categoryId);
      const updatedData = categories.filter(z => !removed.includes(z.categoryId));
      setChecked([]);
      updateCategories(updatedData);
    });
  }

  return (
    <>
      <Dialog
        open={true}
        fullWidth={true}
        TransitionComponent={Transition}
        maxWidth='xs'
        keepMounted
      >
      <DialogTitle>
        <div className={styles.titleDiv}>
          <Typography variant='h6' className={styles.titleStyle}>
            CATEGORIES {checked.length ? <div className={styles.noteText}>{`(${checked.length} selected)`}</div> : null}
          </Typography>
          <div className={styles.iconsDiv}>
            <IconButton size='small' color='primary' onClick={prepareToAddNew}>
              <Tooltip arrow title="Add new">
                <AddBoxIcon/>
              </Tooltip>
            </IconButton>
            {
              checked.length
              ? (
                  <IconButton size='small' color='primary' onClick={() => setConfirmDialog(true)} style={{paddingRight:0}}>
                    <Tooltip arrow title="Delete">
                      <DeleteIcon/>
                    </Tooltip>
                  </IconButton>
                )
              : null
            }
            
          </div>
        </div>
      </DialogTitle>
      <DialogContent className={styles.dialogContent}>
        <List className={styles.listMain} onMouseLeave={() => markForEdit(0)}>
          {
            categories.map((x) => (
              <ListItem
              style={ validateCategory(x.categoryName) || haveError === x.categoryId ? {backgroundColor:'#ffcccc'} : null}
              className={styles.listItem}
              key={x.categoryId}
              dense
              button onClick={() => checkCurrent(x.categoryId)}
              onMouseEnter={() => markForEdit(x.categoryId)}
              >
                <ListItemIcon>
                  <Checkbox
                  color='primary'
                  className={styles.checkBoxes}
                  edge='start'
                  checked={checked.includes(x.categoryId)}
                  tabIndex={-1}
                  disableRipple
                  />
                </ListItemIcon>
                {
                  editMode && editRow === x.categoryId
                  ? (
                    <Input
                      fullWidth
                      error={validateCategory(x.categoryName) || haveError !== false}
                      // style={validateCategory(x.categoryName) || haveError ? {backgroundColor: '#ffcccc'} : null}
                      value={x.categoryName}
                      onChange={(e) => changeData(x.categoryId, e.target.value)}
                      endAdornment={
                        <InputAdornment position='end'>
                          <IconButton size='small' onClick={() => categoryUpdate(x.categoryId)}>
                            <CheckIcon color='primary'/>
                          </IconButton>
                          <IconButton size='small' onClick={cancelEdit}>
                            <ClearIcon color='primary'/>
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    )
                  : <ListItemText className={styles.primaryColorText} primary={x.categoryName} />
                }
                {
                  editRow !== x.categoryId || editMode
                  ? null
                  : (
                      <ListItemSecondaryAction>
                        <IconButton edge='end' onClick={enterEditMode}>
                          <Tooltip arrow title="Edit current">
                            <EditIcon color='primary' fontSize='small' />
                          </Tooltip>
                        </IconButton>
                      </ListItemSecondaryAction>
                    )
                }
              </ListItem>
            ))
          }
          </List>
        </DialogContent>
        <Collapse in={errorMessage.visible}>
          <div className={styles.errorMessageDiv}>{errorMessage.message}</div>
        </Collapse>
        <DialogActions className={styles.dialogActions} style={!addNew ? {justifyContent: 'flex-end'} : null}>
          {
            !addNew
            ? null
            : (
              <FormControl>
                {
                  validateCategory(newCategory) || haveError
                  ? <FormHelperText style={{color: 'red'}}>Only latin symbols in range 2 - 50</FormHelperText>
                  : null
                }

                <Input
                error={validateCategory(newCategory) || haveError}
                placeholder='Add new category'
                value={newCategory}
                onChange={(e) => {updateNewCategory(e.target.value); setHaveError(false)}}
                endAdornment = {
                  <InputAdornment position='end'>
                    <IconButton size='small' onClick={addNewCategory}><CheckIcon color='primary'/></IconButton>
                    <IconButton size='small' onClick={cancelNew}><ClearIcon color='primary'/></IconButton>
                  </InputAdornment>
                  }
                />
                
                
                </FormControl>
              )
          }
          
          <Button variant='contained' onClick={() => setShowDialog(false)} color='secondary' startIcon={<ClearIcon/>}>
            Close
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog open={confirmDialog} TransitionComponent={Transition} keepMounted>
        <DialogTitle>PLEASE CONFIRM</DialogTitle>
        <DialogContent>
          <DialogContentText color='primary'>
            All selected categories will be deleted permanently.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button variant='contained' color='secondary' onClick={() => setConfirmDialog(false)}>Cancel</Button>
          <Button variant='contained' color='primary' onClick={deleteCategories}>ok</Button>
        </DialogActions>
      </Dialog>
    </>)
};

export default ShowCategories;
