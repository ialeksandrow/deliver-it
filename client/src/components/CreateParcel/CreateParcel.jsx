import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import { getCategories } from '../../api/categories-requests';
import { useState, useEffect } from 'react';
import { getCountries, getCities } from '../../api/global-requests';
import { getWarehouses } from '../../api/warehouse-request';
import Button from '@material-ui/core/Button';
import { createParcel } from '../../api/parcel-requests.js';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useStyles } from './CreateParcel.styles';
import Paper from '@material-ui/core/Paper';
import { useHistory } from 'react-router-dom';
import ROUTER_PATHS from '../../common/router-paths';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ArrowBack';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';


const checkName = (value) => !value || value.length < 2 || !RegExp(/^[a-zA-Z0-9-_., ]*$/).test(value);
const checkWeight = (value) => !value || isNaN(value) || value <= 0;

const CreateParcel = () => {
  const data = {
    parcelName: {
      data: '',
      error: false,
    },
    parcelWeight: {
      data: 0,
      error: false,
    },
    parcelCategory: 3,
    deliveryTypeId: 1,
    country: 1,
    city: 0,
    warehouse: 0,
  }

  const styles = useStyles();
  const history = useHistory();  

  const [categories, updateCategories] = useState([]);
  const [countries, updateCountries] = useState([]);
  const [cities, updateCities] = useState([]);
  const [warehouses, updateWarehouses] = useState([]);
  const [showDialog, updateShowDialog] = useState(false);
  const [fields, updateFields] = useState(data);
  const [dialogData, setDialogData] = useState({title: 'NONE', message: 'TEST', action: () => console.log('start')});

  const changeParcelName = (value) => {
    updateFields({...fields, parcelName: {'data': value, 'error': checkName(value)}});
  }

  const changeParcelWeight = (value) => {
    if (value < 0) {
      return;
    }
    updateFields({...fields, parcelWeight: {'data': value, 'error': checkWeight(value)}});
  }
  
  const checkAndSend = () => {
    const nameError = {...fields.parcelName, error: checkName(fields.parcelName.data.trim())};
    const weightError = {...fields.parcelWeight, error: checkWeight(fields.parcelWeight.data)};
    
    updateFields({...fields, parcelName: nameError, parcelWeight: weightError});

    if (nameError.error || weightError.error) {
      return;
    }
    
    const requestBody = {
      parcelName: fields.parcelName.data.trim(),
      parcelWeight: Number(fields.parcelWeight.data),
      parcelCategory: fields.parcelCategory,
      deliveryTypeId: fields.deliveryTypeId,
      warehouseId: fields.warehouse,
    }

    createParcel(requestBody).then(x => {
      if (x.message) {
        setDialogData({
          title: 'ERROR',
          message: x.message,
          action: () => updateShowDialog(false)
        });
      } else {
        setDialogData({
          title: 'SUCCESS',
          message: 'Your parcel was created',
          action: () => history.push(ROUTER_PATHS.PARCELS),
        });
      }
      updateShowDialog(true);
    });
    
  }

  const changeCountry = (value) => {
    const startCity =  cities.filter(x => x.country === value);
    const startWarehouse = warehouses.filter(x => x.cityName === startCity[0].name);
    updateFields({...fields, country: value, city: startCity[0].name, warehouse: startWarehouse[0].id});
  }

  const changeCity = (value) => {
    const startWarehouse = warehouses.filter(x => x.cityName === value);
    updateFields({...fields, city: value, warehouse: startWarehouse[0].id});
  }

  const clearForm = () => {
    const country = countries[0].name
    const city =  (cities.filter(x => x.country === country))[0].name;
    const warehouse = (warehouses.filter(x => x.cityName === city))[0].id;
    updateFields({
      parcelName: { data: '', error: false},
      parcelWeight: { data: 0, error: false},
      parcelCategory: 3,
      deliveryTypeId: 1,
      country: country,
      city: city,
      warehouse: warehouse,
    });
  }

  useEffect(() => {
    getCategories().then(x => {
      updateCategories(x);
      updateFields({...fields, parcelCategory: x[0].categoryId});
    });

    getCountries().then(x => {
      updateCountries(x.data);

      getCities().then(z => {
        updateCities(z.data);
        const startCity =  z.data.filter(s => s.country === x.data[0].name );
        getWarehouses().then(a => {
          updateWarehouses(a.data);
          const startWarehouse = a.data.filter(r => r.cityName === startCity[0].name);
          updateFields({...fields, country: x.data[0].name, city: startCity[0].name, warehouse: startWarehouse[0].id});
        })
      });
    });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  
  
  return (
    <Paper variant='outlined' square elevation={5} className={styles.paperStyle}>
      <Dialog open={showDialog}>
        <DialogTitle style={{textAlign: 'center'}}>{dialogData.title}</DialogTitle>
        <DialogContent style={{width:300, height:100}}>
          <DialogContentText style={{ color: 'black', textAlign: 'center' }}>{dialogData.message}</DialogContentText>
        </DialogContent>
        <DialogActions style={{justifyContent: 'center'}}>
          <Button variant='contained' onClick={dialogData.action} color='primary' >OK</Button>
        </DialogActions>
      </Dialog>
      
      <div className={styles.hLine}>
        <Typography variant='h4' >Create New Parcel</Typography>
        <div className={styles.iconList}>
          <IconButton onClick={() => history.push(ROUTER_PATHS.PARCELS)}>
            <BackIcon className={styles.primaryColorIcon} />
          </IconButton>
        </div>
      </div>

      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                required
                variant='outlined'
                label='Parcel name'
                placeholder='Parcel name'
                value={fields.parcelName.data}
                error={fields.parcelName.error}
                InputLabelProps={{className: styles.myParcelName}}
                onChange={(e) => changeParcelName(e.target.value)}
                helperText={fields.parcelName.error ? 'Must be string with min. 2 characters' : null}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                required
                type='number'
                label='Weight'
                id='parcelWeight'
                variant='outlined'
                value={fields.parcelWeight.data}
                error={fields.parcelWeight.error}
                InputLabelProps={{ className: styles.myParcelName }}
                onChange={(e) => changeParcelWeight(e.target.value)}
                helperText={fields.parcelWeight.error ? 'Must be number bigger from zero' : null}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl variant='outlined' required fullWidth>
                <InputLabel className={styles.myParcelName}>Category</InputLabel>
                <Select
                  native
                  label='Category *'
                  value={fields.parcelCategory}
                  onChange={(e) => updateFields({...fields, parcelCategory: e.target.value})}
                >
                  {
                    !categories.length
                    ? null
                    : categories.map(el => <option key={el.categoryId} value={el.categoryId}>{el.categoryName}</option>)
                  }
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl variant='outlined' fullWidth required>
                <InputLabel className={styles.myParcelName}>Delivery to</InputLabel>
                <Select
                  native
                  label='Delivery to *'
                  value={fields.deliveryTypeId}
                  onChange={(e) => updateFields({...fields, deliveryTypeId: e.target.value})}
                >
                  <option value={1}>Address</option>
                  <option value={2}>Warehouse</option>
                </Select>
              </FormControl>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <FormControl variant='outlined' fullWidth required>
                <InputLabel className={styles.myParcelName}>Country</InputLabel>
                <Select
                  native
                  label='Country *'
                  value={fields.country}
                  disabled={parseInt(fields.deliveryTypeId) === 1}
                  onChange={(e) => changeCountry(e.target.value)}
                >
                  {
                    !countries.length
                    ? null
                    : countries.map(el => <option key={el.name} value={el.name}>{el.name}</option> )
                  }
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl variant='outlined' fullWidth required>
                <InputLabel className={styles.myParcelName}>City</InputLabel>
                <Select
                  native
                  label='City *'
                  value={fields.city}
                  disabled={parseInt(fields.deliveryTypeId) === 1}
                  onChange={(e) => changeCity(e.target.value)}
                >
                  { cities.filter(x => x.country === fields.country).map(el => <option key={el.name} value={el.name}>{el.name}</option>) }
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl variant='outlined' fullWidth required>
                <InputLabel className={styles.myParcelName}>Warehouse</InputLabel>
                <Select
                  native
                  disabled={parseInt(fields.deliveryTypeId) === 1}
                  value={fields.warehouse}
                  onChange={(e) => updateFields({...fields, warehouse: +e.target.value})}
                  label='Warehouse *'
                >
                  { warehouses.filter(x => x.cityName === fields.city).map(el => <option key={el.id} value={el.id}>{el.name}</option>) }
                </Select>
              </FormControl>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Button
      variant='contained'
      startIcon={<CheckIcon/>}
      style={{marginTop: 20}}
      color='primary'
      onClick={checkAndSend}>
        Create
      </Button>
      <Button
      variant='contained'
      startIcon={<ClearIcon/>}
      style={{marginTop: 20, marginLeft: 10}}
      color='secondary'
      onClick={clearForm}
      >
        Clear form
      </Button>
      </Paper>
  );
}

export default CreateParcel;