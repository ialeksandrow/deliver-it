import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  myParcelName: {
    color: theme.palette.primary.main
  },

  paperStyle: {
    padding:20,
    marginTop: 20,
    borderRadius: 2,
  },
  
  hLine: {
    borderBottom: `1px solid ${theme.palette.primary.main}`,
    color: theme.palette.primary.main,
    paddingBottom: 5,
    marginBottom: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  iconList: {
    display: 'flex',
    justifyContent: 'flex-end',
  },

  primaryColorIcon: {
    color: theme.palette.primary.main,
  }

}));
