import { useStyles } from './Navigation.styles';
import { Link } from 'react-router-dom';

const NavigationLink = ({ route, name, active = false }) => {
  const styles = useStyles();

  return (
    <li className={styles.mainMenuItem}>
      <Link
        to={route}
        className={
          active
            ? `${styles.mainMenuLink} ${styles.active}`
            : styles.mainMenuLink
        }
      >
        {name}
      </Link>
    </li>
  );
};

export default NavigationLink;
