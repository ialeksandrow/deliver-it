import { useStyles } from './Navigation.styles';

const Navigation = ({ children }) => {
  const styles = useStyles();

  return <ul className={styles.mainMenu}>{children}</ul>;
};

export default Navigation;
