import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  mainMenu: {
    listStyle: 'none',
    fontFamily: theme.typography.fontFamily,
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    paddingInlineStart: '0',
    marginRight: '15px'
  },
  mainMenuItem: {
    marginLeft: '30px',
    '&:first-of-type': {
      marginLeft: '0'
    }
  },
  mainMenuLink: {
    textDecoration: 'none',
    color: theme.palette.text.secondary,
    '&:hover': {
      color: theme.palette.secondary.main
    },
    transition: theme.transitions.create('color')
  },
  active: {
    color: theme.palette.secondary.main
  }
}));
