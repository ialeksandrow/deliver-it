import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
  searchBar: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 10px',
    borderRadius: theme.shape.borderRadius,
    marginRight: '30px',
    backgroundColor: '#FFFFFF26',
    '&:hover': {
      backgroundColor: '#FFFFFF40'
    },
    transition: theme.transitions.create('background-color')
  },
  searchIcon: {
    marginRight: '10px'
  },
  searchInput: {
    width: '10ch',
    '&:focus-within': {
      width: '30ch'
    },
    fontSize: '0.875rem',
    color: theme.palette.text.secondary,
    transition: theme.transitions.create('width')
  }
}));
