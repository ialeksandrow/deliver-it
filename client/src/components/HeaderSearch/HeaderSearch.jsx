import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { useStyles } from './HeaderSearch.styles';

const HeaderSearch = () => {
  const styles = useStyles();

  return (
    <div className={styles.searchBar}>
      <SearchIcon className={styles.searchIcon} />
      <InputBase className={styles.searchInput} placeholder="Search..." />
    </div>
  );
};

export default HeaderSearch;
