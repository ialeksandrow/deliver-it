import { useState, useEffect } from 'react';
import { BASE_URL } from './../../common/constants';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4maps from '@amcharts/amcharts4/maps';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import am4geodata_worldLow from '@amcharts/amcharts4-geodata/worldLow';

const WarehousesMap = () => {
  const [warehouses, setWarehouses] = useState(null);

  useEffect(() => {
    fetch(`${BASE_URL}/warehouses`)
      .then(res => res.json())
      .then(resData => {
        setWarehouses(resData.data);

        am4core.useTheme(am4themes_animated);

        const map = am4core.create('map', am4maps.MapChart);
        map.geodata = am4geodata_worldLow;
        map.projection = new am4maps.projections.Miller();

        const polygonSeries = map.series.push(new am4maps.MapPolygonSeries());
        polygonSeries.exclude = ['AQ'];
        polygonSeries.useGeodata = true;
        polygonSeries.tooltip.getFillFromObject = false;
        polygonSeries.tooltip.background.fill = am4core.color('#3F51B5');

        const polygonTemplate = polygonSeries.mapPolygons.template;
        polygonTemplate.fill = am4core.color('#FFE082');
        polygonTemplate.tooltipText = '{name}';

        const hoverState = polygonTemplate.states.create('hover');
        hoverState.properties.fill = am4core.color('#FFC107');

        const imageSeries = map.series.push(new am4maps.MapImageSeries());
        imageSeries.tooltip.getFillFromObject = false;
        imageSeries.tooltip.background.fill = am4core.color('#3F51B5');
        imageSeries.mapImages.template.tooltipText = `{name}
        - City: {cityName}
        - Address: {address}
        - Incoming/Outgoing shipments: {shipmentsCount}
        - Associated parcels: {parcelsCount}
        `;
        imageSeries.mapImages.template.propertyFields.longitude = 'longitude';
        imageSeries.mapImages.template.propertyFields.latitude = 'latitude';

        const circleOne = imageSeries.mapImages.template.createChild(
          am4core.Circle
        );
        circleOne.radius = 5;
        circleOne.fill = am4core.color('#3F51B5');
        circleOne.nonScaling = true;

        const circleTwo = imageSeries.mapImages.template.createChild(
          am4core.Circle
        );
        circleTwo.radius = 5;
        circleTwo.fill = am4core.color('#3F51B5');

        const animateCircle = circle => {
          const animation = circle.animate(
            [
              {
                property: 'scale',
                from: 1 / map.zoomLevel,
                to: 5 / map.zoomLevel
              },
              { property: 'opacity', from: 1, to: 0 }
            ],
            1000,
            am4core.ease.circleOut
          );

          return animation.events.on('animationended', () =>
            animateCircle(circle)
          );
        };

        circleTwo.events.on('inited', e => animateCircle(e.target));

        imageSeries.data = resData.data;
      })
      .catch(err => console.error(err));
  }, []);

  return (
    <>
      {warehouses ? (
        <div id="map" style={{ height: '500px', width: '100%' }}></div>
      ) : (
        <CircularProgress color="primary" />
      )}
    </>
  );
};

export default WarehousesMap;
