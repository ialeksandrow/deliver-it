import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
  trackShipmentBlock: {
    height: '300px',
    width: '600px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    padding: '0 30px'
  },
  trackShipmentImage: {
    height: '120px'
  },
  trackShipmentArea: {
    display: 'flex',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: theme.palette.primary.main
  },
  trackShipmentInput: {
    padding: '0 30px',
    fontSize: '0.875rem',
    color: theme.palette.text.secondary
  },
  trackShipmentIcon: {
    color: theme.palette.text.secondary
  }
}));
