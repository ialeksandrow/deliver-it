import React, { useState } from 'react';
import trackShipment from './../../images/shipments/track-shipment.svg';
import ShipmentDocument from '../ShipmentDocument/ShipmentDocument';
import {
  Typography,
  InputBase,
  Divider,
  IconButton,
  Dialog,
  Slide
} from '@material-ui/core';
import TrackShipmentIcon from '@material-ui/icons/MyLocation';
import { useStyles } from './TrackShipment.styles';
import { BASE_URL } from './../../common/constants';

const Transition = React.forwardRef((props, ref) => (
  <Slide direction="up" ref={ref} {...props} />
));

const TrackShipment = () => {
  const styles = useStyles();
  const [shipmentInput, setShipmentInput] = useState('');
  const [shipment, setShipment] = useState(null);
  const [dialog, setDialog] = useState(false);
  const [error, setError] = useState('');

  const loadShipment = shipmentName => {
    if (!shipmentName.trim()) {
      return setError('Shipment tracking number can not be an empty field.');
    }

    fetch(`${BASE_URL}/shipments/?name=${shipmentName}`)
      .then(res => res.json())
      .then(resData => {
        if (resData.msg) {
          return setError(resData.msg);
        }

        setShipment(resData.data);
        setDialog(!dialog);
        return setShipmentInput('');
      })
      .catch(err => console.error(err));
  };

  return (
    <>
      <div className={styles.trackShipmentBlock}>
        <Typography variant="h6" component="h3">
          Track Your Shipment On The Fly
        </Typography>
        <img
          src={trackShipment}
          alt="track shipment"
          className={styles.trackShipmentImage}
        />
        {error && <Typography variant="body2">{error}</Typography>}
        <div className={styles.trackShipmentArea}>
          <InputBase
            placeholder="Enter tracking number..."
            className={styles.trackShipmentInput}
            value={shipmentInput}
            onChange={e => setShipmentInput(e.target.value)}
          />
          <Divider orientation="vertical" />
          <IconButton
            className={styles.trackShipmentIcon}
            onClick={() => loadShipment(shipmentInput)}
          >
            <TrackShipmentIcon />
          </IconButton>
        </div>
      </div>
      {shipment && (
        <Dialog
          fullScreen
          open={dialog}
          onClose={() => setDialog(!dialog)}
          TransitionComponent={Transition}
        >
          <ShipmentDocument shipment={shipment} closeDialog={setDialog} />
        </Dialog>
      )}
    </>
  );
};

export default TrackShipment;
