import { useState, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Collapse from '@material-ui/core/Collapse';
import { useStyles } from './UserAvatar.styles';

const UserAvatar = ({ accountState }) => {
  const [avatar, setAvatar] = useState(null);
  const [userInfo, setUserInfo] = useState(false);
  const styles = useStyles();

  useEffect(() => {
    fetch(`${accountState.avatar}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.blob())
      .then(resData => setAvatar(URL.createObjectURL(resData)))
      .catch(err => console.error(err));
  }, [accountState]);

  return (
    <>
      {accountState.avatar ? (
        <Avatar
          src={`${avatar}`}
          alt={`${accountState.firstName.toLowerCase()} ${accountState.lastName.toLowerCase()}`}
          className={styles.Avatar}
          onClick={() => setUserInfo(!userInfo)}
        />
      ) : (
        <Avatar
          className={styles.Avatar}
          onClick={() => setUserInfo(!userInfo)}
        >{`${accountState.firstName[0].toUpperCase()}${accountState.lastName[0].toUpperCase()}`}</Avatar>
      )}
      <Collapse in={userInfo} className={styles.UserBlock}>
        <div className={styles.UserBlockContainer}>
          {accountState.avatar ? (
            <Avatar
              src={`${avatar}`}
              alt={`${accountState.firstName.toLowerCase()} ${accountState.lastName.toLowerCase()}`}
              className={styles.UserBlockAvatar}
              onClick={() => setUserInfo(!userInfo)}
            />
          ) : (
            <Avatar
              className={styles.UserBlockAvatar}
              onClick={() => setUserInfo(!userInfo)}
            >{`${accountState.firstName[0].toUpperCase()}${accountState.lastName[0].toUpperCase()}`}</Avatar>
          )}
          <ul className={styles.UserBlockInfo}>
            <li className={styles.UserBlockInfoItem}>
              Name:
              <span>
                {accountState.firstName} {accountState.lastName}
              </span>
            </li>
            <li className={styles.UserBlockInfoItem}>
              Email:<span>{accountState.eMail}</span>
            </li>
            <li className={styles.UserBlockInfoItem}>
              Location:
              <span>
                {accountState.city}, {accountState.country}
              </span>
            </li>
            <li className={styles.UserBlockInfoItem}>
              Privileges:
              <span>{accountState.role === 1 ? 'Operator' : 'Customer'}</span>
            </li>
            <li className={styles.UserBlockInfoItem}>
              Shipments done:<span>{accountState.associatedShipments}</span>
            </li>
            <li className={styles.UserBlockInfoItem}>
              Parcels send:<span>{accountState.associatedParcels}</span>
            </li>
          </ul>
        </div>
      </Collapse>
    </>
  );
};

export default UserAvatar;
