import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
  Avatar: {
    height: '32px',
    width: '32px',
    fontSize: '0.875rem',
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.text.primary,
    cursor: 'pointer'
  },
  UserBlock: {
    width: 'auto',
    position: 'absolute',
    top: '100%',
    right: '0',
    borderBottomLeftRadius: `calc(${theme.shape.borderRadius}*10px)`,
    backgroundColor: theme.palette.primary.main
  },
  UserBlockContainer: {
    display: 'flex',
    padding: '10px'
  },
  UserBlockAvatar: {
    height: '64px',
    width: '64px',
    alignSelf: 'flex-end',
    border: `3px solid ${theme.palette.secondary.main}`,
    fontSize: '2rem',
    backgroundColor: theme.palette.secondary.main,
    color: '#FFA000',
    cursor: 'pointer'
  },
  UserBlockInfo: {
    padding: '0',
    margin: '0 0 0 15px',
    listStyle: 'none'
  },
  UserBlockInfoItem: {
    padding: '5px 10px',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#5c6bc0',
    '& + &': {
      marginTop: '10px'
    },
    '& span': {
      marginLeft: '5px',
      color: theme.palette.secondary.main
    }
  }
}));
