import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  brandBlock: {
    margin: props => props.margin.value
  },
  brandLink: {
    display: 'flex',
    alignItems: 'center',
    textDecoration: 'none',
    color: theme.palette.text.secondary
  },
  brandLogo: {
    height: props => props.logoSize.value,
    width: props => props.logoSize.value,
    marginRight: '5px'
  },
  brandLogoPath: {
    fill: theme.palette.secondary.main
  },
  brandName: {
    fontFamily: theme.typography.fontFamily,
    fontSize: props => props.nameSize.value
  }
}));
