import React from 'react';
import { useHistory } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { useStyles } from './ParcelUserView.styles.js';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import IconButton from '@material-ui/core/IconButton';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import TablePagination from '@material-ui/core/TablePagination';
import TextField from '@material-ui/core/TextField';
import { userFilters } from '../../common/filters';
import { stableSort, getComparator } from '../../common/sort-functions';
import { getParcels, deleteParcel, getParcelById } from '../../api/parcel-requests';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FilterListIcon from '@material-ui/icons/FilterList';
import Tooltip from '@material-ui/core/Tooltip';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import DeleteIcon from '@material-ui/icons/Delete';
import CachedIcon from '@material-ui/icons/Cached';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import ClearIcon from '@material-ui/icons/Clear';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Slide from '@material-ui/core/Slide';
import RowUserTable from './ParcelTableUserRow.jsx';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import TuneIcon from '@material-ui/icons/Tune';
import ROUTER_PATHS from '../../common/router-paths.js';
import Snackbar from '@material-ui/core/Snackbar';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import { getCategories } from '../../api/categories-requests.js';
import { getWarehouses } from '../../api/warehouse-request.js';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const headCells = [
  { id: 'parcelId', label: 'ID', align: 'left', width: '5%' },
  { id: 'parcelName', label: 'Name', align: 'left', width: '30%' },
  { id: 'deliveryType', label: 'Delivery to', align: 'left', width: '15%' },
  { id: 'ship', label: 'Shipment', align: 'center', width: '30%' },
  { id: 'parcelStatusId', label: 'Status', align: 'right', width: '10%' },
];

const ParcelUserTable =() => {
  const [rowsPerPage, setRowsPerPage] = useState(6);
  const [page, setPage] = useState(0);
  const [rows, updateRows] = useState([]);
  const [orderBy, setOrderBy] = useState('parcelId');
  const [order, setOrder] = useState('desc');
  const [checked, updateChecked] = useState([]);
  const [filter, updateFilter] = useState(userFilters);
  const [search, updateSearch] = useState('');
  const [dialog, updateDialog] = useState(false);
  const [showFilters, updateShowFilters] = useState({ show: false, gridSize: 12 });
  const [successMessage, setSuccessMessage] = useState({open: false, type: 'success', message: ''});

  const styles = useStyles();
  const history = useHistory();

  useEffect(() => {
    getParcels(search).then(x => {
      if (x.message) {
        updateRows([])
      } else {
        const addShipment = x.map(s => s.shipment ? {...s, ship: s.shipment.name} : {...s, ship: ' - '})
        updateRows(addShipment);
      }
    });
  },[search]);


  useEffect(() => {
    if (userFilters.length > 5) {
      return;
    }

    getCategories().then(x => {
      const convertCategories = [{ id: '', text: '' }];
      x.forEach(s => convertCategories.push ({id: s.categoryId, text: s.categoryName }));
      
      const categories = {
        id:5,
        name: 'Category',
        key: 'categoryId' ,
        value: '',
        options: convertCategories
      }

      userFilters.push(categories);
    });

    getWarehouses().then(x => {
      const convertWarehouses = [{ id: '', text: '' }];
      x.data.forEach(s => convertWarehouses.push ({id: s.id, text: s.name }));

      const warehouses = {
        id:6,
        name: 'Warehouse',
        key: 'warehouseId',
        value: '',
        options: convertWarehouses
      }

      userFilters.push(warehouses);
    });
  }, [])

  const handleChangePage = (event, newPage) => {
    updateChecked([]);
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleRequestSort = (_, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const createSortHandler = (property) => (event) => {
    handleRequestSort(event, property);
  };

  const getCurrentRows = () => {
    return stableSort(rows, getComparator(order, orderBy)).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => row.parcelId);
  }

  const checkAll = () => {
    if (checked.length === rowsPerPage || checked.length === rows.length || getCurrentRows().length === checked.length) {
      updateChecked([])
      return
    }
    const newArray = stableSort(rows, getComparator(order, orderBy)).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => row.parcelId);
    updateChecked(newArray);
  }

  const filterState = (index, data) => {
    const updated = filter.map(x => x.id !== index ? x : {...x, value: data});
    updateFilter(updated);
  }

  const applyFilters = () => {    
    const searchQuery = filter.map(x => x.value.trim() ? `${x.key}=${x.value}` : null).filter(s => s !== null).join('&');
    updateSearch(searchQuery);
    setPage(0);
  }

  const clearFilters = () => {
    const newArray = filter.map(x => x = {...x, value: ''});
    updateSearch('');
    updateFilter(newArray);
  }

  const deleteSelected = () => {
    updateDialog(false);
    Promise.all(checked.map(x => deleteParcel(x)))
    .then(x => {
      if (x.filter(el => el.message).length) {
        const errorMessage = (x.filter(el => el.message)).map(x => x.message);
        console.log (errorMessage);
        setSuccessMessage({open: true, type: 'error', message: errorMessage});
      }
      const removed = x.map(s => s.parcelId);
      updateRows(rows.filter(el => !removed.includes(el.parcelId)));
      updateChecked([])
    });
  }

  const markParcelAsRead = (id) => {
    getParcelById(id)
    .then(x => {
      if (x.message) {
        updateRows([])
      } else {
        updateRows(rows.map(s => s.parcelId !== x.parcelId ? s : x));
      }
    })
  }

  const reloadData = () => {
    updateSearch(search ? '' : 0);
    setPage(0);
    setOrderBy('parcelId');
  }

  const allIsChecked = () => {
    if ((checked.length === rowsPerPage || checked.length === rows.length ||
          getCurrentRows().length === checked.length) && rows.length) {
      return true;
    }
    return false;
  }

  const showForm = () => updateShowFilters({show: !showFilters.show, gridSize: 10})

  const hideFilters = () => updateShowFilters({...showFilters, gridSize: 12});

  return (
    <Paper variant='outlined' square elevation={5} className={styles.paperStyle}>
      <Grid container spacing={3}>
        <Slide direction='right' in={showFilters.show} mountOnEnter unmountOnExit timeout={800} onExited={hideFilters}>
          <Grid item xs={12} sm={2}>
            <div className={styles.filterHead}>
              <Typography variant='h4'>Filters</Typography>
            <div className={styles.iconList}>
              {
                filter.some(x => x.value)
                  ? (
                    <Tooltip title='Remove filters'>
                      <IconButton className={styles.filterButton} onClick={clearFilters}><ClearIcon color='primary'/></IconButton>
                    </Tooltip>
                    )
                  : null
              }
              <Tooltip title='Apply filters'>
                <IconButton className={styles.filterButton} onClick={applyFilters}><FilterListIcon color='primary'/></IconButton>
              </Tooltip>
            </div>
          </div>
            {
              userFilters.map(item => {
                return (
                <Accordion key={item.id}>
                  <AccordionSummary expandIcon={<ExpandMoreIcon /> }>
                    <Typography className={styles.heading}>{item.name} {filter[item.id].value ? '*' : null}  </Typography>
                  </AccordionSummary>
                  <AccordionDetails className={styles.accordionStyle}>
                  {
                    item.options.length
                    ? (
                      <FormControl variant='outlined' fullWidth size='small'>
                        <Select
                        native
                        fullWidth
                        value={filter[item.id].value}
                        onChange={(e) => filterState(item.id, e.target.value)}
                        > 
                          { item.options.map(option => <option key={option.id} value={option.id}>{option.text}</option>) }
                        </Select>
                        </FormControl>
                      )
                    : (
                        <TextField
                          fullWidth
                          size='small'
                          variant='outlined'
                          value={filter[item.id].value}
                          onChange={(e) => filterState(item.id, e.target.value)}
                        />
                      )
                  }
                  </AccordionDetails>
                </Accordion>)
              })
            }
          </Grid>
        </Slide>
        <Grid item xs={12} sm={showFilters.gridSize}>
          <div className={styles.tableHead}>
            <Typography variant='h4' style={{display:'flex'}}>My Parcels
              {
                !showFilters.show && search
                ? <Tooltip arrow title='Have active filters'><AnnouncementIcon fontSize='small' color='primary' style={{marginLeft:5}}/></Tooltip>
                : null
              }
              </Typography>
            <div className={styles.iconList}>
              <Tooltip title='Reload parcels'>
                <IconButton onClick={reloadData}><CachedIcon color='primary'/></IconButton>
              </Tooltip>
              <Tooltip title='Filters'>
                <IconButton onClick={showForm}><TuneIcon color='primary'/></IconButton>
              </Tooltip>
              <Tooltip title='Create parcel'>
                <IconButton onClick={() => history.push(ROUTER_PATHS.CREATE_PARCEL)}><AddCircleOutlineIcon color='primary'/></IconButton>
              </Tooltip>
              {
                checked.length
                ? (
                  <Tooltip title='Delete'>
                    <IconButton onClick={() => updateDialog(true)}><DeleteIcon color='primary'/></IconButton>
                  </Tooltip>
                  )
                : null
              }
            </div>
          </div>
          <TableContainer component={Paper}>
            <Table aria-label='collapsible table'>
              <TableHead>
                <TableRow>
                  <TableCell width='5%' />
                  <TableCell width='5%'>
                    <Checkbox
                    color='primary'
                    className={styles.checkBoxStyleDark}
                    checked={allIsChecked()}
                    onClick={checkAll}
                    />
                  </TableCell>
                    {headCells.map((headCell) => (
                    <TableCell width={headCell.width}
                      key={headCell.id}
                      align={headCell.align}
                      sortDirection={orderBy === headCell.id ? order : false}
                    >
                      <TableSortLabel
                        classes={{
                          root: styles.tableRoot,
                          active: styles.tableActive,
                          icon: styles.tableIcon,
                        }}
                        className={styles.test}
                        active={orderBy === headCell.id}
                        direction={orderBy === headCell.id ? order : 'asc'}
                        onClick={createSortHandler(headCell.id)}
                      >
                        { headCell.label }
                      </TableSortLabel>
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {stableSort(rows, getComparator(order, orderBy)).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => (
                  <RowUserTable key={row.parcelId} row={row} checked={checked} updateChecked={updateChecked} markAsRead={markParcelAsRead} />
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[6, 12, 24]}
            component='div'
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Grid>
      </Grid>
      <Dialog open={dialog} TransitionComponent={Transition} keepMounted>
        <DialogTitle>PLEASE CONFIRM</DialogTitle>
        <DialogContent>
          <DialogContentText color='primary'>
            All selected parcels will be deleted permanently.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button variant='contained' color='secondary' onClick={() => updateDialog(false)}>Cancel</Button>
          <Button variant='contained' color='primary' onClick={deleteSelected}>ok</Button>
        </DialogActions>
      </Dialog>
      { successMessage.open
        ? successMessage.message.map((element, index) =>
            (
              <Snackbar key={index}
                open={successMessage.open}
                style={{bottom: 24 + (index * 60)}}
                onClose={() => setSuccessMessage({...successMessage, open: false})}
                autoHideDuration={5000}
                message={element}
                ContentProps={{
                  className: successMessage.type === 'error' ? styles.snackbarError : styles.snackbarSuccess,
                }}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
                action={successMessage.type === 'error' ? <ErrorIcon/> : <CheckCircleIcon />}
              />
            )
          )
        :null
      }
    </Paper>
  );
}

export default ParcelUserTable;