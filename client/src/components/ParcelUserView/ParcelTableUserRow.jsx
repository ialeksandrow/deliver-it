import React from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import Collapse from '@material-ui/core/Collapse';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import { useState } from 'react';
import { useStyles } from './ParcelUserView.styles.js';
import { Link } from 'react-router-dom';
import ROUTER_PATHS from '../../common/router-paths.js';


const RowUserTable = (props) => {  
  const { row, checked, updateChecked, markAsRead } = props;
  const styles = useStyles();
  const [open, setOpen] = useState(false);  

  const addCheck = (id) => {
    if (checked.includes(id)) {
      updateChecked(checked.filter(x => x !== id));
    } else {
      updateChecked([...checked, id])
    }
  }

  const checkForNew = (shipment, notice) => {
    if (shipment && !notice) {
      return ({fontWeight: 'bold'});
    }
  }

  const openRow = (id) => {
    setOpen(!open)
    markAsRead(id);
  }

  return (    
    <React.Fragment>
      <TableRow className={!open ? styles.closedRow : styles.openRow}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => openRow(row.parcelId)}>
            {open ? <KeyboardArrowUpIcon className={styles.whiteIcon} /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell>
          <Checkbox
          color={!open ?'primary' : 'secondary'}
          className={!open ? styles.checkBoxStyleDark : styles.checkBoxStyleLight}
          onChange={() => addCheck(row.parcelId)}
          checked={checked.includes(row.parcelId)}
          />
        </TableCell>
        
        <TableCell style={checkForNew(row.shipment, row.ownerIsNotified)} className={!open ? styles.textColorDark : styles.textColorWhite} align="left">
          <Link className={styles.linkStyle} to={ROUTER_PATHS.VIEW_PARCEL.replace(':id', row.parcelId)}>
            {row.parcelId}
          </Link>
        </TableCell>
        <TableCell style={checkForNew(row.shipment, row.ownerIsNotified)} className={!open ? styles.textColorDark : styles.textColorWhite} align="left">
          <Link className={styles.linkStyle} to={ROUTER_PATHS.VIEW_PARCEL.replace(':id', row.parcelId)}>
            {row.parcelName}
          </Link>
        </TableCell>
        <TableCell style={checkForNew(row.shipment, row.ownerIsNotified)} className={!open ? styles.textColorDark : styles.textColorWhite} align="left">{row.deliveryType}</TableCell>
        <TableCell style={checkForNew(row.shipment, row.ownerIsNotified)} className={!open ? styles.textColorDark : styles.textColorWhite} align="center">{row.shipment ? row.shipment.name : ' - '}</TableCell>
        <TableCell style={checkForNew(row.shipment, row.ownerIsNotified)} className={!open ? styles.textColorDark : styles.textColorWhite} align="right">{row.parcelStatus}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ padding: 0}} colSpan={7}>
          <Collapse in={open} timeout="auto" unmountOnExit >
            <Box margin={0}>
              <Table size="small">
                <TableHead>
                  <TableRow className={styles.hiddenTableHead}>
                    <TableCell className={styles.headFonts}>Weight</TableCell>
                    <TableCell className={styles.headFonts}>Category</TableCell>
                    <TableCell className={styles.headFonts}>Country</TableCell>
                    <TableCell className={styles.headFonts}>City</TableCell>
                    <TableCell className={styles.headFonts}>Address</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow className={styles.hiddenTableData}>
                    <TableCell>{row.parcelWeight}</TableCell>
                    <TableCell>{row.parcelCategory}</TableCell>
                    <TableCell>{row.deliveryCountry}</TableCell>
                    <TableCell>{row.deliveryCity}</TableCell>
                    <TableCell>{row.deliveryTypeId === 1 ? row.userAddress : row.warehouseName}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
              {
                !row.shipment
                ? null
                : (
                  <Table size="small">
                  <TableHead>
                    <TableRow className={styles.hiddenTableHead}>
                    <TableCell className={styles.headFonts} align='center' colSpan={5}>SHIPMENT INFO</TableCell>                    
                    </TableRow>
                    <TableRow className={styles.hiddenTableHead}>
                      <TableCell className={styles.headFonts}>Departure Date</TableCell>
                      <TableCell className={styles.headFonts}>Arrival Date</TableCell>
                      <TableCell className={styles.headFonts}>Origin Warehouse</TableCell>
                      <TableCell className={styles.headFonts}>Destination Warehouse</TableCell>
                      <TableCell className={styles.headFonts}>Status</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className={styles.hiddenTableData}>
                      <TableCell>{new Date(row.shipment.departureDate).toLocaleDateString('fr-CA')}</TableCell>
                      <TableCell>{new Date(row.shipment.arrivalDate).toLocaleDateString('fr-CA')}</TableCell>
                      <TableCell>{row.shipment.originWarehouse}</TableCell>
                      <TableCell>{row.shipment.destinationWarehouse}</TableCell>
                      <TableCell>{row.shipment.status}</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
                  )
              }
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

RowUserTable.propTypes = {
  row: PropTypes.object.isRequired,
  checked: PropTypes.array.isRequired,
  updateChecked: PropTypes.func.isRequired,
  markAsRead: PropTypes.func.isRequired,
};

export default RowUserTable;