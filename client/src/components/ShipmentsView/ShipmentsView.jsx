import shipmentBox from './../../images/shipments/shipment-box.svg';
import shipmentBoxOpened from './../../images/shipments/shipment-box-opened.svg';
import { useEffect, useState } from 'react';
import ShipmentsEdit from './../ShipmentsEdit/ShipmentsEdit';
import ShipmentsParcelManagement from './../ShipmentsParcelManagement/ShipmentsParcelManagement';
import { BASE_URL } from './../../common/constants';
import { useStyles } from './ShipmentsView.styles';

const ShipmentsView = () => {
  const styles = useStyles();
  const [shipments, setShipments] = useState(null);
  const [warehouses, setWarehouses] = useState(null);
  const [selectedShipment, setSelectedShipment] = useState('');
  const [selectedWarehouse, setSelectedWarehouse] = useState('');
  const [givenCustomerEmail, setGivenCustomerEmail] = useState('');

  useEffect(() => {
    fetch(
      `${BASE_URL}/shipments?warehouseId=${selectedWarehouse}&userEmail=${givenCustomerEmail}`
    )
      .then(res => res.json())
      .then(resData => {
        if (resData.msg) {
          return console.error(resData.msg);
        }

        return setShipments(resData.data);
      })
      .catch(err => console.error(err));
  }, [selectedWarehouse, givenCustomerEmail]);

  useEffect(() => {
    fetch(`${BASE_URL}/warehouses`)
      .then(res => res.json())
      .then(resData => setWarehouses(resData.data))
      .catch(err => console.error(err));
  }, []);

  return (
    <>
      {shipments && warehouses && (
        <>
          <div className={styles.shipmentsViewContainer}>
            <div className={styles.shipmentsViewBlock}>
              <div className={styles.shipmentsViewBlockHead}>
                <div className={styles.shipmentsViewBlockHeading}>
                  Shipments Overview
                </div>
                <div className={styles.shipmentsViewBlockFilters}>
                  <select
                    defaultValue={'default'}
                    className={styles.shipmentsViewBlockFilter}
                    onChange={e => setSelectedWarehouse(e.target.value)}
                  >
                    <option value="default" disabled>
                      Warehouse
                    </option>
                    {warehouses.map(({ id, name }) => (
                      <option key={id} value={id}>
                        {name}
                      </option>
                    ))}
                  </select>
                  <input
                    type="text"
                    placeholder="Customer Email"
                    className={styles.shipmentsViewBlockFilter}
                    onChange={e => setGivenCustomerEmail(e.target.value)}
                  />
                </div>
              </div>
              <div className={styles.shipmentsViewBlockBody}>
                {shipments.map(shipment => (
                  <div
                    key={shipment.name}
                    className={
                      Date.now() >= Date.parse(shipment.arrivalDate)
                        ? `${styles.shipmentsViewBlockBox} ${
                            styles.delivered
                          } ${
                            selectedShipment === shipment.name &&
                            `${styles.active}`
                          }`
                        : `${styles.shipmentsViewBlockBox} ${
                            selectedShipment === shipment.name &&
                            `${styles.active}`
                          }`
                    }
                    onClick={() => setSelectedShipment(shipment.name)}
                  >
                    <div className={styles.shipmentsViewBlockBoxData}>
                      <img
                        src={
                          Date.now() >= Date.parse(shipment.arrivalDate)
                            ? shipmentBoxOpened
                            : shipmentBox
                        }
                        alt="shipment box"
                        className={styles.shipmentsViewBlockBoxDataImg}
                      />
                      <div className={styles.shipmentsViewBlockBoxDataInfo}>
                        <ul>
                          <li>{shipment.name}</li>
                          <li>
                            Status: <span>{shipment.status}</span>
                          </li>
                          <li>
                            Foreperson: <span>{shipment.operatorEmail}</span>
                          </li>
                          <li>
                            Departed from{' '}
                            <span>{shipment.originWarehouse}</span> on{' '}
                            <span>
                              {new Date(shipment.departureDate).toLocaleString(
                                'en-GB'
                              )}
                            </span>
                          </li>
                          {Date.now() >= Date.parse(shipment.arrivalDate) ? (
                            <li>
                              Arrived in{' '}
                              <span>{shipment.destinationWarehouse}</span> on{' '}
                              <span>
                                {new Date(shipment.arrivalDate).toLocaleString(
                                  'en-GB'
                                )}
                              </span>
                            </li>
                          ) : (
                            <li>
                              Expected arrival in{' '}
                              <span>{shipment.destinationWarehouse}</span> on{' '}
                              <span>
                                {new Date(shipment.arrivalDate).toLocaleString(
                                  'en-GB'
                                )}
                              </span>
                            </li>
                          )}
                        </ul>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
              <div className={styles.shipmentsViewBlockFooter}>
                <div className={styles.shipmentsViewBlockInfo}>
                  <span></span>Delivered
                </div>
                <div className={styles.shipmentsViewBlockInfo}>
                  <span></span>Ongoing
                </div>
              </div>
            </div>
            <ShipmentsEdit
              name={selectedShipment}
              setShipments={setShipments}
              setSelectedShipment={setSelectedShipment}
              warehouses={warehouses}
            />
          </div>
          <ShipmentsParcelManagement warehouses={warehouses} />
        </>
      )}
    </>
  );
};

export default ShipmentsView;
