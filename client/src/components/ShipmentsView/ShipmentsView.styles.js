import { makeStyles } from '@material-ui/core/styles';
import { indigo, amber, green } from '@material-ui/core/colors';

export const useStyles = makeStyles(theme => ({
  shipmentsViewContainer: {
    height: 'auto',
    width: '100%',
    display: 'flex',
    border: `1px solid ${theme.palette.divider}`,
    marginTop: '20px'
  },
  shipmentsViewBlock: {
    height: 'auto',
    width: '60%'
  },
  shipmentsViewBlockHead: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '10px'
  },
  shipmentsViewBlockHeading: {
    fontSize: '1.25rem'
  },
  shipmentsViewBlockFilters: {
    '& select:first-of-type': {
      marginRight: '10px'
    }
  },
  shipmentsViewBlockFilter: {
    padding: '5px',
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: theme.shape.borderRadius,
    outline: 'none',
    fontFamily: theme.typography.fontFamily,
    fontSize: '0.875rem',
    color: theme.palette.text.primary,
    '&:hover, &:focus': {
      borderColor: amber[500]
    },
    '&::placeholder': {
      fontFamily: theme.typography.fontFamily,
      fontSize: '0.875rem',
      color: theme.palette.text.primary
    }
  },
  shipmentsViewBlockBody: {
    display: 'grid',
    gridTemplateColumns: 'repeat(auto-fill, minmax(50px,50px))',
    gridAutoRows: '50px',
    gridGap: '15px',
    placeContent: 'center',
    padding: '15px'
  },
  shipmentsViewBlockBox: {
    position: 'relative',
    borderRadius: `calc(${theme.shape.borderRadius}*3px)`,
    backgroundColor: amber[100],
    transition: theme.transitions.create(['background-color']),
    '&:hover': {
      backgroundColor: indigo[500]
    },
    '&:hover $shipmentsViewBlockBoxData': {
      visibility: 'visible'
    },
    cursor: 'pointer'
  },
  delivered: {
    backgroundColor: amber[500]
  },
  active: {
    backgroundColor: green[500]
  },
  shipmentsViewBlockBoxData: {
    minWidth: '420px',
    position: 'absolute',
    top: '50%',
    left: '50%',
    display: 'flex',
    padding: '10px',
    borderTopRightRadius: `calc(${theme.shape.borderRadius}*3px)`,
    borderTopLeftRadius: `calc(${theme.shape.borderRadius}*3px)`,
    borderBottomLeftRadius: `calc(${theme.shape.borderRadius}*3px)`,
    backgroundColor: `${indigo[500]}E6`,
    visibility: 'hidden',
    zIndex: '999',
    cursor: 'auto'
  },
  shipmentsViewBlockBoxDataImg: {
    height: '100px',
    width: '100px',
    padding: '15px',
    marginRight: '10px',
    borderRadius: `calc(${theme.shape.borderRadius}*3px)`,
    backgroundColor: `${indigo[300]}40`
  },
  shipmentsViewBlockBoxDataInfo: {
    color: '#FFFFFF',
    '& ul': {
      padding: '0',
      margin: '0',
      listStyle: 'none',
      '& li': {
        maxWidth: '40ch',
        fontSize: '0.875rem',
        '&:first-of-type': {
          fontSize: '1rem'
        },
        '& span': {
          color: amber[500]
        }
      }
    }
  },
  shipmentsViewBlockFooter: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: '10px'
  },
  shipmentsViewBlockInfo: {
    display: 'flex',
    alignItems: 'center',
    fontSize: '0.875rem',
    '&:first-child': {
      marginRight: '10px'
    },
    '& span': {
      display: 'inline-block',
      height: '14px',
      width: '14px',
      borderRadius: theme.shape.borderRadius,
      marginRight: '5px'
    },
    '&:first-child span': {
      backgroundColor: amber[500]
    },
    '&:last-child span': {
      backgroundColor: amber[200]
    }
  }
}));
