import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FormControl from '@material-ui/core/FormControl';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Slide from '@material-ui/core/Slide';
import Grid from '@material-ui/core/Grid';
import ErrorIcon from '@material-ui/icons/Error';
import { useStyles } from './ChangePasswordDialog.styles.js';
import { useState } from 'react';
import { userValidator } from '../../common/validators.js';
import { changeUserPassword } from '../../api/user-requests.js';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const { password: passCheck  } = userValidator;

const passPrototype = {
  oldPass: {
    value:'',
    error: false,
    title: 'Old Password',
    labelWidth: 95,
    visible: false,
    errorMessage: 'min 6 characters, only latin, numbers and _',
    validateFunc: passCheck,
  },

  newPass: {
    value:'',
    error: false,
    title: 'New Password',
    labelWidth: 100,
    visible: false,
    errorMessage: 'min 6 characters, only latin, numbers and _',
    validateFunc: passCheck,
  },

  confirmPass: {
    value:'',
    error: false,
    title: 'Confirm New Password',
    labelWidth: 155,
    visible: false,
    errorMessage: `don't match to new password`,
    validateFunc: function (value) {
      return value !== this.newPass.value
    },
  },
}


const ChangePasswordDialog = (props) => {
  const { open, viewFunc, id, successMessage } = props;
  const styles = useStyles();

  const [passData, updatePassData] = useState(passPrototype);
  const [errorMessage, setErrorMessage] = useState(null);

  const viewPassword = (field, value) => updatePassData({...passData, [field]: {...passData[field], visible: value}});  

  const changeData = (field, newValue) => {
    const haveError = passData[field].validateFunc.call(passData, newValue);
    updatePassData({...passData, [field]: {...passData[field], value: newValue, error: haveError}})
  };

  const closeWindow = () => {
    updatePassData(passPrototype);
    setErrorMessage(null);
    viewFunc(!open);
  }

  const validateFields = () => {
    setErrorMessage(null)    
    const validatedData = {};

    Object.keys(passData)
      .forEach(x => validatedData[x] = {...passData[x], error: passData[x].validateFunc.call(passData, passData[x].value || x.value)});

    updatePassData({...validatedData});

    const checkFields = Object.keys(passData).map(x => validatedData[x].error);
    if (checkFields.some(x => x)) {
      return;
    }

    changePassword();
  }

  const changePassword = () => {
    const data = {
      'oldPassword': passData.oldPass.value,
      'newPassword': passData.newPass.value,
      'newPasswordConfirm': passData.confirmPass.value
    }

    changeUserPassword(id, data).then(x => {
      if (x.message) {
        setErrorMessage(x.message)
        return;
      }
      successMessage({open: true, type: 'success', message: 'Password was successful updated!'});
      closeWindow();
    });
  }

  return (
    <Dialog maxWidth='xs' open={open} TransitionComponent={Transition} keepMounted>
      <DialogTitle className={styles.dialogTitle}>Password change</DialogTitle>
        <DialogContent className={styles.dialogContext}>
          <Grid container spacing={2}>
            {
              !errorMessage
              ? null
              : (            
                <Grid className={styles.errorMessage} item xs={12}>
                  <ErrorIcon fontSize='medium' />
                  <Typography>&nbsp;{errorMessage}</Typography>
                </Grid>
                )
            }
            {
              Object.keys(passData).map(x => (
                <Grid key={x} item xs={12}>
                  <FormControl variant="outlined" error={passData[x].error} fullWidth>
                    <InputLabel className={styles.labelText}>{passData[x].title}</InputLabel>
                    <OutlinedInput
                      type={passData[x].visible ? 'text' : 'password'}
                      value={passData[x].value}
                      name={x}
                      onChange={(e) => changeData(e.target.name, e.target.value)}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton onClick={(e) => viewPassword(x, !passData[x].visible)}edge="end">
                            {passData[x].visible ? <Visibility className={styles.labelText} /> : <VisibilityOff className={styles.labelText}/>}
                          </IconButton>
                        </InputAdornment>
                      }
                      labelWidth={passData[x].labelWidth}
                    />
                    <FormHelperText className={styles.labelText}>{passData[x].error ? passData[x].errorMessage : null}</FormHelperText>
                  </FormControl>
                </Grid>
                )
              )
            }
          </Grid>
        </DialogContent>
        <DialogActions style={{justifyContent: 'center'}}>
          <Button color='primary' variant='contained' onClick={validateFields}>OK</Button>
          <Button color='secondary' variant='contained' onClick={closeWindow}>Cancel</Button>
        </DialogActions>
      </Dialog>
  )
}

export default ChangePasswordDialog;
