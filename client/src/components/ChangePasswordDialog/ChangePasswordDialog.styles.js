import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  dialogTitle: {
    textAlign: 'center',
    color: 'white',
    backgroundColor: theme.palette.primary.main
  },

  labelText: {
    color: theme.palette.primary.main,
  },

  dialogContext: {
    paddingTop: 40,
    paddingBottom: 20,
  },

  errorMessage: {
    color: 'red',
    textAlign: 'center',
    textTransform: 'uppercase',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
}));


