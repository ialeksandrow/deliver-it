import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles({
  freightTypeImage: {
    height: '50px',
    maxWidth: '100px',
    marginBottom: '10px'
  }
});
