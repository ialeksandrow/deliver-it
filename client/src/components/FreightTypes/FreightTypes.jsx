import roadFreight from './../../images/freight-types/road-freight.svg';
import oceanFreight from './../../images/freight-types/ocean-freight.svg';
import airFreight from './../../images/freight-types/air-freight.svg';
import railFreight from './../../images/freight-types/rail-freight.svg';
import { Container, Grid, Typography } from '@material-ui/core';
import { useStyles } from './FreightTypes.styles';

const FreightTypes = () => {
  const styles = useStyles();

  return (
    <Container>
      <Grid container spacing={5}>
        <Grid item xs={12} sm={6} md={3}>
          <img
            src={roadFreight}
            className={styles.freightTypeImage}
            alt="road freight"
          ></img>
          <Typography variant="h6" component="h3" gutterBottom>
            Road Freight
          </Typography>
          <Typography variant="body2">
            Our road freight products offer high quality road transportation,
            from standard services such as LTL (Less-than-Truck Load), PTL
            (Part) or FTL (Full-Truck Load) shipments to temperature controlled
            and highly secured transports.
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <img
            src={oceanFreight}
            className={styles.freightTypeImage}
            alt="ocean freight"
          ></img>
          <Typography variant="h6" component="h3" gutterBottom>
            Ocean Freight
          </Typography>
          <Typography variant="body2">
            With our broad product range we cover different equipment types and
            consolidation services to ensure your cargo reaches the right place,
            at the right time in a cost-efficient way. Highest reliability with
            space protection.
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <img
            src={airFreight}
            className={styles.freightTypeImage}
            alt="air freight"
          ></img>
          <Typography variant="h6" component="h3" gutterBottom>
            Air Freight
          </Typography>
          <Typography variant="body2">
            Working with carefully selected carriers, we operate with schedules
            on all the world’s major routes so you can plan with certainty. We
            offer flexible products which allow a choice of delivery speeds to
            best suit your requirements.
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <img
            src={railFreight}
            className={styles.freightTypeImage}
            alt="rail freight"
          ></img>
          <Typography variant="h6" component="h3" gutterBottom>
            Rail Freight
          </Typography>
          <Typography variant="body2">
            Our rail freight products offer secure, reliable and environmentally
            friendly freight transportation via rail, either throughout Europe
            or Asia, LCL (Less-than-Container Load) or FCL (Full-Container
            Load).
          </Typography>
        </Grid>
      </Grid>
    </Container>
  );
};

export default FreightTypes;
