import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
  paperStyle: {
    padding:20,
    marginTop: 20,
    borderRadius: 2,
  },

  hLine: {
    borderBottom: `1px solid ${theme.palette.primary.main}`,
    color: theme.palette.primary.main,
    paddingBottom: 5,
    marginBottom: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  iconList: {
    display: 'flex',
    justifyContent: 'flex-end',
  },

  labelPrimary: {
    color: theme.palette.primary.main,
  },

  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },

  dialogTitle: {
    textAlign: 'center',
    color: 'white',
    backgroundColor: theme.palette.primary.main
  },

  dialogText: {
    color: 'black',
    padding: 30
  },
  
  editStyle: {
    backgroundColor: theme.palette.primary[50],
  },

  titleStyle: {
    display: 'flex',
  },
  
}));
