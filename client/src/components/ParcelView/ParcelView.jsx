import React from 'react';
import { useState, useEffect, useContext } from 'react';
import { getParcelById, changeParcelStatus, updateParcel } from '../../api/parcel-requests.js';
import { useHistory } from 'react-router-dom';
import { getCategories } from '../../api/categories-requests';
import { getCountries, getCities } from '../../api/global-requests.js';
import { getWarehouses } from '../../api/warehouse-request';
import { useStyles } from './ParcelView.styles';
import AuthenticationContext from '../../contexts/AuthenticationContext.js';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import Paper from '@material-ui/core/Paper';
import ROUTER_PATHS from '../../common/router-paths';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ArrowBack';
import EditIcon from '@material-ui/icons/Edit';
import Tooltip from '@material-ui/core/Tooltip';
import DELIVERY_TYPES from '../../common/delivery-types.js';
import PARCEL_STATUSES from '../../common/parcel-statuses.js';
import USER_ROLES from '../../common/user-roles.js';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const ParcelView = (props) => {
  const { id } = props;
  const history = useHistory();
  const styles = useStyles();
  const { accountState } = useContext(AuthenticationContext);
  const operator = accountState.role === USER_ROLES.operator;
  
  const [backupInfo, setBackupInfo] = useState({});
  const [editMode, setEditMode] = useState(false);
  const [parcelData, updateParcelData] = useState(null);
  const [categories, updateCategories] = useState([]);
  const [countries, updateCountries] = useState([]);
  const [cities, updateCities] = useState([]);
  const [warehouses, updateWarehouses] = useState([]);
  const [showDialog, updateShowDialog] = useState(false);
  const [dialogData, setDialogData] = useState({ title: 'SUCCESS', message: 'none' });

  
  useEffect(() => {
    getParcelById(id).then(x => {
      updateParcelData(x);
    });

    getCategories().then(x => {
      updateCategories(x);
    });

    getCountries().then(x => {
      updateCountries(x.data);

      getCities().then(z => {
        updateCities(z.data);
        
        getWarehouses().then(a => {
          updateWarehouses(a.data);
        })
      });
    });
  }, [id])

  const enterEditMode = () => {
    if (parcelData.shipment && parcelData.shipment.statusId > 1) {
      updateShowDialog(true);
      setDialogData({ title: 'ERROR', message: 'This parcel is already in completed shipment, cannot be edited!' })
      return;
    }
    setBackupInfo({...parcelData});
    setEditMode(true);
  }

  const cancelChanges = () => {
    updateParcelData(backupInfo);
    setEditMode(false);
  };

  const changeData = (field, value) => {
    updateParcelData({...parcelData, [field]: value});
  }

  const changeCountry = (value) => {
    const startCity =  cities.filter(x => x.country === value);
    const startWarehouse = warehouses.filter(x => x.cityName === startCity[0].name);
    updateParcelData({...parcelData, deliveryCountry: value, deliveryCity: startCity[0].name, warehouseId: startWarehouse[0].id})
  }

  const changeCity = (value) => {
    const startWarehouse = warehouses.filter(x => x.cityName === value);
    updateParcelData({...parcelData, deliveryCity: value, warehouseId: startWarehouse[0].id })
  }

  const updateStatus = (status) => {
    const data = {
      "parcelId": id,
      "statusId": status
    }

    changeParcelStatus(data)
    .then(x => {
      updateParcelData({...parcelData, parcelStatusId: x.parcelStatusId})
    })
  }

  const updateParcelInfo = () => {
    if (!parcelData.parcelName.length || parcelData.parcelWeight <= 0) {
      updateShowDialog(true);
      setDialogData({ title: 'ERROR', message: 'Invalid fields!' })
      return;
    }

    const newData = {
      'parcelName': parcelData.parcelName,
      'parcelWeight': parcelData.parcelWeight,
      'parcelCategory': parcelData.parcelCategoryId,
      'deliveryTypeId': parcelData.deliveryTypeId,
      'warehouseId': parcelData.warehouseId
    }

    updateParcel(id, newData)
    .then(x => {
      if (x.message) {
        updateShowDialog(true);
        setDialogData({ title: 'ERROR', message: x.message })
        return;
      }
      setEditMode(false);
      updateParcelData(x);
      updateShowDialog(true);
      setDialogData({ title: 'SUCCESS', message: 'Parcel was successful updated' })
    });
  }

  if (!parcelData) {
    return (
      <Backdrop className={styles.backdrop} open={true}>
        <CircularProgress size={100} color="primary" />
      </Backdrop>
    )
  }

  if (parcelData.message) {
      return (
      <Dialog open={true} TransitionComponent={Transition} keepMounted>
        <DialogTitle className={styles.dialogTitle}>ERROR</DialogTitle>
        <DialogContent>
          <DialogContentText className={styles.dialogText}>{parcelData.message}</DialogContentText>
        </DialogContent>
        <DialogActions style={{justifyContent:'center'}}>
          <Button onClick={() => history.push(ROUTER_PATHS.PARCELS)} variant='contained' color='primary'>OK</Button>
        </DialogActions>
      </Dialog>
    )
  }

  if (!parcelData.warehouseId) {
    if (warehouses.length) {
      const startWarehouse = warehouses.filter(x => x.cityName === parcelData.deliveryCity);
      updateParcelData({...parcelData, warehouseId: startWarehouse[0].id, warehouseName: startWarehouse[0].name})
    }
    return (<div>wait</div>)
  }
  
  return (
    <Paper variant='outlined' square elevation={5} className={styles.paperStyle}>
      <div className={styles.hLine}>
        <div className={styles.titleStyle}>
          <Typography variant='h4' >{`Parcel #${id} Details`}</Typography>&nbsp;
          {editMode ? ' (edit mode)' : null}
        </div>
        <div className={styles.iconList}>
          {
            editMode
            ? (<>
                <Tooltip title='Confirm changes' arrow>
                  <IconButton onClick={updateParcelInfo} color='primary'>
                    <DoneIcon className={styles.primaryColorIcon} />
                  </IconButton>
                </Tooltip>
                <Tooltip title='Cancel changes' arrow>
                  <IconButton onClick={cancelChanges} color='primary'>
                    <CloseIcon className={styles.primaryColorIcon} />
                  </IconButton>
                </Tooltip>
              </>
              )
            : (<>
                <Tooltip title='Back to parcels' arrow>
                  <IconButton onClick={() => history.push(ROUTER_PATHS.PARCELS)} color='primary'>
                    <BackIcon className={styles.primaryColorIcon} />
                  </IconButton>
                </Tooltip>
                <Tooltip title='Edit mode' arrow>
                  <IconButton onClick={enterEditMode} color='primary'>
                    <EditIcon className={styles.primaryColorIcon} />
                  </IconButton>
                </Tooltip>
              </>
              )
          }
        </div>
      </div>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <TextField
            className={editMode ? styles.editStyle : null}
            fullWidth
            label='Parcel name'
            InputProps={{ readOnly: !editMode }}
            InputLabelProps={{ className: styles.labelPrimary }}
            variant='outlined'
            value={parcelData.parcelName}
            error={!parcelData.parcelName.length}
            onChange={(e) => changeData('parcelName', e.target.value)}
          />
        </Grid>
        <Grid item xs={2}>
          <TextField
            fullWidth
            className={editMode ? styles.editStyle : null}
            type='number'
            label='Weight'
            InputProps={{ readOnly: !editMode }}
            InputLabelProps={{ className: styles.labelPrimary }}
            variant='outlined'
            value={parcelData.parcelWeight}
            error={parcelData.parcelWeight <= 0}
            onChange={(e) => changeData('parcelWeight', e.target.value)}
          />
        </Grid>
        <Grid item xs={2}>
          {
            !editMode
            ? (
                <TextField
                fullWidth
                label='Category'
                InputProps={{ readOnly: true }}
                InputLabelProps={{ className: styles.labelPrimary }}
                variant='outlined'
                value={parcelData.parcelCategory}
                />
              )
            : (
                <FormControl variant="outlined" fullWidth>
                  <InputLabel className={styles.labelPrimary} >Category</InputLabel>
                  <Select
                    native
                    className={styles.editStyle}
                    readOnly
                    label='Category'
                    value={parcelData.parcelCategoryId}
                    onChange={(e) => changeData('parcelCategoryId', parseInt(e.target.value))}
                  >
                    {
                      !categories.length
                      ? null
                      : categories.map(el => <option key={el.categoryId} value={el.categoryId}>{el.categoryName}</option>)
                    }
                  </Select>
                </FormControl>
              )
          }
        </Grid>
        <Grid item xs={2}>
          {
            operator && !parcelData.shipment
            ? (
                <FormControl variant="outlined" fullWidth>
                  <InputLabel className={styles.labelPrimary} >Parcel Status</InputLabel>
                  <Select
                    native
                    readOnly
                    label='Parcel Status'
                    value={parcelData.parcelStatusId}
                    onChange={(e) => updateStatus(e.target.value)}
                  >              
                    <option value={PARCEL_STATUSES.AWAITING}>Awaiting</option>
                    <option value={PARCEL_STATUSES.RECEIVED}>Received</option>
                  </Select>
                </FormControl>
              )
            : (
                <TextField
                fullWidth
                label='Parcel Status'
                InputProps={{ readOnly: true }}
                InputLabelProps={{ className: styles.labelPrimary }}
                variant='outlined'
                value={parcelData.parcelStatus}
                />
              )
          }
        </Grid>
        <Grid item xs={3}>
          <TextField
            fullWidth
            label="Customer Name"
            InputProps={{ readOnly: true }}
            InputLabelProps={{ className: styles.labelPrimary }}
            variant="outlined"
            value={`${parcelData.userFirstName} ${parcelData.userLastName}`}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            fullWidth
            label="Customer E-mail"
            InputProps={{ readOnly: true }}
            InputLabelProps={{ className: styles.labelPrimary }}
            variant="outlined"
            value={parcelData.userEmail}
          />
        </Grid>
        <Grid item xs={3}>
          {
            editMode && parcelData.deliveryTypeId === DELIVERY_TYPES.WAREHOUSE
            ? (
              <FormControl variant='outlined' fullWidth>
              <InputLabel className={styles.labelPrimary}>Country</InputLabel>
                <Select
                  native
                  label='Country'
                  className={styles.editStyle}
                  value={parcelData.deliveryCountry}
                  onChange={(e) => changeCountry(e.target.value)}
                >
                  { countries.map(el => <option key={el.name} value={el.name}>{el.name}</option> ) }
                </Select>
              </FormControl>
              )
            : (
                <TextField
                fullWidth
                label='Country'
                InputProps={{ readOnly: true }}
                InputLabelProps={{ className: styles.labelPrimary }}
                variant='outlined'
                value={parcelData.deliveryTypeId === DELIVERY_TYPES.ADDRESS ? parcelData.userCountry : parcelData.deliveryCountry}
                />
              )
          }
        </Grid>
        <Grid item xs={3}>
          {
            editMode && parcelData.deliveryTypeId === DELIVERY_TYPES.WAREHOUSE
            ? (
                <FormControl variant="outlined" fullWidth>
                  <InputLabel className={styles.labelPrimary}>City</InputLabel>
                  <Select
                    native
                    label="City"
                    className={styles.editStyle}
                    value={parcelData.deliveryCity}
                    onChange={(e) => changeCity(e.target.value)}
                  >
                    { cities.filter(x => x.country === parcelData.deliveryCountry).map(el => <option key={el.name} value={el.name}>{el.name}</option>) }
                  </Select>
                </FormControl>
              )
            : (
                <TextField
                fullWidth
                label='City'
                InputProps={{ readOnly: true }}
                InputLabelProps={{ className: styles.labelPrimary }}
                variant="outlined"
                value={parcelData.deliveryTypeId === DELIVERY_TYPES.ADDRESS ? parcelData.userCity : parcelData.deliveryCity}
                />
              )
          }
        </Grid>
        <Grid item xs={2}>
          {
            editMode && (!parcelData.shipment || parcelData.shipment.statusId <= 1)
            ? (
                <FormControl variant="outlined" fullWidth>
                  <InputLabel className={styles.labelPrimary} >Delivery Type</InputLabel>
                  <Select
                    native
                    className={styles.editStyle}
                    readOnly
                    label='Delivery Type'
                    value={parcelData.deliveryTypeId}
                    onChange={(e) => changeData('deliveryTypeId', parseInt(e.target.value))}
                  >
                    <option value={DELIVERY_TYPES.ADDRESS}>Address</option>
                    <option value={DELIVERY_TYPES.WAREHOUSE}>Warehouse</option>
                  </Select>
                </FormControl>
              )
            : (
                <TextField
                fullWidth
                label='Delivery Type'
                InputProps={{ readOnly: true }}
                InputLabelProps={{ className: styles.labelPrimary }}
                variant="outlined"
                disabled={editMode}
                value={parcelData.deliveryType}
                />
              )
          }
        </Grid>
        <Grid item xs={4}>
          {
            editMode && parcelData.deliveryTypeId === DELIVERY_TYPES.WAREHOUSE
            ? (
                <FormControl variant="outlined" fullWidth>
                  <InputLabel className={styles.labelPrimary}>Warehouse</InputLabel>
                  <Select
                    native
                    className={styles.editStyle}
                    label="Warehouse"
                    value={parcelData.warehouseId}
                    onChange={(e) => changeData('warehouseId', +e.target.value)}
                  >
                    { 
                      warehouses.length
                      ? warehouses.filter(x => x.cityName === parcelData.deliveryCity).map(el => <option key={el.id} value={el.id}>{el.name}</option>)
                      : null
                    }
                  </Select>
                </FormControl>
              )
            : (
                <TextField
                fullWidth
                label='Delivery Address'
                InputProps={{ readOnly: true }}
                InputLabelProps={{ className: styles.labelPrimary }}
                variant='outlined'
                value={parcelData.deliveryTypeId === DELIVERY_TYPES.ADDRESS ? parcelData.userAddress : parcelData.warehouseName}
                />
              )
          }
        </Grid>
        <Grid item xs={4}>
          <TextField
            fullWidth
            label='Shipment'
            InputProps={{ readOnly: true }}
            InputLabelProps={{ className: styles.labelPrimary }}
            variant="outlined"
            value={parcelData.shipment ? parcelData.shipment.name : ' - '}
          />
        </Grid>
        <Grid item xs={2}>
          <TextField
            fullWidth
            label='Shipment Status'
            InputProps={{ readOnly: true }}
            InputLabelProps={{ className: styles.labelPrimary }}
            variant="outlined"
            value={parcelData.shipment ? parcelData.shipment.status : ' - '}
          />
        </Grid>
      </Grid>
      <Dialog open={showDialog} TransitionComponent={Transition} keepMounted>
        <DialogTitle className={styles.dialogTitle}>{dialogData.title}</DialogTitle>
        <DialogContent>
          <DialogContentText className={styles.dialogText}>{dialogData.message}</DialogContentText>
        </DialogContent>
        <DialogActions style={{justifyContent:'center'}}>
          <Button onClick={() => updateShowDialog(false)} variant='contained' color='primary'>OK</Button>
        </DialogActions>
      </Dialog>
    </Paper>
  );
}

export default ParcelView;