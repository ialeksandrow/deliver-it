import React from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import Collapse from '@material-ui/core/Collapse';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import IconButton from '@material-ui/core/IconButton';
import NativeSelect from '@material-ui/core/NativeSelect';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import Tooltip from '@material-ui/core/Tooltip';
import { useState } from 'react';
import { useStyles } from './ParcelOperatorView.styles.js';
import { Link } from 'react-router-dom';
import { withStyles } from "@material-ui/core/styles";

const bluStyle = {
  tooltip: {
    backgroundColor: '#1a237e',
    fontWeight: 'bold',
    fontSize: '0.7rem',
  },
  
  arrow: {
    color: '#1a237e',
  }
}

const BluToolTip = withStyles(bluStyle)(Tooltip);


const Row = (props) => {
  const { row, checked, updateChecked, updateStatus } = props;
  const styles = useStyles();
  const [open, setOpen] = useState(false);

  const addCheck = (id) => {
    if (checked.includes(id)) {
      updateChecked(checked.filter(x => x !== id));
    } else {
      updateChecked([...checked, id])
    }
  }

  return (    
    <React.Fragment>
      <TableRow className={!open ? styles.closedRow : styles.openRow}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon className={styles.whiteIcon} /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell>
          <Checkbox
          color={!open ?'primary' : 'secondary'}
          className={!open ? styles.checkBoxStyleDark : styles.checkBoxStyleLight}
          onChange={() => addCheck(row.parcelId)}
          checked={checked.includes(row.parcelId)}
          />
        </TableCell>
        <TableCell className={!open ? styles.textColorDark : styles.textColorWhite} align="left">
          <Link className={styles.linkStyle} to={`parcels/details/${row.parcelId}`}>{row.parcelId}</Link>
        </TableCell>
        <TableCell className={!open ? styles.textColorDark : styles.textColorWhite} align="left">
          <Link className={styles.linkStyle} to={`parcels/details/${row.parcelId}`}>{row.parcelName}</Link>
        </TableCell>
        <TableCell className={!open ? styles.textColorDark : styles.textColorWhite} align="left">
          <Link className={styles.linkStyle} to={`user-profile/${row.userId}`}>{row.userFirstName} {row.userLastName}</Link>
        </TableCell>
        <TableCell className={!open ? styles.textColorDark : styles.textColorWhite} align="left">{row.deliveryType}</TableCell>
        <TableCell className={!open ? styles.textColorDark : styles.textColorWhite} align="center">{row.shipment ? row.shipment.name : ' - '}</TableCell>
        <TableCell align='left'>
        {
          row.shipment
          ? (
              <BluToolTip arrow title={`Can't change status because already is part of shipment`}>
                <span className={!open ? styles.textColorDark : styles.textColorWhite}>{row.parcelStatus}</span>
              </BluToolTip>
            )
          : (
              <NativeSelect
              style={{font:'inherit'}}
              value={row.parcelStatusId}
              className={!open ? styles.textColorDark : styles.textColorWhite}
              onChange={(event) => updateStatus(row.parcelId, event.target.value)}
              disableUnderline
              classes={{
                icon: !open ? styles.darkIcon : styles.whiteIcon
              }}
              >          
                <option value={1} style={{color:'black'}}>Awaiting</option>
                <option value={2} style={{color:'black'}}>Received</option>
              </NativeSelect>
            )
        }
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ padding: 0}} colSpan={8}>
          <Collapse in={open} timeout="auto" unmountOnExit >
            <Box margin={0}>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow className={styles.hiddenTableHead}>
                    <TableCell className={styles.headFonts}>Weight</TableCell>
                    <TableCell className={styles.headFonts}>Category</TableCell>
                    <TableCell className={styles.headFonts}>Country</TableCell>
                    <TableCell className={styles.headFonts}>City</TableCell>
                    <TableCell className={styles.headFonts}>User E-mail</TableCell>
                    <TableCell className={styles.headFonts}>Address</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow className={styles.hiddenTableData}>
                    <TableCell>{row.parcelWeight}</TableCell>
                    <TableCell>{row.parcelCategory}</TableCell>
                    <TableCell>{row.deliveryCountry}</TableCell>
                    <TableCell>{row.deliveryCity}</TableCell>
                    <TableCell>{row.userEmail}</TableCell>
                    <TableCell>{row.deliveryTypeId === 1 ? row.userAddress : row.warehouseName}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
              {
                !row.shipment
                ? null
                : (
                  <Table size="small">
                  <TableHead>
                    <TableRow className={styles.hiddenTableHead}>
                    <TableCell className={styles.headFonts} align='center' colSpan={5}>SHIPMENT INFO</TableCell>                    
                    </TableRow>
                    <TableRow className={styles.hiddenTableHead}>
                      <TableCell className={styles.headFonts}>Departure Date</TableCell>
                      <TableCell className={styles.headFonts}>Arrival Date</TableCell>
                      <TableCell className={styles.headFonts}>Origin Warehouse</TableCell>
                      <TableCell className={styles.headFonts}>Destination Warehouse</TableCell>
                      <TableCell className={styles.headFonts}>Status</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow className={styles.hiddenTableData}>
                      <TableCell>{new Date(row.shipment.departureDate).toLocaleDateString('fr-CA')}</TableCell>
                      <TableCell>{new Date(row.shipment.arrivalDate).toLocaleDateString('fr-CA')}</TableCell>
                      <TableCell>{row.shipment.originWarehouse}</TableCell>
                      <TableCell>{row.shipment.destinationWarehouse}</TableCell>
                      <TableCell>{row.shipment.status}</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
                  )
              }
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

Row.propTypes = {
  row: PropTypes.object.isRequired,
  checked: PropTypes.array.isRequired,
  updateChecked: PropTypes.func.isRequired,
  updateStatus: PropTypes.func.isRequired,
};

export default Row;