import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  closedRow: {
    '& > *': {
      borderBottom: 'unset',
    },
  },

  openRow: {
    '& > *': {
      borderBottom: 'unset',
    },
    backgroundColor: theme.palette.primary.main
  },

  checkBoxStyleDark: {
    color: 'grey'
  },

  checkBoxStyleLight: {
    color: 'white'
  },

  tableHead: {
    borderBottom: `1px solid ${theme.palette.primary.main}`,
    paddingBottom: 10,
    color: theme.palette.primary.main,
    display: 'flex',
    justifyContent: 'space-between',

  },

  paperStyle: {
    padding: 20,
    marginTop: 20,
  },

  filterHead: {
    borderBottom: `1px solid ${theme.palette.primary.main}`,
    paddingBottom: 10,
    color: theme.palette.primary.main,
    display: 'flex',
    justifyContent: 'space-between',
  },

  iconList: {
    display: 'flex',
    justifyContent: 'flex-end',
  },

  hiddenTableHead: {
    backgroundColor: theme.palette.secondary.main
  },

  hiddenTableData: {
    backgroundColor: theme.palette.secondary.light
  },

  headFonts: {
    fontWeight: 'bold'
  },

  textColorDark: {
    color: '#333333'
  },

  textColorWhite: {
    color: 'white'
  },  

  whiteIcon: {
    color: 'white'
  },

  darkIcon: {},

  accordionStyle: {
    paddingLeft: 2,
    paddingRight: 2,
    paddingBottom: 10,
    paddingTop: 0
  },

  filterButton: {
    paddingLeft: 3,
    paddingRight: 3
  },

  tableRoot: {
    color: theme.palette.primary.main,
    fontWeight: 'bold',
    
    '&:hover': {
      color: theme.palette.primary.dark,
    },

    '&:focus': {
      color: theme.palette.primary.dark,
    },
  },

  tableActive: { 
    color: theme.palette.primary.dark,
  },

  tableIcon: {
    color: theme.palette.primary.main,
  },

  linkStyle: {
    textDecoration: 'none',
    color: 'inherit'
  },
}));


