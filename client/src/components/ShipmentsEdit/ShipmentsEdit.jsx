import { useState, useEffect } from 'react';
import { BASE_URL } from './../../common/constants';
import SHIPMENT_STATUSES from './../../common/shipment-statuses';
import {
  FormControl,
  InputLabel,
  OutlinedInput,
  IconButton,
  Select,
  MenuItem,
  Button,
  CircularProgress
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import SaveIcon from '@material-ui/icons/Save';
import classes from './../../utils/use-design.styles';
import { useStyles } from './ShipmentsEdit.styles';

const ShipmentsEdit = ({
  name,
  setShipments,
  setSelectedShipment,
  warehouses
}) => {
  const [shipment, setShipment] = useState(null);
  const [updatedShipmentData, setUpdatedShipmentData] = useState(null);
  const styles = useStyles();

  // Form Fields Styles
  const InputLabelStyleLight = classes.InputLabelStyleLight();
  const InputStyleLight = classes.InputStyleLight();
  const SelectStyle = classes.SelectStyle();

  useEffect(() => {
    if (!name) {
      return;
    }

    fetch(`${BASE_URL}/shipments?name=${name}`)
      .then(res => res.json())
      .then(resData => {
        setUpdatedShipmentData({
          departureDate: new Date(resData.data.departureDate)
            .toLocaleString('fr-CA')
            .split(',')[0],
          arrivalDate: new Date(resData.data.arrivalDate)
            .toLocaleString('fr-CA')
            .split(',')[0],
          userId: resData.data.userId,
          originId: resData.data.originId,
          destinationId: resData.data.destinationId,
          shipmentStatusId: resData.data.shipmentStatusId
        });

        return setShipment(resData.data);
      })
      .catch(err => console.error(err));
  }, [name]);

  const handleShipmentDelete = (shipmentName, shipmentId) => {
    fetch(`${BASE_URL}/shipments/${shipmentId}`, {
      method: 'DELETE'
    })
      .then(res => res.json())
      .then(resData => {
        if (resData.msg) {
          return console.error(resData.msg);
        }

        setShipments(shipments =>
          shipments.filter(shipment => shipment.name !== shipmentName)
        );

        return setSelectedShipment('');
      })
      .catch(err => console.error(err));
  };

  const handleShipmentUpdate = shipmentId => {
    if (
      isNaN(Date.parse(updatedShipmentData.departureDate)) ||
      isNaN(Date.parse(updatedShipmentData.arrivalDate))
    )
      return console.error('Invalid date.');

    fetch(`${BASE_URL}/shipments/${shipmentId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(updatedShipmentData)
    })
      .then(res => res.json())
      .then(resData => {
        if (resData.msg) {
          return console.error(resData.msg);
        }

        setShipments(shipments =>
          shipments.map(shipment => {
            if (shipment.name === resData.data.name) {
              return { ...shipment, ...resData.data };
            }

            return shipment;
          })
        );

        return setSelectedShipment('');
      })
      .catch(err => console.error(err));
  };

  return (
    <>
      {name && shipment && updatedShipmentData ? (
        <div className={styles.shipmentsEditBlock}>
          <div className={styles.shipmentsEditBlockHead}>
            <div className={styles.shipmentsEditBlockHeading}>
              Shipment Configuration
            </div>
            <IconButton
              className={styles.shipmentEditBlockIcon}
              onClick={() => handleShipmentDelete(shipment.name, shipment.id)}
            >
              <DeleteIcon />
            </IconButton>
          </div>
          <form
            noValidate
            autoComplete="off"
            className={styles.shipmentEditBlockBody}
          >
            <FormControl variant="outlined">
              <InputLabel
                htmlFor="departureDate"
                classes={InputLabelStyleLight}
              >
                Departure Date
              </InputLabel>
              <OutlinedInput
                type="text"
                name="departureDate"
                classes={InputStyleLight}
                value={updatedShipmentData.departureDate}
                onChange={e =>
                  setUpdatedShipmentData(previousData => ({
                    ...previousData,
                    departureDate: e.target.value
                  }))
                }
              />
            </FormControl>
            <FormControl variant="outlined">
              <InputLabel htmlFor="arrivalDate" classes={InputLabelStyleLight}>
                Arrival Date
              </InputLabel>
              <OutlinedInput
                type="text"
                name="arrivalDate"
                classes={InputStyleLight}
                value={updatedShipmentData.arrivalDate}
                onChange={e =>
                  setUpdatedShipmentData(previousData => ({
                    ...previousData,
                    arrivalDate: e.target.value
                  }))
                }
              />
            </FormControl>
            <FormControl variant="outlined">
              <InputLabel
                htmlFor="originWarehouseId"
                classes={InputLabelStyleLight}
              >
                Origin Warehouse
              </InputLabel>
              <Select
                labelId="originWarehouseId"
                name="originWarehouseId"
                input={<OutlinedInput classes={InputStyleLight} />}
                classes={SelectStyle}
                value={updatedShipmentData.originId}
                onChange={e =>
                  setUpdatedShipmentData(previousData => ({
                    ...previousData,
                    originId: e.target.value
                  }))
                }
              >
                {warehouses.map(warehouse => (
                  <MenuItem key={warehouse.name} value={warehouse.id}>
                    {warehouse.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <FormControl variant="outlined">
              <InputLabel
                htmlFor="destinationWarehouseId"
                classes={InputLabelStyleLight}
              >
                Destination Warehouse
              </InputLabel>
              <Select
                labelId="destinationWarehouseId"
                name="destinationWarehouseId"
                input={<OutlinedInput classes={InputStyleLight} />}
                classes={SelectStyle}
                value={updatedShipmentData.destinationId}
                onChange={e =>
                  setUpdatedShipmentData(previousData => ({
                    ...previousData,
                    destinationId: e.target.value
                  }))
                }
              >
                {warehouses.map(warehouse => (
                  <MenuItem key={warehouse.name} value={warehouse.id}>
                    {warehouse.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <FormControl variant="outlined">
              <InputLabel htmlFor="forepersonId" classes={InputLabelStyleLight}>
                Foreperson ID
              </InputLabel>
              <OutlinedInput
                type="text"
                name="forepersonId"
                classes={InputStyleLight}
                value={updatedShipmentData.userId}
                onChange={e =>
                  setUpdatedShipmentData(previousData => ({
                    ...previousData,
                    userId: e.target.value
                  }))
                }
              />
            </FormControl>
            <FormControl variant="outlined">
              <InputLabel
                htmlFor="shipmentStatusId"
                classes={InputLabelStyleLight}
              >
                Shipment Status
              </InputLabel>
              <Select
                labelId="shipmentStatusId"
                name="shipmentStatusId"
                input={<OutlinedInput classes={InputStyleLight} />}
                classes={SelectStyle}
                value={updatedShipmentData.shipmentStatusId}
                onChange={e =>
                  setUpdatedShipmentData(previousData => ({
                    ...previousData,
                    shipmentStatusId: e.target.value
                  }))
                }
              >
                {Object.entries(SHIPMENT_STATUSES).map(([id, name]) => (
                  <MenuItem key={id} value={id}>
                    {name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </form>
          <div className={styles.shipmentsEditBlockFooter}>
            <abbr title="Shipment Unique Number">{shipment.name}</abbr>
            <Button
              variant="contained"
              color="secondary"
              startIcon={<SaveIcon />}
              onClick={() => handleShipmentUpdate(shipment.id)}
            >
              Save Changes
            </Button>
          </div>
        </div>
      ) : (
        <div className={styles.shipmentsEditLoadingBlock}>
          <CircularProgress color="secondary" />
          <div className={styles.shipmentsEditLoadingBlockHeading}>
            Select a cell with shipment from the left pane to configure its
            details...
          </div>
        </div>
      )}
    </>
  );
};

export default ShipmentsEdit;
