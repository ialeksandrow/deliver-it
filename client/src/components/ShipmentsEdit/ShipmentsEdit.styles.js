import { makeStyles } from '@material-ui/styles';
import { red } from '@material-ui/core/colors';

export const useStyles = makeStyles(theme => ({
  shipmentsEditBlock: {
    height: 'auto',
    width: '40%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingLeft: '30px'
  },
  shipmentsEditBlockHead: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '10px'
  },
  shipmentsEditBlockHeading: {
    fontSize: '1.25rem'
  },
  shipmentEditBlockIcon: {
    padding: '0',
    transition: theme.transitions.create(['color']),
    '&:hover': {
      color: red[500]
    }
  },
  shipmentEditBlockBody: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    padding: '0 10px',
    '& > *': {
      flexBasis: '49%',
      marginBottom: '2%'
    }
  },
  shipmentsEditBlockFooter: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    padding: '10px'
  },
  shipmentsEditLoadingBlock: {
    height: 'auto',
    width: '40%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  shipmentsEditLoadingBlockHeading: {
    width: '50ch',
    textAlign: 'center'
  }
}));
