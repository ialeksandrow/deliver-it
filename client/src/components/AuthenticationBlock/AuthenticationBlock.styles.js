import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  AuthenticationBlockContainer: {
    height: '100vh',
    width: '100vw',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  AuthenticationBlock: {
    minHeight: options => options.minHeight,
    width: '1000px',
    display: 'flex'
  },
  AuthenticationBlockImage: {
    height: 'auto',
    width: '40%',
    borderTopLeftRadius: `calc(${theme.shape.borderRadius}*10px)`,
    borderBottomLeftRadius: `calc(${theme.shape.borderRadius}*10px)`,
    backgroundImage: options => `url(${options.image})`,
    backgroundPosition: 'center center',
    backgroundRepeat: 'no-repeat',
    backgroundColor: theme.palette.secondary.main
  },
  AuthenticationBlockForm: {
    height: 'auto',
    width: '60%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '30px',
    borderTopRightRadius: `calc(${theme.shape.borderRadius}*10px)`,
    backgroundColor: theme.palette.primary.main
  },
  AuthenticationBlockIconButton: {
    width: 'auto',
    alignSelf: 'flex-end',
    borderRadius: '50%',
    backgroundColor: '#5c6bc0'
  },
  AuthenticationBlockIcon: {
    color: theme.palette.text.secondary
  },
  AuthenticationBlockSubtitle: {
    fontSize: '1rem',
    color: theme.palette.text.secondary
  },
  AuthenticationBlockCta: {
    fontSize: '1rem',
    color: theme.palette.text.secondary,
    '& > a': {
      color: theme.palette.secondary.main
    }
  }
}));
