import sectionThree from './../../images/sections/section-three.png';
import { Link, useHistory, useLocation } from 'react-router-dom';
import Brand from './../Brand/Brand';
import ROUTER_PATHS from './../../common/router-paths';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ArrowBack';
import { useStyles } from './AuthenticationBlock.styles';

const AuthenticationBlock = ({
  children,
  minHeight = '500px',
  image = sectionThree
}) => {
  const { pathname } = useLocation();
  const history = useHistory();
  const styles = useStyles({ minHeight, image });

  return (
    <main className={styles.AuthenticationBlockContainer}>
      <div className={styles.AuthenticationBlock}>
        <div className={styles.AuthenticationBlockImage}></div>
        <div className={styles.AuthenticationBlockForm}>
          <div className={styles.AuthenticationBlockIconButton}>
            <IconButton onClick={() => history.push('/')}>
              <BackIcon className={styles.AuthenticationBlockIcon} />
            </IconButton>
          </div>
          <Brand options={{ margin: '0' }} />
          <div className={styles.AuthenticationBlockSubtitle}>
            Your Partner In International Delivery
          </div>
          {children}
          {pathname === ROUTER_PATHS.SIGN_IN && (
            <div className={styles.AuthenticationBlockCta}>
              Don't have an account? <Link to="/sign-up">Sign Up</Link>
            </div>
          )}
          {pathname === ROUTER_PATHS.SIGN_UP && (
            <div className={styles.AuthenticationBlockCta}>
              Already have an account? <Link to="/sign-in">Sign In</Link>
            </div>
          )}
        </div>
      </div>
    </main>
  );
};

export default AuthenticationBlock;
