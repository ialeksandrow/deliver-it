import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import { Paper, FormControl, Button } from '@material-ui/core';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import { useStyles } from './UserRegister.styles';
import { useState } from 'react';


const registerValidator = {
  email: (value) => (RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/).test(value)),
  // password: validatePassword,
  // firstName: validateFirstName,
  // lastName: validateLastName,
  // city: validateNumber,
  // address: validateAddress,
}




const UserRegister = () => {

  const data = {
    email: '',
    emailValidate: false,
  }

  // const [countries, updateCountries] = useState([]);
  // const [cities, updateCities] = useState([]);
  const [userData, updateUserData] = useState(data);
  const styles = useStyles();

  const changeEmail = (value) => {
    const error = value ? !registerValidator.email(value) : false;
    updateUserData({...userData, email: value, emailValidate: error})
  }





  return (
    <div>
      <Paper variant='outlined' square elevation={5} className={styles.paperStyle}>
        <div className={styles.hLine}>
          <AccountBoxIcon color='primary' fontSize='large' className={styles.icon}/>
          <Typography variant='h4' >Register new user</Typography>
        </div>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  error={userData.emailValidate}
                  required
                  label="E-mail"
                  variant="outlined"
                  value={userData.email}
                  placeholder='Your e-mail address'
                  InputLabelProps={{className: styles.inputCaption}}
                  onChange={(e) => changeEmail(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  required
                  type="password"
                  variant="outlined"
                  label="Password"
                  placeholder='Your password'
                  InputLabelProps={{className: styles.inputCaption}}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  required
                  type="password"
                  variant="outlined"
                  label="Confirm Password"
                  placeholder='Confirm password'
                  InputLabelProps={{className: styles.inputCaption}}
                />
              </Grid>
              <Grid item xs={12}>
                <input
                  className={styles.hiddenInput}
                  accept="image/*"                
                  id="contained-button-file"
                  type="file"
                />
                <label htmlFor="contained-button-file">
                  <Button variant="contained" color="primary" component="span">Choose avatar</Button>
                </label>
                <Typography variant='caption' className={styles.captionCustom}>no file chosen</Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  required
                  variant="outlined"
                  label="First Name"
                  placeholder='Your First name'
                  InputLabelProps={{className: styles.inputCaption}}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  required
                  variant="outlined"
                  label="Last Name"
                  placeholder='Your Last name'
                  InputLabelProps={{className: styles.inputCaption}}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <FormControl variant="outlined" fullWidth required>
                  <InputLabel className={styles.inputCaption}>Country</InputLabel>
                  <Select
                    native
                    label="Country *"
                  >
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={6}>
                <FormControl variant="outlined" fullWidth required>
                  <InputLabel className={styles.inputCaption}>City</InputLabel>
                  <Select
                    native
                    label="City *"
                  >
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  required
                  variant="outlined"
                  label="Address"
                  placeholder='Your address'
                  InputLabelProps={{className: styles.inputCaption}}
                />
              </Grid>
            </Grid>     
          </Grid>
        </Grid>
      </Paper>
    </div>
  )
}





export default UserRegister;