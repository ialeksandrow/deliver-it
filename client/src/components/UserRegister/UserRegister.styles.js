import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  paperStyle: {
    padding: 20,
    marginTop: 20,
  },
  
  hLine: {
    borderBottom: `1px solid ${theme.palette.primary.main}`,
    color: theme.palette.primary.main,
    paddingBottom: 5,
    marginBottom: 20,
    display: 'flex',
    alignItems: 'center'
  },

  icon: {
    marginRight: 4
  },

  inputCaption: {
    color: theme.palette.primary.main
  },

  hiddenInput: {
    display: 'none'
  },

  captionCustom: {
    marginLeft: 10,
    fontSize: '0.9em'
  }
}));
