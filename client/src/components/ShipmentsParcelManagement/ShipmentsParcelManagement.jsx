import { useState, useEffect } from 'react';
import { BASE_URL } from './../../common/constants';
import SHIPMENT_STATUSES from './../../common/shipment-statuses';
import {
  FormControl,
  InputLabel,
  OutlinedInput,
  Select,
  MenuItem,
  Button,
  CircularProgress
} from '@material-ui/core';
import UserIcon from '@material-ui/icons/AccountBox';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import classes from './../../utils/use-design.styles';
import { useStyles } from './ShipmentsParcelManagement.styles';

const ShipmentsParcelManagement = ({ warehouses }) => {
  const [parcels, setParcels] = useState(null);
  const [selectedParcels, setSelectedParcels] = useState([]);
  const [updatedShipmentData, setUpdatedShipmentData] = useState({
    departureDate: '',
    arrivalDate: '',
    userId: '',
    originId: '',
    destinationId: '',
    shipmentStatusId: ''
  });
  const styles = useStyles();

  // Form Fields Styles
  const InputLabelStyleLight = classes.InputLabelStyleLight();
  const InputStyleLight = classes.InputStyleLight();
  const SelectStyle = classes.SelectStyle();

  useEffect(() => {
    fetch(`${BASE_URL}/parcels`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(resData =>
        setParcels(
          resData.filter(
            element => Date.parse(element.shipment?.arrivalDate) < Date.now()
          )
        )
      )
      .catch(err => console.error(err));
  }, []);

  const handleShipmenManagement = () => {
    fetch(`${BASE_URL}/shipments`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(updatedShipmentData)
    })
      .then(res => res.json())
      .then(resData => {
        return Promise.all(
          selectedParcels.map(parcel => {
            return fetch(`${BASE_URL}/parcels/shipment`, {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
              },
              body: JSON.stringify({
                parcelId: parcel.parcelId,
                shipmentId: resData.data.id
              })
            })
              .then(managedParcel => managedParcel.json())
              .catch(err => console.error(err));
          })
        );
      })
      .then(managedParcels => {
        setParcels(parcels => [...parcels, ...managedParcels]);

        return setSelectedParcels([]);
      })
      .catch(err => console.error(err));
  };

  return (
    <>
      {parcels && (
        <div className={styles.shipmentsParcelManagementContainer}>
          <div className={styles.shipmentsParcelManagementLeftBlock}>
            {parcels.map(parcel => (
              <div
                key={parcel.parcelId}
                className={styles.shipmentsParcelManagementParcelCard}
              >
                <div className={styles.shipmentsParcelManagementParcelCardHead}>
                  <span>{parcel.parcelName}</span>
                  <div>
                    <span>{parcel.shipment.shipmentId}</span>
                    <span
                      onClick={() => {
                        setParcels(parcels =>
                          parcels.filter(
                            selectedParcel =>
                              selectedParcel.parcelId !== parcel.parcelId
                          )
                        );

                        return setSelectedParcels([...selectedParcels, parcel]);
                      }}
                    >
                      <ArrowForwardIcon />
                    </span>
                  </div>
                </div>
                <div className={styles.shipmentsParcelManagementParcelCardBody}>
                  <span>Category: {parcel.parcelCategory}</span>
                  <span>Weight: {parcel.parcelWeight}</span>
                  <span>Status: {parcel.parcelStatus}</span>
                </div>
                <div
                  className={styles.shipmentsParcelManagementParcelCardFooter}
                >
                  <span>
                    <UserIcon />
                  </span>
                  <span>
                    {`${parcel.userFirstName} ${parcel.userLastName}`} <br />
                    {parcel.userEmail} <br />
                    {parcel.userAddress} <br />
                    {`${parcel.userCity}, ${parcel.userCountry}`}
                  </span>
                </div>
              </div>
            ))}
          </div>
          <div className={styles.shipmentsParcelManagementRightBlock}>
            {selectedParcels.length ? (
              <>
                <div className={styles.shipmentsParcelManagementHeading}>
                  Create a Shipment
                </div>
                <form className={styles.shipmentsParcelManagementForm}>
                  <FormControl variant="outlined">
                    <InputLabel
                      htmlFor="departureDate"
                      classes={InputLabelStyleLight}
                    >
                      Departure Date
                    </InputLabel>
                    <OutlinedInput
                      type="text"
                      name="departureDate"
                      classes={InputStyleLight}
                      value={updatedShipmentData.departureDate}
                      onChange={e =>
                        setUpdatedShipmentData(previousData => ({
                          ...previousData,
                          departureDate: e.target.value
                        }))
                      }
                    />
                  </FormControl>
                  <FormControl variant="outlined">
                    <InputLabel
                      htmlFor="arrivalDate"
                      classes={InputLabelStyleLight}
                    >
                      Arrival Date
                    </InputLabel>
                    <OutlinedInput
                      type="text"
                      name="arrivalDate"
                      classes={InputStyleLight}
                      value={updatedShipmentData.arrivalDate}
                      onChange={e =>
                        setUpdatedShipmentData(previousData => ({
                          ...previousData,
                          arrivalDate: e.target.value
                        }))
                      }
                    />
                  </FormControl>
                  <FormControl variant="outlined">
                    <InputLabel
                      htmlFor="originWarehouseId"
                      classes={InputLabelStyleLight}
                    >
                      Origin Warehouse
                    </InputLabel>
                    <Select
                      labelId="originWarehouseId"
                      name="originWarehouseId"
                      input={<OutlinedInput classes={InputStyleLight} />}
                      classes={SelectStyle}
                      value={updatedShipmentData.originId}
                      onChange={e =>
                        setUpdatedShipmentData(previousData => ({
                          ...previousData,
                          originId: e.target.value
                        }))
                      }
                    >
                      {warehouses.map(warehouse => (
                        <MenuItem key={warehouse.name} value={warehouse.id}>
                          {warehouse.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                  <FormControl variant="outlined">
                    <InputLabel
                      htmlFor="destinationWarehouseId"
                      classes={InputLabelStyleLight}
                    >
                      Destination Warehouse
                    </InputLabel>
                    <Select
                      labelId="destinationWarehouseId"
                      name="destinationWarehouseId"
                      input={<OutlinedInput classes={InputStyleLight} />}
                      classes={SelectStyle}
                      value={updatedShipmentData.destinationId}
                      onChange={e =>
                        setUpdatedShipmentData(previousData => ({
                          ...previousData,
                          destinationId: e.target.value
                        }))
                      }
                    >
                      {warehouses.map(warehouse => (
                        <MenuItem key={warehouse.name} value={warehouse.id}>
                          {warehouse.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                  <FormControl variant="outlined">
                    <InputLabel
                      htmlFor="forepersonId"
                      classes={InputLabelStyleLight}
                    >
                      Foreperson ID
                    </InputLabel>
                    <OutlinedInput
                      type="text"
                      name="forepersonId"
                      classes={InputStyleLight}
                      value={updatedShipmentData.userId}
                      onChange={e =>
                        setUpdatedShipmentData(previousData => ({
                          ...previousData,
                          userId: e.target.value
                        }))
                      }
                    />
                  </FormControl>
                  <FormControl variant="outlined">
                    <InputLabel
                      htmlFor="shipmentStatusId"
                      classes={InputLabelStyleLight}
                    >
                      Shipment Status
                    </InputLabel>
                    <Select
                      labelId="shipmentStatusId"
                      name="shipmentStatusId"
                      input={<OutlinedInput classes={InputStyleLight} />}
                      classes={SelectStyle}
                      value={updatedShipmentData.shipmentStatusId}
                      onChange={e =>
                        setUpdatedShipmentData(previousData => ({
                          ...previousData,
                          shipmentStatusId: e.target.value
                        }))
                      }
                    >
                      {Object.entries(SHIPMENT_STATUSES).map(([id, name]) => (
                        <MenuItem key={id} value={id}>
                          {name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </form>
                <div
                  className={styles.shipmentsParcelManagementDropParcelBlock}
                >
                  {selectedParcels.map(parcel => (
                    <div
                      key={parcel.parcelId}
                      className={styles.shipmentsParcelManagementParcelCard}
                    >
                      <div
                        className={
                          styles.shipmentsParcelManagementParcelCardHead
                        }
                      >
                        <span>{parcel.parcelName}</span>
                        <div>
                          <span>{parcel.shipment.shipmentId}</span>
                          <span
                            onClick={() => {
                              setParcels(parcels => [...parcels, parcel]);

                              return setSelectedParcels(selectedParcels =>
                                selectedParcels.filter(
                                  selectedParcel =>
                                    selectedParcel.parcelId !== parcel.parcelId
                                )
                              );
                            }}
                          >
                            <ArrowBackIcon />
                          </span>
                        </div>
                      </div>
                      <div
                        className={
                          styles.shipmentsParcelManagementParcelCardBody
                        }
                      >
                        <span>Category: {parcel.parcelCategory}</span>
                        <span>Weight: {parcel.parcelWeight}</span>
                        <span>Status: {parcel.parcelStatus}</span>
                      </div>
                      <div
                        className={
                          styles.shipmentsParcelManagementParcelCardFooter
                        }
                      >
                        <span>
                          <UserIcon />
                        </span>
                        <span>
                          {`${parcel.userFirstName} ${parcel.userLastName}`}{' '}
                          <br />
                          {parcel.userEmail} <br />
                          {parcel.userAddress} <br />
                          {`${parcel.userCity}, ${parcel.userCountry}`}
                        </span>
                      </div>
                    </div>
                  ))}
                  <Button
                    variant="contained"
                    color="secondary"
                    className={styles.shipmentsParcelManagementButton}
                    startIcon={<ArrowUpwardIcon />}
                    onClick={handleShipmenManagement}
                  >
                    Create Shipment
                  </Button>
                </div>
              </>
            ) : (
              <div className={styles.shipmentsParcelManagementLoadingBlock}>
                <CircularProgress color="secondary" />
                <div
                  className={
                    styles.shipmentsParcelManagementLoadingBlockHeading
                  }
                >
                  Select a parcel from the left pane to attach as an item of the
                  current shipment...
                </div>
              </div>
            )}
          </div>
        </div>
      )}
    </>
  );
};

export default ShipmentsParcelManagement;
