import { makeStyles } from '@material-ui/styles';
import { amber } from '@material-ui/core/colors';

export const useStyles = makeStyles(theme => ({
  shipmentsParcelManagementContainer: {
    height: 'auto',
    width: '100%',
    display: 'flex',
    border: `1px solid ${theme.palette.divider}`,
    margin: '20px 0',
    overflowY: 'hidden'
  },
  shipmentsParcelManagementLeftBlock: {
    height: 'auto',
    width: '60%',
    display: 'grid',
    gridTemplateColumns: 'repeat(2, 1fr)',
    gridGap: '10px',
    padding: '10px'
  },
  shipmentsParcelManagementParcelCard: {
    padding: '10px',
    borderRadius: `calc(${theme.shape.borderRadius}*3px)`,
    backgroundColor: amber[200]
  },
  shipmentsParcelManagementParcelCardHead: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    fontSize: '1rem',
    '& span:first-of-type': {
      maxWidth: '30ch'
    },
    '& div': {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
    },
    '& div span + span': {
      marginLeft: '10px'
    },
    '& div span': {
      height: '2rem',
      width: '2rem',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: amber[100],
      transition: theme.transitions.create('background-color')
    },
    '& div span:last-of-type': {
      cursor: 'pointer'
    },
    '& div span:last-of-type:hover': {
      backgroundColor: amber[500]
    }
  },
  shipmentsParcelManagementParcelCardBody: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: '10px 0',
    '& span': {
      textDecoration: 'underline dotted'
    }
  },
  shipmentsParcelManagementParcelCardFooter: {
    display: 'flex',
    alignItems: 'center',
    padding: '10px',
    borderRadius: `calc(${theme.shape.borderRadius}*3px)`,
    backgroundColor: amber[100],
    '& span:first-of-type': {
      marginRight: '10px'
    },
    '& span:first-of-type svg': {
      height: '64px',
      width: '64px',
      color: amber[200]
    }
  },
  shipmentsParcelManagementRightBlock: {
    height: 'auto',
    width: '40%',
    padding: '10px 10px 10px 40px'
  },
  shipmentsParcelManagementHeading: {
    marginBottom: '10px',
    fontSize: '1.25rem'
  },
  shipmentsParcelManagementForm: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    '& > *': {
      flexBasis: '49%',
      marginBottom: '2%'
    }
  },
  shipmentsParcelManagementDropParcelBlock: {
    height: '100%',
    width: '100%',
    padding: '10px',
    borderTopRightRadius: `calc(${theme.shape.borderRadius}*3px)`,
    borderTopLeftRadius: `calc(${theme.shape.borderRadius}*3px)`,
    marginTop: '10px',
    background: '#ECECEC',
    '& > * + *': {
      marginTop: '10px'
    }
  },
  shipmentsParcelManagementLoadingBlock: {
    height: '300px',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  shipmentsParcelManagementLoadingBlockHeading: {
    width: '50ch',
    textAlign: 'center'
  },
  shipmentsParcelManagementButton: {
    height: 'auto',
    width: '100%'
  }
}));
