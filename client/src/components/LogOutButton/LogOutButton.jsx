import IconButton from '@material-ui/core/IconButton';
import ExitIcon from '@material-ui/icons/ExitToApp';
import { useHistory } from 'react-router-dom';

const LogOutButton = ({ setAccountState }) => {
  const history = useHistory();

  return (
    <IconButton
      color="inherit"
      onClick={() => {
        localStorage.removeItem('token');
        setAccountState(null);

        return history.push('/sign-in');
      }}
    >
      <ExitIcon />
    </IconButton>
  );
};

export default LogOutButton;
