import { makeStyles } from '@material-ui/styles';

export const useStyle = makeStyles(theme => ({
  shipmentBlock: {
    height: '100%',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FAFAFA'
  },
  shipmentDocument: {
    height: '600px',
    width: '900px',
    display: 'flex'
  },
  shipmentDocumentLeft: {
    width: '40%',
    padding: '0 30px',
    borderTopLeftRadius: `calc(${theme.shape.borderRadius}*10px)`,
    borderBottomLeftRadius: `calc(${theme.shape.borderRadius}*10px)`,
    backgroundColor: theme.palette.secondary.main
  },
  shipmentDocumentRight: {
    width: '60%',
    padding: '0 30px',
    borderTopRightRadius: `calc(${theme.shape.borderRadius}*10px)`,
    backgroundColor: theme.palette.primary.main
  },
  shipmentDocumentInfo: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    padding: '0',
    margin: '0',
    listStyle: 'none'
  },
  shipmentDocumentInfoItem: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    borderTop: '1px solid #FFA000',
    color: '#333333',
    '&:first-of-type': {
      width: '15ch',
      borderTop: 'none',
      fontSize: '1.5rem',
      color: '#333333'
    },
    '@media print': {
      borderColor: '#DDDDDD'
    }
  },
  shipmentDocumentIcon: {
    marginRight: '15px',
    fontSize: '3rem'
  },
  shipmentDocumentInfoData: {
    display: 'flex',
    flexDirection: 'column',
    fontSize: '1rem'
  },
  shipmentDocumentInfoItemMirror: {
    padding: '15px',
    borderRadius: theme.shape.borderRadius,
    fontSize: '1rem',
    backgroundColor: '#5c6bc0',
    color: theme.palette.text.secondary,
    '& a, & a:hover, & a:active, & a:visited': {
      marginLeft: '15px',
      textDecoration: 'underline',
      color: theme.palette.secondary.main,
      '@media print': {
        textDecoration: 'none',
        color: '#333333'
      }
    },
    '&:first-of-type': {
      padding: '0',
      borderRadius: '50%'
    },
    '&:last-of-type': {
      padding: '0',
      borderRadius: '50%'
    },
    '@media print': {
      color: '#DDDDDD'
    }
  },
  shipmentDocumentIconMirror: {
    color: theme.palette.text.secondary,
    '@media print': {
      display: 'none'
    }
  },
  shipmentDocumentTrackingImportant: {
    marginLeft: '15px',
    color: theme.palette.secondary.main,
    '@media print': {
      color: '#333333'
    }
  },
  alignEnd: {
    alignSelf: 'flex-end'
  }
}));
