import { IconButton } from '@material-ui/core';
import EventNotegIcon from '@material-ui/icons/EventNote';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import PrintIcon from '@material-ui/icons/Print';
import CloseIcon from '@material-ui/icons/Close';
import { useStyle } from './ShipmentDocument.styles';

const ShipmentDocument = ({ shipment, closeDialog = () => {} }) => {
  const styles = useStyle();

  return (
    <div className={styles.shipmentBlock}>
      <div className={styles.shipmentDocument}>
        <div className={styles.shipmentDocumentLeft}>
          <ul className={styles.shipmentDocumentInfo}>
            <li className={styles.shipmentDocumentInfoItem}>
              Information For Your Shipment
            </li>
            <li className={styles.shipmentDocumentInfoItem}>
              <EventNotegIcon className={styles.shipmentDocumentIcon} />
              <div className={styles.shipmentDocumentInfoData}>
                <span>Departure date:</span>
                <span>
                  {new Date(shipment.departureDate).toLocaleString('en-GB')}
                </span>
              </div>
            </li>
            <li className={styles.shipmentDocumentInfoItem}>
              <EventAvailableIcon className={styles.shipmentDocumentIcon} />
              <div className={styles.shipmentDocumentInfoData}>
                <span>Arrival date:</span>
                <span>
                  {new Date(shipment.arrivalDate).toLocaleString('en-GB')}
                </span>
              </div>
            </li>
            <li className={styles.shipmentDocumentInfoItem}>
              <LocalShippingIcon className={styles.shipmentDocumentIcon} />
              <div className={styles.shipmentDocumentInfoData}>
                <span>Origin warehouse:</span>
                <span>{shipment.originWarehouse}</span>
              </div>
            </li>
            <li className={styles.shipmentDocumentInfoItem}>
              <LocalShippingIcon className={styles.shipmentDocumentIcon} />
              <div className={styles.shipmentDocumentInfoData}>
                <span>Destination warehouse:</span>
                <span>{shipment.destinationWarehouse}</span>
              </div>
            </li>
          </ul>
        </div>
        <div className={styles.shipmentDocumentRight}>
          <ul className={styles.shipmentDocumentInfo}>
            <li
              className={`${styles.shipmentDocumentInfoItemMirror} ${styles.alignEnd}`}
            >
              <IconButton onClick={() => closeDialog(dialog => !dialog)}>
                <CloseIcon className={styles.shipmentDocumentIconMirror} />
              </IconButton>
            </li>
            <li className={styles.shipmentDocumentInfoItemMirror}>
              Tracking number:
              <span className={styles.shipmentDocumentTrackingImportant}>
                {shipment.name}
              </span>
            </li>
            <li className={styles.shipmentDocumentInfoItemMirror}>
              Shipment status:
              <span className={styles.shipmentDocumentTrackingImportant}>
                {shipment.status}
              </span>
            </li>
            <li className={styles.shipmentDocumentInfoItemMirror}>
              Foreperson:
              <span className={styles.shipmentDocumentTrackingImportant}>
                {shipment.operatorFirstName} {shipment.operatorLastName},
                <a href={`mailto:${shipment.operatorEmail}`}>
                  {shipment.operatorEmail}
                </a>
              </span>
            </li>
            <li className={styles.shipmentDocumentInfoItemMirror}>
              Lookup date:
              <span className={styles.shipmentDocumentTrackingImportant}>
                {new Date().toLocaleString('en-GB')}
              </span>
            </li>
            <li
              className={`${styles.shipmentDocumentInfoItemMirror} ${styles.alignEnd}`}
            >
              <IconButton onClick={() => window.print()}>
                <PrintIcon className={styles.shipmentDocumentIconMirror} />
              </IconButton>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default ShipmentDocument;
