/* Fill database with values using this SQL dump script */
USE wilson;

INSERT INTO countries (`name`) VALUES
('Canada'),
('United States of America'),
('Mexico'),
('Argentina'),
('France'),
('Germany'),
('Monaco'),
('Morocco'),
('China'),
('Bulgaria'),
('Russia'),
('Australia'),
('Brazil'),
('India'),
('South Africa');

INSERT INTO cities (`name`, `country_id`) VALUES
('Montreal', 1),
('Vancouver', 1),
('Winnipeg', 1),
('New York', 2),
('Los Angeles', 2),
('Denver', 2),
('Mexico City', 3),
('Buenos Aires', 4),
('Paris', 5),
('Bordeaux', 5),
('Berlin', 6),
('Hamburg', 6),
('Monaco', 7),
('Marrakesh', 8),
('Hong Kong', 9),
('Beijing', 9),
('Sofia', 10),
('Moscow', 11),
('Chelyabinsk', 11),
('Vladivostok', 11),
('Melbourne', 12),
('Rio de Janeiro', 13),
('Mumbai', 14),
('Cape Town', 15);

INSERT INTO roles (`name`) VALUES
('operator'),
('customer');

INSERT INTO users (`email`, `password`, `first_name`, `last_name`, `address`, `avatar`, `city_id`, `role_id`) VALUES
/* password: Operator123 */
('operator_demo@mail.com', '$2a$10$GnFlEuS/fD/72lZJ2vlHZeTbJfBlLK86cnmKMmYt.y50VoocNxi/q', 'Operator', 'Demo', '0000 Demo', 'http://localhost:5000/users/avatar/1.jpeg', 1, 1),
/* password: Customer123 */
('customer_demo@mail.com', '$2a$10$Ab30iep0dAeCMCXuT4T1FO710bJNZI0K4Pdq1gkf8lZyKsry.QhOK', 'Customer', 'Demo', '0000 Demo', 'http://localhost:5000/users/avatar/2.jpeg', 2, 2),
/* password: Random123 */
('fmockett0@newsvine.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Flore', 'Mockett', '97881 Springview Alley', 'http://localhost:5000/users/avatar/3.jpeg', 8, 2),
('agarfoot1@cnet.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Adelheid', 'Garfoot', '7965 Elmside Hill', 'http://localhost:5000/users/avatar/4.jpeg', 7, 2),
('hsaurat2@hc360.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Hinze', 'Saurat', '30810 Spaight Park', 'http://localhost:5000/users/avatar/5.jpeg', 7, 2),
('wmalkie3@networksolutions.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Willey', 'Malkie', '343 Dovetail Trail', 'http://localhost:5000/users/avatar/6.jpeg', 4, 2),
('jcrummy4@arstechnica.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Jean', 'Crummy', '27 Lotheville Terrace', 'http://localhost:5000/users/avatar/7.jpeg', 1, 2),
('mbeaument5@examiner.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Markos', 'Beaument', '33 Amoth Lane', 'http://localhost:5000/users/avatar/8.jpeg', 8, 2),
('dcredland6@twitter.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Darcee', 'Credland', '9975 American Ash Junction', 'http://localhost:5000/users/avatar/9.jpeg', 6, 2),
('smccrae7@freewebs.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Sela', 'McCrae', '87 Laurel Park', 'http://localhost:5000/users/avatar/10.jpeg', 2, 2),
('ejarrard8@usatoday.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Eirena', 'Jarrard', '45018 Hollow Ridge Street', 'http://localhost:5000/users/avatar/11.jpeg', 8, 2),
('ghanne9@etsy.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Gayel', 'Hanne', '7056 Canary Circle', 'http://localhost:5000/users/avatar/12.jpeg', 5, 2),
('cbicka@123-reg.co.uk', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Cara', 'Bick', '421 Quincy Street', 'http://localhost:5000/users/avatar/13.jpeg', 4, 2),
('mtorrijosb@wordpress.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Manon', 'Torrijos', '25706 Paget Place', 'http://localhost:5000/users/avatar/14.jpeg', 7, 2),
('edwirec@people.com.cn', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Emmaline', 'Dwire', '7980 Buena Vista Circle', 'http://localhost:5000/users/avatar/15.jpeg', 7, 2),
('lbirkmyrd@yellowpages.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Lilllie', 'Birkmyr', '1 Kingsford Park', 'http://localhost:5000/users/avatar/16.jpeg', 4, 2),
('esherele@mozilla.org', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Essy', 'Sherel', '15383 Harbort Point', 'http://localhost:5000/users/avatar/17.jpeg', 8, 2),
('msylvainef@ezinearticles.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Maye', 'Sylvaine', '40 Brown Parkway', 'http://localhost:5000/users/avatar/18.jpeg', 6, 2),
('vpoultong@mlb.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Vera', 'Poulton', '161 Nova Pass', 'http://localhost:5000/users/avatar/19.jpeg', 5, 2),
('draysh@slate.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Dara', 'Rays', '686 Ryan Way', 'http://localhost:5000/users/avatar/20.jpeg', 3, 2),
('snapoleonei@wikispaces.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Swen', 'Napoleone', '4 Blue Bill Park Lane', 'http://localhost:5000/users/avatar/21.jpeg', 3, 2),
('kmcilmoriej@fda.gov', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Kath', 'McIlmorie', '93 Rockefeller Pass', 'http://localhost:5000/users/avatar/22.jpeg', 9, 2),
('mrymank@constantcontact.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Maybelle', 'Ryman', '7 Heath Park', 'http://localhost:5000/users/avatar/23.jpeg', 4, 2),
('lmccaffreyl@forbes.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Latashia', 'McCaffrey', '76 Jay Junction', 'http://localhost:5000/users/avatar/24.jpeg', 5, 2),
('hformoym@yelp.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Hendrika', 'Formoy', '33 Sachtjen Street', 'http://localhost:5000/users/avatar/25.jpeg', 5, 2),
('cyvensn@etsy.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Collin', 'Yvens', '2711 Comanche Place', 'http://localhost:5000/users/avatar/26.jpeg', 5, 2),
('ehessleo@wikia.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Erinna', 'Hessle', '72725 Monterey Terrace', 'http://localhost:5000/users/avatar/27.jpeg', 2, 2),
('smountlowp@scientificamerican.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Stanley', 'Mountlow', '8 Donald Place', 'http://localhost:5000/users/avatar/28.jpeg', 2, 2),
('jmcguineyq@pagesperso-orange.fr', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Jamal', 'McGuiney', '8 Debra Place', 'http://localhost:5000/users/avatar/29.jpeg', 1, 2),
('kjohannr@indiegogo.com', '$2a$10$OiHrqqe4U.PamGPgsv4dn.PAzJMv0YvkHxWGrjxnlTH6ey21ZKHPe', 'Kienan', 'Johann', '90154 Oneill Center', 'http://localhost:5000/users/avatar/30.jpeg', 1, 2);

INSERT INTO warehouses (`name`, `address`, `latitude`, `longitude`, `city_id`) VALUES
('Bulletproof Logistics', '5055 Rue Courval, Saint-Laurent, QC H4T 1X6', 45.4408044, -73.7152459, 1),
('Nexgen Logistics & Warehouse', '150 Pulaski St, Bayonne, NJ 07002', 40.6262029, -73.9191507, 2),
('Circa Freight Forwarders', 'Arequipa 892 - 401, Lindavista, Gustavo A. Madero', 19.3878628, -99.1068412, 3),
('Crane Worldwide Logistics', 'Estados Unidos 20, C1063 ABO', -34.6254734, -58.4082454, 4),
('Pantos Logistics', 'Le Noyer aux Perdrix, 77170 Servon', 48.7903721, 2.0928228, 5),
('Rieck Fulfillment Solutions', 'Alexander-Meißner-Straße 84-88', 52.4124465, 13.4871841, 6),
('Ferrari Logistiques SAM', 'Gildo Pastor Center, 7 Rue du Gabian', 43.7282249, 7.4114527, 7),
('FedEx Marrakech', '09 angle Bd Abdelkrim Al Khattabi', 31.6546118, -8.0253496, 8),
('Hong Kong Dehe Logistics', '1 Cheong Hang Rd, Hung Hom Bay', 22.3070457, 114.1686672, 9);

INSERT INTO shipment_statuses (`name`) VALUES
('Awaiting'),
('Received'),
('In Transit'),
('Delivered');

INSERT INTO shipments (`name`, `departure_date`, `arrival_date`, `user_id`, `origin_id`, `destination_id`, `shipment_status_id`) VALUES
('6229aa0a-569c-4968-a864-43e95bb2247b', '2021-04-11', '2020-11-19', 8, 3, 9, 1),
('705e96a3-fcd6-4d0b-8fb3-f41e2e08fb3d', '2021-08-18', '2021-05-29', 10, 2, 7, 1),
('e8f8de77-ca9a-4b71-9797-e16d36eb0b46', '2020-12-28', '2020-09-01', 1, 6, 9, 1),
('eb7df969-0863-43b6-b631-5f75bcfccb9c', '2021-08-14', '2021-05-05', 30, 2, 7, 1),
('42fb21d0-75dd-46e5-9e55-606b4137b086', '2021-08-06', '2020-10-15', 8, 7, 8, 1),
('50db4bb5-f5a2-4886-b16b-a3663ddcb5dd', '2021-01-23', '2021-05-29', 27, 3, 5, 1),
('06d3e8ab-b86d-4dbb-80ec-80f7d94f7c41', '2021-01-26', '2021-08-19', 27, 2, 2, 1),
('42658376-a1d0-4f74-846a-105002c4d4c5', '2021-01-22', '2021-04-07', 6, 4, 4, 1),
('b3773306-dc30-4cef-98fc-f3ab2b319ee5', '2021-03-25', '2021-09-01', 24, 1, 4, 1),
('786e10e0-817a-45d8-9e61-98fec9db37ea', '2021-03-24', '2021-02-19', 20, 2, 5, 1),
('936a7e6b-0635-4016-a307-66781db79e80', '2020-10-07', '2021-01-12', 27, 3, 5, 1),
('fc1a68df-6260-40be-8f98-9d40c30691cc', '2020-11-11', '2021-08-07', 5, 3, 2, 1),
('a1dd52bc-2926-4090-8de4-0d24c817b5af', '2020-08-20', '2021-02-07', 26, 2, 5, 1),
('d8e17f93-2ce1-4253-a7cb-d1388e8153c3', '2021-03-20', '2021-09-24', 18, 9, 2, 1),
('9ecf2073-525f-4c8a-8782-61af0e92b929', '2020-11-16', '2020-09-06', 7, 1, 3, 1),
('4adb0e49-6be8-4bc0-aa85-7dbd0c926ae1', '2021-04-09', '2020-10-06', 12, 6, 6, 1),
('89b11c81-4530-4fac-ba8e-1d8a99638385', '2021-07-01', '2021-06-23', 1, 2, 4, 1),
('29d34d98-4934-43e4-bb37-019f19cb8085', '2021-02-25', '2021-05-28', 20, 7, 7, 1),
('5375f3d4-66a5-474d-9485-227f7e22d30f', '2021-03-08', '2021-03-07', 26, 2, 4, 1),
('fa752f81-d0ae-415b-b13b-07d9277fc5cc', '2020-09-23', '2021-06-18', 23, 4, 4, 1),
('64b7ae55-4d63-48aa-8477-9cca4b92c9b3', '2021-08-17', '2021-08-01', 29, 2, 8, 1),
('1e96f647-a249-4b2b-9353-cf839cb832a7', '2020-09-25', '2021-07-26', 5, 5, 7, 1),
('1cf3fb8a-3f68-4fcf-b769-240d5bec0a37', '2020-11-04', '2021-08-28', 28, 8, 1, 1),
('4e99e555-916f-4260-a9ea-9f3277662458', '2020-08-16', '2021-07-27', 19, 2, 4, 1),
('e7015dfa-18d7-498b-b567-196ba3e46240', '2021-01-16', '2021-05-08', 28, 6, 1, 1),
('dabb7c87-2b70-47f5-a056-bdb07e6400f4', '2020-12-18', '2021-08-01', 18, 3, 4, 1),
('fcc4caf3-8370-432a-aa4d-54942886266d', '2020-12-25', '2021-05-09', 24, 7, 2, 1),
('1a691c64-c030-4eba-a703-9fe1d68fce8c', '2021-01-08', '2021-04-26', 23, 1, 9, 1),
('614a54d2-f246-4d36-8da8-9673c0d33c6a', '2021-07-08', '2020-12-10', 28, 5, 6, 1),
('4a404848-ad8f-4ac8-a275-70cbe045b724', '2020-12-23', '2021-06-23', 16, 9, 2, 1);

INSERT INTO delivery_types (`name`) VALUES
('Address'),
('Warehouse');

INSERT INTO parcel_categories (`name`) VALUES
('Automotive'),
('Beauty'),
('Books'),
('Clothing'),
('Computers'),
('Electronics'),
('Games'),
('Garden'),
('Grocery'),
('Health'),
('Home'),
('Industrial'),
('Jewelry'),
('Movies'),
('Music'),
('Outdoors'),
('Shoes'),
('Sports'),
('Tools'),
('Toys');

INSERT INTO parcel_delivery_statuses (`name`) VALUES
('Awaiting'),
('Received');

/* delivery destination: null (users' address) */
INSERT INTO parcels (`name`, `weight`, `user_id`, `shipment_id`, `parcel_category_id`, `parcel_delivery_status_id`) VALUES
('SWAROVSKI Tennis Deluxe Jewelry Collection, Clear Crystals', 0.3, 2, 12, 13, 1),
('SWAROVSKI Iconic Swan Jewelry Collection, Rose Gold Tone Finish, Black Crystals', 0.9, 1, 27, 13, 1),
('SWAROVSKI Attract Soul Jewelry Collection, Pink Crystals, Clear Crystals', 0.6, 24, 21, 13, 1),
('The Godfather Trilogy: Corleone Legacy Edition [Blu-ray]', 0.25, 1, 7, 14, 1),
('Scarface (1983) [Blu-ray]', 0.32, 2, 17, 14, 1),
('Intex Challenger Kayak Inflatable Set with Aluminum Oars', 0.23, 16, 24, 16, 1),
('Suncast 33 Gallon Hideaway Can Resin Outdoor Trash with Lid Use in Backyard, Deck, or Patio, 33-Gallon, Brown', 7.71, 1, 26, 16, 1),
('Catchmaster X-Large Outdoor Disposable Fly Bag Trap - Bulk Pack of 4 Fly Bags', 6.75, 9, 30, 16, 1),
('Coleman Portable Butane Stove with Carrying Case', 5.87, 1, 11, 16, 1),
('Coleman Gas Camping Stove | Triton+ Propane Stove, 2 Burner', 5.16, 4, 2, 16, 1),
('Nike Revolution 5 Wide Running Shoe', 1.29, 19, 11, 17, 1),
('Under Armour Assert 8 Mrble Running Shoe', 0.67, 7, 6, 17, 1),
('Under Armour Mid Keyhole Sports Bra', 1.01, 19, 29, 18, 1),
('Under Armour Graphic Shorts', 0.17, 21, 26, 18, 1),
('REXBETI 217-Piece Tool Kit, General Household Hand Tool Set with Solid Carrying Tool Box, Auto Repair Tool Sets', 5.19, 1, 28, 19, 1),
('DEWALT 20V Max Cordless Drill Combo Kit, 2-Tool (DCK240C2)', 6.84, 12, 13, 19, 1),
('CRAFTSMAN Home Tool Kit / Mechanics Tools Kit, 57-Piece (CMMT99446)
', 3.51, 1, 22, 19, 1),
('LEGO NASA Space Shuttle Discovery 10283 Build and Display Model for Adults, New 2021 (2,354 Pieces)', 2.25, 3, 14, 20, 1),
('LEGO Marvel Infinity Gauntlet 76191 Collectible Building Kit; Thanos Right Hand Gauntlet Model with Infinity Stones (590 Pieces)', 2.36, 2, 3, 20, 1),
('Peppa Pig Lights & Sounds Family Home Feature Playset', 3.48, 5, 20, 20, 1);

/* delivery destination: destination_id */
INSERT INTO parcels (`name`, `weight`, `user_id`, `destination_id`, `shipment_id`, `parcel_category_id`, `parcel_delivery_status_id`) VALUES
('L’Oreal HIP eyeshadow', 0.76, 29, 8, 25, 2, 1),
('Sephora Triple Action Mascara', 0.83, 2, 7, 29, 2, 1),
('Bose SoundLink Micro: Small Portable Bluetooth Speaker', 1.71, 3, 4, 11, 6, 1),
('Xbox Series X', 3.45, 1, 1, 26, 7, 1),
('Wireless Pro Controller', 1.95, 27, 2, 4, 7, 1),
('Himalayan Glow 1002 Crystal, Salt Lamp', 3.23, 1, 6, 8, 11, 1),
('Minecraft: Java Edition for PC/Mac', 0.17, 12, 2, 8, 8, 1),
('SugarBearHair Vitamins', 1.18, 1, 8, 29, 10, 1),
('Perfect Keto Test Strips', 1.81, 2, 5, 22, 10, 1),
('SWAROVSKI Sunshine Crystal Jewelry Collection', 0.93, 22, 3, 19, 13, 1);
