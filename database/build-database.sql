CREATE DATABASE IF NOT EXISTS wilson;

USE wilson;

CREATE TABLE countries (
  country_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(255) UNIQUE NOT NULL,
  PRIMARY KEY (country_id)
);

CREATE TABLE cities (
  city_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(255) UNIQUE NOT NULL,
  country_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (city_id),
  FOREIGN KEY (country_id) REFERENCES countries(country_id)
);

CREATE TABLE roles (
  role_id TINYINT(1) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(255) UNIQUE NOT NULL,
  PRIMARY KEY (role_id)
);

CREATE TABLE users (
  `user_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  email VARCHAR(255) UNIQUE NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  first_name  NVARCHAR(15),
  last_name  NVARCHAR(15),
  `address` NVARCHAR(255),
  avatar NVARCHAR(255),
  city_id INT UNSIGNED NOT NULL,
  role_id TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`),
  FOREIGN KEY (city_id) REFERENCES cities(city_id),
  FOREIGN KEY (role_id) REFERENCES roles(role_id)
);

CREATE TABLE warehouses (
  warehouse_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(255) UNIQUE NOT NULL,
  `address` NVARCHAR(255) UNIQUE NOT NULL,
  latitude DOUBLE PRECISION UNIQUE NOT NULL,
  longitude DOUBLE PRECISION UNIQUE NOT NULL,
  is_deleted TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  city_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (warehouse_id),
  FOREIGN KEY (city_id) REFERENCES cities(city_id)
);

CREATE TABLE shipment_statuses (
  shipment_status_id TINYINT(1) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(255) UNIQUE NOT NULL,
  PRIMARY KEY (shipment_status_id)
);

CREATE TABLE shipments (
  shipment_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(255) UNIQUE NOT NULL,
  departure_date DATETIME NOT NULL,
  arrival_date DATETIME NOT NULL,
  is_deleted TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` INT UNSIGNED NOT NULL,
  origin_id INT UNSIGNED NOT NULL,
  destination_id INT UNSIGNED NOT NULL,
  shipment_status_id TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY (shipment_id),
  FOREIGN KEY (`user_id`) REFERENCES users(`user_id`),
  FOREIGN KEY (origin_id) REFERENCES warehouses(warehouse_id),
  FOREIGN KEY (destination_id) REFERENCES warehouses(warehouse_id),
  FOREIGN KEY (shipment_status_id) REFERENCES shipment_statuses(shipment_status_id)
);

CREATE TABLE delivery_types (
  delivery_type_id TINYINT(1) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(255) UNIQUE NOT NULL,
  PRIMARY KEY (delivery_type_id)
);

CREATE TABLE parcel_categories (
  parcel_category_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(255) UNIQUE NOT NULL,
  is_deleted TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (parcel_category_id)
);

CREATE TABLE parcel_delivery_statuses (
  parcel_delivery_status_id TINYINT(1) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(255) UNIQUE NOT NULL,
  PRIMARY KEY (parcel_delivery_status_id)
);

CREATE TABLE parcels (
  parcel_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(255) NOT NULL,
  `weight` FLOAT UNSIGNED NOT NULL,
  user_notified TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  is_deleted TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` INT UNSIGNED NOT NULL,
  destination_id INT UNSIGNED DEFAULT NULL,
  shipment_id BIGINT UNSIGNED DEFAULT NULL,
  delivery_type_id TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  parcel_category_id INT UNSIGNED NOT NULL,
  parcel_delivery_status_id TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (parcel_id),
  FOREIGN KEY (`user_id`) REFERENCES users(`user_id`),
  FOREIGN KEY (destination_id) REFERENCES warehouses(warehouse_id),
  FOREIGN KEY (shipment_id) REFERENCES shipments(shipment_id),
  FOREIGN KEY (delivery_type_id) REFERENCES delivery_types(delivery_type_id),
  FOREIGN KEY (parcel_category_id) REFERENCES parcel_categories(parcel_category_id),
  FOREIGN KEY (parcel_delivery_status_id) REFERENCES parcel_delivery_statuses(parcel_delivery_status_id)
);

CREATE TABLE tokens (
  token_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `token` TEXT(7000) NOT NULL,
  PRIMARY KEY (token_id)
);
