import express from 'express';
import idsChecker from './../middlewares/ids-checker.js';
import warehousesService from './../services/warehouses-service.js';
import warehousesData from './../data/warehouses-data.js';
import errors from './../common/errors.js';
import bodyValidator from './../middlewares/body-validator.js';
import warehouseSchema from './../validators/warehouse.schema.js';

export const warehousesRouter = express.Router();

warehousesRouter.param('id', idsChecker);

warehousesRouter.get('/', async (_, res) => {
  const result = await warehousesService.getWarehouses(warehousesData);

  if (result === errors.NOT_FOUND) {
    return res.status(404).json({ msg: 'No warehouses were found.' });
  }

  return res.status(200).json({
    data: result
  });
});

warehousesRouter.get('/:id', async (req, res) => {
  const { id } = req.params;
  const result = await warehousesService.getWarehouseById(warehousesData)(
    parseInt(id)
  );

  if (result === errors.NOT_FOUND) {
    return res
      .status(404)
      .json({ msg: `There is no record of warehouse with id: ${id}.` });
  }

  return res.status(200).json({
    data: result
  });
});

warehousesRouter.put(
  '/:id',
  bodyValidator(warehouseSchema),
  async (req, res) => {
    const { id } = req.params;
    const updateData = Object.values(req.body);
    const result = await warehousesService.updateWarehouse(warehousesData)(
      parseInt(id),
      updateData
    );

    if (result === errors.NOT_FOUND) {
      return res
        .status(404)
        .json({ msg: `There is no record of warehouse with id: ${id}.` });
    }

    if (result === errors.SERVER_PROBLEM) {
      return res
        .status(500)
        .json({ msg: `Server failed to update warehouse with id: ${id}.` });
    }

    return res.status(200).json({
      data: result
    });
  }
);

warehousesRouter.post('/', bodyValidator(warehouseSchema), async (req, res) => {
  const addData = req.body;
  const result = await warehousesService.addWarehouse(warehousesData)(addData);

  if (result === errors.DUPLICATE_RECORD) {
    return res.status(409).json({
      msg: `There is already a record of warehouse with this name or address.`
    });
  }

  if (result === errors.SERVER_PROBLEM) {
    return res.status(500).json({ msg: `Server failed to add warehouse.` });
  }

  return res.status(200).json({
    data: result
  });
});

warehousesRouter.delete('/:id', async (req, res) => {
  const { id } = req.params;
  const result = await warehousesService.deleteWarehouse(warehousesData)(
    parseInt(id)
  );

  if (result === errors.NOT_FOUND) {
    return res
      .status(404)
      .json({ msg: `There is no record of warehouse with id: ${id}.` });
  }

  if (result === errors.SERVER_PROBLEM) {
    return res
      .status(500)
      .json({ msg: `Server failed to delete warehouse with id: ${id}.` });
  }

  return res.status(200).json({
    data: result
  });
});
