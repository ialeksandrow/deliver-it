import express from 'express';
import idsChecker from './../middlewares/ids-checker.js';
import globalService from './../services/global-service.js';
import globalData from './../data/global-data.js';
import errors from './../common/errors.js';

export const globalRouter = express.Router();

globalRouter.param('id', idsChecker);

globalRouter.get('/countries', async (_, res) => {
  const result = await globalService.getCountries(globalData);

  if (result === errors.NOT_FOUND) {
    return res.status(404).json({ msg: 'No countries were found.' });
  }

  return res.status(200).json({
    data: result
  });
});

globalRouter.get('/countries/:id', async (req, res) => {
  const { id } = req.params;
  const result = await globalService.getCountryById(globalData)(parseInt(id));

  if (result === errors.NOT_FOUND) {
    return res
      .status(404)
      .json({ msg: `There is no record of country with id: ${id}.` });
  }

  return res.status(200).json({
    data: result
  });
});

globalRouter.get('/cities', async (req, res) => {
  const { name } = req.query;

  if (name) {
    const result = await globalService.getCityByName(globalData)(name);

    if (result === errors.NOT_FOUND) {
      return res
        .status(404)
        .json({ msg: `There is no record of city with ${name} as name.` });
    }

    return res.status(200).json({
      data: result
    });
  }

  const result = await globalService.getCities(globalData);

  if (result === errors.NOT_FOUND) {
    return res.status(404).json({ msg: 'No cities were found.' });
  }

  return res.status(200).json({
    data: result
  });
});

globalRouter.get('/cities/:id', async (req, res) => {
  const { id } = req.params;
  const result = await globalService.getCityById(globalData)(parseInt(id));

  if (result === errors.NOT_FOUND) {
    return res
      .status(404)
      .json({ msg: `There is no record of city with id: ${id}.` });
  }

  return res.status(200).json({
    data: result
  });
});
