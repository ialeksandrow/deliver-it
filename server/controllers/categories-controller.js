import express from 'express';
import authMiddleware from '../middlewares/authenticate-user.js';
import tokenValidator from '../middlewares/token-validator.js';
import employeeOnly from '../middlewares/employee-only.js';
import categoryData from '../data/categories-data.js'
import paramsValidator from '../middlewares/params-validator.js';
import bodyValidator from '../middlewares/body-validator.js';
import categoryValidator from '../validators/category-validator.js';
import errors from '../common/errors.js';
import { getAllCategories, getCategory, createCategory, updateCategory, deleteCategory } from '../services/categories-services.js';

const categoryRoute = express.Router();
categoryRoute.use(authMiddleware, tokenValidator);

/**
 * Get all categories
 */
categoryRoute.get('/', async (_, res) => {
  const {error, data} = await getAllCategories(categoryData);

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(data);
});

categoryRoute.use(employeeOnly);
/**
 * Get single category by id
 */
categoryRoute.get('/:id', paramsValidator, async (req, res) => {
  const {error, data} = await getCategory(categoryData)(req.params.id);

  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `category with id '${req.params.id}' not found` });
  }

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});


/**
 * Create new category
 */
categoryRoute.post('/', bodyValidator(categoryValidator), async (req, res) => {
  const {error, data} = await createCategory(categoryData)(req.body.category);

  if (error === errors.DUPLICATE_RECORD) {
    return res.status(404).send({ message: `category with name '${req.body.category}' already exists` });
  }

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});


/**
 * Update category name
 */
categoryRoute.put('/:id', paramsValidator, bodyValidator(categoryValidator), async (req, res) => {
  const {error, data} = await updateCategory(categoryData)(req.params.id, req.body.category);
  
  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `category with id '${req.params.id}' not found` });
  }

  if (error === errors.DUPLICATE_RECORD) {
    return res.status(404).send({ message: `category with name '${req.body.category}' already exists` });
  }

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});


/**
 * Delete category
 */
categoryRoute.delete('/:id', paramsValidator, async (req, res) => {
  const {error, data} = await deleteCategory(categoryData)(req.params.id);
  
  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `category with id '${req.params.id}' not found` });
  }

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});


export default categoryRoute;