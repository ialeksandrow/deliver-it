import express from 'express';
import bodyValidator from '../middlewares/body-validator.js';
import paramsValidator from '../middlewares/params-validator.js';
import ownerOrEmployee from '../middlewares/owner-employee.js';
import employeeOnly from '../middlewares/employee-only.js';
import fileValidator from '../middlewares/file-validator.js';
import queryValidator from '../middlewares/query-validator.js';
import searchUserValidator from '../validators/search-user-validator.js';
import loginValidator from '../validators/login-validator.js';
import searchKeyWordValidator from '../validators/search-keyword-validator.js';
import registerValidator from '../validators/register-validator.js';
import newPasswordValidator from '../validators/new-password-validator.js';
import updateUserInfoValidator from '../validators/update-user-validator.js';
import authMiddleware from '../middlewares/authenticate-user.js';
import tokenValidator from '../middlewares/token-validator.js';
import fileUpload from 'express-fileupload';
import errors from '../common/errors.js';
import path from 'path'
import * as userData from '../data/user-data.js';
import {
  userLogin,
  userRegister,
  customersCount,
  changeUserPassword,
  updateUserInfo,
  deleteUser,
  searchByCriteria,
  searchByKeyWord,
  getUser,
  logOutUser
} from '../services/user-services.js';

const userRoute = express.Router();

/**
 * User login
 */
userRoute.post('/login', bodyValidator(loginValidator), async (req, res) => {
  const {error, data} = await userLogin(userData)(req.body.email, req.body.password);

  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `this user don't exists` });
  }

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  if (error === errors.WRONG_CREDENTIALS) {
    return res.status(401).send({ message: `wrong credentials` });
  }

  res.status(200).send(data)
});


/**
 * User register
 */
userRoute.post('/register', fileUpload({ createParentPath: true }), fileValidator, bodyValidator(registerValidator), async (req, res) => {
  const {error, data} = await userRegister(userData)(req.body, req.files);

  if (error === errors.DUPLICATE_RECORD) {
    return res.status(409).send({ message: `this e-mail already exists` });
  }

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `this city don't exists` });
  }

  res.status(201).send(...data);
});


/**
 * Get count of all customers
 */
userRoute.get('/customersCount', async (req, res) => {
  const {error, data} = await customersCount(userData)();

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(data);
});


/**
 * Get avatars
 */
userRoute.get('/avatar/:filename', authMiddleware, tokenValidator, async (req, res) => {
  res.sendFile(`${req.params.filename}`, { root: path.join('avatars') });
});


/**
 * Update user password
 */
userRoute.put('/:id/password', authMiddleware, tokenValidator, paramsValidator, bodyValidator(newPasswordValidator), async (req, res) => {
  if (+req.params.id !== +req.user.id) {
    return res.status(401).send({ message: `access denied` });
  }

  const {error, data} = await changeUserPassword(userData)(req.user.id, req.body);

  if (error === errors.WRONG_CREDENTIALS) {
    return res.status(409).send({ message: `wrong credentials` });
  }

  if (error === errors.NOT_MATCH) {
    return res.status(404).send({ message: `new password is not confirmed` });
  }

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});


/**
 * Update user personal info
 */
userRoute.put('/:id/update', authMiddleware, tokenValidator, fileUpload({ createParentPath: true }), fileValidator,
  ownerOrEmployee(false), paramsValidator, bodyValidator(updateUserInfoValidator), async (req, res) => {

  const {error, data} = await updateUserInfo(userData)(req.params.id, req.body, req.files);
  
  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});


/**
 * Logout user
 */
userRoute.delete('/logout', authMiddleware, tokenValidator, async (req, res) => {
  const token = req.headers.authorization.replace('Bearer ', '');
  const {error} = await logOutUser(userData)(token);

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send({ message: `successful logout` });
});


/**
 * Delete user
 */
userRoute.delete('/:id', authMiddleware, tokenValidator, employeeOnly, paramsValidator, async (req, res) => {
  const {error, data} = await deleteUser(userData)(req.params.id);

  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `user with id ${req.params.id} don't exists` });
  }

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});


/**
 * Search in users by multiple criteria
 */
userRoute.get('/search', authMiddleware, tokenValidator, employeeOnly, queryValidator(searchUserValidator), async (req, res) => {
  const {error, data} = await searchByCriteria(userData)(req.query);
  
  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(data);
});


/**
 * Search in users by keyWord
 */
userRoute.get('/', authMiddleware, tokenValidator, employeeOnly, queryValidator(searchKeyWordValidator), async (req, res) => {
  const {error, data} = await searchByKeyWord(userData)(req.query);
  
  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(data);
});


/**
 * Get user info by ID
 */
userRoute.get('/:id', authMiddleware, tokenValidator, tokenValidator, paramsValidator, ownerOrEmployee(true), async (req, res) => {
  const {error, data} = await getUser(userData)(req.params.id);
  
  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});

export default userRoute;
