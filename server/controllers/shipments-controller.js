import express from 'express';
import idsChecker from './../middlewares/ids-checker.js';
import shipmentsData from './../data/shipments-data.js';
import warehousesData from './../data/warehouses-data.js';
import shipmentsService from './../services/shipments-service.js';
import errors from './../common/errors.js';
import bodyValidator from './../middlewares/body-validator.js';
import shipmentSchema from './../validators/shipment.schema.js';

export const shipmentsRouter = express.Router();

shipmentsRouter.param('id', idsChecker);

shipmentsRouter.get('/', async (req, res) => {
  const { name, warehouseId, userEmail } = req.query;

  if (warehouseId || userEmail) {
    const result = await shipmentsService.filterShipment(
      shipmentsData,
      warehousesData
    )(warehouseId, userEmail);

    if (result === errors.NOT_FOUND) {
      return res.status(404).json({
        msg: `There is no record of warehouse with id: ${warehouseId}.`
      });
    }

    if (result === errors.NOT_MATCH) {
      return res.status(404).json({
        msg: `There is no record that matches the given query string.`
      });
    }

    if (result === errors.SERVER_PROBLEM) {
      return res.status(500).json({
        msg: 'Server failed to find shipment with the given query string.'
      });
    }

    return res.status(200).json({
      data: result
    });
  }

  if (name) {
    const result = await shipmentsService.getShipmentByName(shipmentsData)(
      name
    );

    if (result === errors.NOT_FOUND) {
      return res
        .status(404)
        .json({ msg: `There is no record of shipment with ${name} as name.` });
    }

    return res.status(200).json({
      data: result
    });
  }

  const result = await shipmentsService.getShipments(shipmentsData);

  if (result === errors.NOT_FOUND) {
    return res.status(404).json({ msg: 'No shipments were found.' });
  }

  return res.status(200).json({
    data: result
  });
});

shipmentsRouter.get('/:id', async (req, res) => {
  const { id } = req.params;
  const result = await shipmentsService.getShipmentById(shipmentsData)(
    parseInt(id)
  );

  if (result === errors.NOT_FOUND) {
    return res
      .status(404)
      .json({ msg: `There is no record of shipment with id: ${id}.` });
  }

  return res.status(200).json({
    data: result
  });
});

shipmentsRouter.put('/:id', bodyValidator(shipmentSchema), async (req, res) => {
  const { id } = req.params;
  const updateData = Object.values(req.body);
  const result = await shipmentsService.updateShipment(shipmentsData)(
    parseInt(id),
    updateData
  );

  if (result === errors.NOT_FOUND) {
    return res
      .status(404)
      .json({ msg: `There is no record of shipment with id: ${id}.` });
  }

  if (result === errors.SERVER_PROBLEM) {
    return res
      .status(500)
      .json({ msg: `Server failed to update shipment with id: ${id}.` });
  }

  return res.status(200).json({
    data: result
  });
});

shipmentsRouter.post('/', bodyValidator(shipmentSchema), async (req, res) => {
  const addData = req.body;
  const result = await shipmentsService.addShipment(shipmentsData)(addData);

  if (result === errors.SERVER_PROBLEM) {
    return res.status(500).json({ msg: `Server failed to add shipment.` });
  }

  return res.status(200).json({
    data: result
  });
});

shipmentsRouter.delete('/:id', async (req, res) => {
  const { id } = req.params;
  const result = await shipmentsService.deleteShipment(shipmentsData)(
    parseInt(id)
  );

  if (result === errors.NOT_FOUND) {
    return res
      .status(404)
      .json({ msg: `There is no record of shipment with id: ${id}.` });
  }

  if (result === errors.SERVER_PROBLEM) {
    return res
      .status(500)
      .json({ msg: `Server failed to delete shipment with id: ${id}.` });
  }

  return res.status(200).json({
    data: result
  });
});
