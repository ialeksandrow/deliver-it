import express from 'express';
import authMiddleware from '../middlewares/authenticate-user.js';
import tokenValidator from '../middlewares/token-validator.js';
import bodyValidator from '../middlewares/body-validator.js';
import newParcelValidator from '../validators/new-parcel-validator.js'
import employeeOnly from '../middlewares/employee-only.js';
import queryValidator from '../middlewares/query-validator.js';
import updateStatusValidator from '../validators/update-status-validator.js';
import searchParcelValidator from '../validators/parcels-search-validator.js';
import addParcelToShipmentValidator from '../validators/parcel-to-shipment-validator.js';
import checkShipmentExists from '../middlewares/check-shipment-exists.js';
import * as parcelData from '../data/parcels-data.js';
import deliveryTypes from '../common/delivery-types.js';
import parcelPermission from '../middlewares/parcel-permission.js';
import errors from '../common/errors.js';
import middlewareConstants from '../common/middleware-constants.js';
import userRoles from '../common/user-roles.js';
import parcelStatuses from '../common/parcel-statuses.js';
import paramsValidator from '../middlewares/params-validator.js';
import {
  getParcel,
  createParcel,
  getUserParcels,
  getUnseenParcels,
  searchParcels,
  setNotified,
  updateParcelStatus,
  setShipmentToParcel,
  deleteParcel,
  updateParcel,
  removeFromShipment
} from '../services/parcels-services.js';

const parcelRoute = express.Router();

parcelRoute.use(authMiddleware, tokenValidator);


/**
 * Get parcel by ID
 */
parcelRoute.get('/:id', paramsValidator, parcelPermission, async (req, res) => {
  const {error, data} = await getParcel(parcelData)(req.params.id);

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  const { userId, shipment, ownerIsNotified } = data[0];

  if (+userId === +req.user.id && shipment !== null && ownerIsNotified === 0) {
    if (!(await setNotified(parcelData)(data[0].parcelId))) {
      return res.status(503).send({ message: `try again later` });
    }

    data[0].ownerIsNotified = 1;
  }

  res.status(200).send(...data);
});


/**
 * Get count of parcels added to a shipment and not yet seen by the owner 
 */
parcelRoute.get('/user/unseen', async (req, res) => {
  const { id: userId } = req.user;
  const {error, data} = await getUnseenParcels(parcelData)(userId);

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});


/**
 * Get parcels by user
 */
parcelRoute.get('/user/:id', paramsValidator, async (req, res) => {
  if (+req.user.id !== +req.params.id && req.user.role !== userRoles.OPERATOR) {
    return res.status(403).send(`You don't have permission for this operation!`);
  }

  const {error, data} = await getUserParcels(parcelData)(req.params.id);

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(data);
});


/**
 * Get parcels
 */
parcelRoute.get('/', queryValidator(searchParcelValidator), async (req, res) => {
  const {error, data} = await searchParcels (parcelData)(req.query, req.user);
  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(data);
});


/**
 * Create new parcel
 */
parcelRoute.post('/', bodyValidator(newParcelValidator), async (req, res) => {
  if (req.body.deliveryTypeId > Math.max(...Object.values(deliveryTypes))) {
    return res.status(400).send({ message: `Invalid delivery type` });
  }

  if (req.body.deliveryTypeId === deliveryTypes.WAREHOUSE && !req.body.warehouseId) {
    return res.status(400).send({ message: `Invalid warehouseId` });
  }

  const {error, data} = await createParcel(parcelData)(req.body, req.user.id);

  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `warehouse with id '${req.body.warehouseId}' don't exists` });
  }
  
  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});


/**
 * Change parcel status
 */
parcelRoute.put('/status',bodyValidator(updateStatusValidator), employeeOnly, async (req, res) => {
  const { statusId, parcelId } = req.body;

  if (!Object.values(parcelStatuses).includes(parseInt(statusId))) {
    return res.status(400).send({ message: `Invalid status` });
  }

  const {error, data} = await updateParcelStatus(parcelData)(statusId, parcelId);

  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `parcel with id ${parcelId} don't exists` });
  }

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});


/**
 * Add parcel to shipment
 */
parcelRoute.put('/shipment', bodyValidator(addParcelToShipmentValidator),
  checkShipmentExists(middlewareConstants.body, middlewareConstants.shipmentId), employeeOnly, async (req, res) => {
  
    const {error, data} = await setShipmentToParcel(parcelData)(req.body.parcelId, req.body.shipmentId);

    if (error === errors.NOT_FOUND) {
      return res.status(404).send({ message: `parcel with id ${req.body.parcelId} don't exists` });
    }
  
    if (error === errors.SERVER_PROBLEM) {
      return res.status(503).send({ message: `try again later` });
    }
  
    res.status(200).send(...data);
});


/**
 * Remove parcel from shipment
 */
parcelRoute.delete('/shipment/:id', paramsValidator, employeeOnly, async (req, res) => {
  const {error, data} = await removeFromShipment(parcelData)(req.params.id);
  
  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `parcel with id ${req.body.parcelId} don't exists` });
  }

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});


/**
 * Delete parcel
 */
parcelRoute.delete('/:id', paramsValidator, parcelPermission, async (req, res) => {
  const {error, data} = await deleteParcel(parcelData)(req.params.id, req.user.role === userRoles.OPERATOR);
  
  if (error === errors.OPERATION_NOT_ALLOWED) {
    return res.status(404).send({ message: `parcel with id '${req.params.id}' already arrived, can't be deleted` });
  }

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});


/**
 * Update parcel
 */
parcelRoute.put('/:id', paramsValidator, bodyValidator(newParcelValidator), parcelPermission, async (req, res) => {
  if (req.body.deliveryTypeId > Math.max(...Object.values(deliveryTypes))) {
    return res.status(400).send({ message: `Invalid delivery type` });
  }

  if (req.body.deliveryTypeId === deliveryTypes.WAREHOUSE && !req.body.warehouseId) {
    return res.status(400).send({ message: `Invalid warehouseId` });
  }

  const {error, data} = await updateParcel(parcelData)(req.params.id, req.body);

  if (error === errors.OPERATION_NOT_ALLOWED) {
    return res.status(404).send({ message: `parcel with id '${req.params.id}' already is on completed shipment, can't change delivery type` });
  }

  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `warehouse id '${req.body.warehouseId}' don't exists` });
  }

  if (error === errors.SERVER_PROBLEM) {
    return res.status(503).send({ message: `try again later` });
  }

  res.status(200).send(...data);
});

export default parcelRoute;