import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

const SECRET_KEY = dotenv.config().parsed.SECRET_KEY;
const TOKEN_LIFE = dotenv.config().parsed.TOKEN_LIFE;

/**
 * Create JWT token
 * @param { object } payLoad data to be included in token
 * @returns { string } jwt token
 */
const createToken = (payLoad) => jwt.sign(payLoad, SECRET_KEY, { expiresIn: TOKEN_LIFE });

export default createToken;