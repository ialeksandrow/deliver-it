import passportJwt from 'passport-jwt';
import dotenv from 'dotenv';

const SECRET_KEY = dotenv.config().parsed.SECRET_KEY;

const options = {
    secretOrKey: SECRET_KEY,
    jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

/**
 * Settings for the passport strategy
 */
const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
    const user = {
        id: payload.id,
        firstName: payload.firstName,
        lastName: payload.lastName,
        avatar: payload.avatar,
        role: payload.role,
        eMail: payload.email,
        city: payload.cityName,
        country: payload.countryName,
        associatedParcels: payload.associatedParcels,
        associatedShipments: payload.associatedShipments,
    };

    done(null, user);
});

export default jwtStrategy;
