import { validateNumber } from './validate-functions.js';

const addParcelToShipmentValidator = {
  parcelId: validateNumber,
  shipmentId: validateNumber,
}

export default addParcelToShipmentValidator;