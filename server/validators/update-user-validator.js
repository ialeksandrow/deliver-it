import { validateFirstName, validateLastName, validateNumber, validateAddress } from './validate-functions.js';

const updateUserInfoValidator = {
  firstName: validateFirstName,
  lastName: validateLastName,
  city: validateNumber,
  address: validateAddress,
}

export default updateUserInfoValidator;