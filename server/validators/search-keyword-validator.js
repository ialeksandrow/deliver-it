import { validateSimpleString } from './validate-functions.js';

const searchKeyWordValidator = {
  keyWord: validateSimpleString,
}

export default searchKeyWordValidator;
