import { validateCategory } from './validate-functions.js';

const categoryValidator = { category: validateCategory }

export default categoryValidator;