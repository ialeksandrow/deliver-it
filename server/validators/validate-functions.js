import validateMessages from './validate-messages.js';

const regularExpressions = {
  validEmail: /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/,
  onlyLatinAndCyrillic : /^[A-Za-z\u0400-\u04FF]*$/,
  address : /^[A-Za-z0-9\u0400-\u04FF. ]*$/,
  onlyLatin: /^[a-zA-Z]*$/,
  password: /^[a-z0-9_]+$/i,
  simpleString: /^[a-z0-9_@.-]+$/i,
}


/**
 * Check value for truthy 
 * @param { any } value 
 * @returns { boolean }
 */
const checkForFalsy = (value) => {
  if (typeof value === 'string') {
    value = value.trim();
  }

  if (!value) {
    return false;
  }

  return true;
}


/**
 * Check all criteria of value
 * @param { string } value 
 * @param { RegExp } regular 
 * @param { number } minLength 
 * @param { number } maxLength 
 * @param { string } type
 * @returns { boolean }
 */
const checkValidation = (value, regular, minLength, maxLength, type) => {
  if (typeof value === 'string') {
    value = value.trim();
  }
  
  return (checkForFalsy(value) && RegExp(regular).test(value) && value.length >= minLength && value.length <= maxLength  && typeof value === type)
    ? true
    : false
}


/**
 * Check e-mail validation
 * @param { string } value 
 * @returns { boolean | string }
 */
export const validateEmail = (value) => checkValidation (value, regularExpressions.validEmail, 8, 200, 'string') ? true : validateMessages.invalidEmail;


/**
 * Check first name validation
 * @param { string } value 
 * @returns { boolean | string }
 */
export const validateFirstName = (value) => checkValidation (value, regularExpressions.onlyLatinAndCyrillic, 1, 200, 'string') ? true : validateMessages.invalidFirstName;


/**
 * Check last name validation
 * @param { string } value 
 * @returns { boolean | string }
 */
export const validateLastName = (value) => checkValidation (value, regularExpressions.onlyLatinAndCyrillic, 1, 200, 'string') ? true : validateMessages.invalidLastName;


/**
 * Check address validation
 * @param { string } value 
 * @returns { boolean | string }
 */
export const validateAddress = (value) => checkValidation (value, regularExpressions.address, 1, 400, 'string') ? true : validateMessages.invalidAddress;


/**
 * Check country name validation
 * @param { string } value 
 * @returns { boolean | string }
 */
export const validateCountry = (value) => checkValidation (value, regularExpressions.onlyLatin, 1, 200, 'string') ? true : validateMessages.invalidCountry;


/**
 * Check city name validation
 * @param { string } value 
 * @returns { boolean | string }
 */
export const validateCity = (value) => checkValidation (value, regularExpressions.onlyLatin, 1, 200, 'string') ? true : validateMessages.invalidCity;


/**
 * Check password validation
 * @param { string } value 
 * @returns { boolean | string }
 */
export const validatePassword = (value) => checkValidation (value, regularExpressions.password, 6, 200, 'string') ? true : validateMessages.invalidPassword;


/**
 * check weight validation
 * @param { string } value 
 * @returns { boolean | string }
 */
export const validateWeight = (value) => checkForFalsy(value) && !isNaN(value) && +value > 0 ? true : validateMessages.invalidWeight;


/**
 * check latitude validation
 * @param { string } value 
 * @returns { boolean | string }
 */
export const validateLatitude = (value) => checkForFalsy(value) && !isNaN(value) && +value <= 90 && +value >= -90 ? true : validateMessages.invalidLatitude;


/**
 * check longitude validation
 * @param { string } value 
 * @returns { boolean | string }
 */
export const validateLongitude = (value) => checkForFalsy(value) && !isNaN(value) && +value <= 180 && +value >= -180 ? true : validateMessages.invalidLongitude;


/**
 * check category validation
 * @param { string } value 
 * @returns 
 */
export const validateCategory = (value) => checkValidation (value, regularExpressions.onlyLatin, 2, 50, 'string') ? true : validateMessages.invalidCategory;


/**
 * check date format validation
 * @param { string } value 
 * @returns { boolean | string }
 */
export const validateDate = (value) => checkForFalsy(value) && (new Date(value).toString() !== 'Invalid Date') ? true : validateMessages.invalidDate;


/**
 * Check for ID number is valid
 * @param { string } value 
 * @returns { boolean | string }
 */
export const validateNumber = (value) => checkForFalsy(value) && !isNaN(value) && +value > 0 && Number.isInteger(+value)  ? true : validateMessages.invalidNumber;


/**
 * Check for email not strong match
 * @param { string } value 
 * @returns { boolean | string }
 */
export const validateSimpleString = (value) => checkValidation (value, regularExpressions.simpleString, 1, 200, 'string') ? true : validateMessages.invalidString;
