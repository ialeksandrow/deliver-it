import { validateSimpleString, validateNumber, validateWeight } from './validate-functions.js';

const checkSortBy = (value) => {
  const wordsArray = value.split(',').map(x => x.trim());

  if (wordsArray.every(x => validateSimpleString(x) === true)) {
    return true;
  }
  return false;
}

const searchParcelValidator = {
  weight: validateWeight,
  parcelId: validateNumber,
  parcelName: validateSimpleString,
  deliveryType: validateNumber,
  customerId: validateNumber,
  customerFirstName: validateSimpleString,
  customerLastName: validateSimpleString,
  customerEmail: validateSimpleString,
  warehouseName: validateSimpleString,
  warehouseId: validateNumber,
  categoryId: validateNumber,
  status: validateNumber,
  sortBy: checkSortBy,
  desc: x => x === 'true' || x === 'false' ? true : false,
}

export default searchParcelValidator;
