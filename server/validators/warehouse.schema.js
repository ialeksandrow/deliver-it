import {
  validateAddress,
  validateLatitude,
  validateLongitude
} from './validate-functions.js';

export default {
  name: value =>
    value &&
    typeof value === 'string' &&
    value.length >= 15 &&
    value.length <= 60,
  address: validateAddress,
  latitude: validateLatitude,
  longitude: validateLongitude,
  city: value => value && typeof value === 'string' && parseInt(value) >= 1
};
