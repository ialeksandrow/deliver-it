import { validateNumber } from './validate-functions.js';

const updateStatusValidator = {
  parcelId: validateNumber,
  statusId: validateNumber,
}

export default updateStatusValidator;