import { validateEmail, validatePassword } from './validate-functions.js';

const loginValidator = {
  email: validateEmail,
  password: validatePassword,
}

export default loginValidator;