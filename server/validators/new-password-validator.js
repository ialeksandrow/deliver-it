import { validatePassword } from './validate-functions.js';

const newPasswordValidator = {
  oldPassword: validatePassword,
  newPassword: validatePassword,
  newPasswordConfirm: validatePassword,
}

export default newPasswordValidator;