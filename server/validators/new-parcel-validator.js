import { validateAddress, validateWeight, validateNumber } from './validate-functions.js';

const newParcelValidator = {
  parcelName: validateAddress,
  parcelWeight: validateWeight,
  parcelCategory: validateNumber,
  deliveryTypeId: validateNumber,
  warehouseId: x => x ? validateNumber(x) : true,
}

export default newParcelValidator;