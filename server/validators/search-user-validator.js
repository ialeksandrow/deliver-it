import { validateSimpleString, validateNumber } from './validate-functions.js';

const searchUserValidator = {
  email: validateSimpleString,
  firstName: validateSimpleString,
  lastName: validateSimpleString,
  city: validateSimpleString,
  cityId: validateNumber,
  country: validateSimpleString,
  countryId: validateNumber,
  address: validateSimpleString,
  role: validateSimpleString,
  roleId: validateNumber,
}

export default searchUserValidator;
