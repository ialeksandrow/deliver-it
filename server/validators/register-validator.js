import { validateEmail, validatePassword, validateFirstName, validateLastName, validateNumber, validateAddress } from './validate-functions.js';

const registerValidator = {
  email: validateEmail,
  password: validatePassword,
  firstName: validateFirstName,
  lastName: validateLastName,
  city: validateNumber,
  address: validateAddress,
}

export default registerValidator;