const validateMessages = {
  invalidEmail: 'invalid e-mail',
  invalidFirstName: 'invalid first name',
  invalidLastName: 'invalid last name',
  invalidAddress: 'invalid address',
  invalidCountry: 'invalid country',
  invalidCity: 'invalid city',
  invalidPassword: 'invalid password',
  invalidWeight: 'invalid weight',
  invalidLatitude: 'invalid latitude',
  invalidLongitude: 'invalid longitude',
  invalidDate: 'invalid date format',
  invalidNumber: 'invalid number ID, must be integer bigger from zero',
  invalidString: 'invalid string',
  invalidCategory: 'invalid category! Must be string in range 2 - 50 symbols, includes only latin characters'
};

export default validateMessages;