export default {
  departureDate: value =>
    !isNaN(Date.parse(value)) ? true : `${value} is not a valid date`,
  arrivalDate: value =>
    !isNaN(Date.parse(value)) ? true : `${value} is not a valid date`,
  userId: value =>
    !isNaN(parseInt(value)) ? true : `${value} is not a valid number`,
  originId: value =>
    !isNaN(parseInt(value)) ? true : `${value} is not a valid number`,
  destinationId: value =>
    !isNaN(parseInt(value)) ? true : `${value} is not a valid number`,
  shipmentStatusId: value =>
    !isNaN(parseInt(value)) ? true : `${value} is not a valid number`
};
