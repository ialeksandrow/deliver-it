import './config.js';
import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import morgan from 'morgan';
import passport from 'passport';
import jwtStrategy from './authentication/strategy.js';
import apiVersion from './middlewares/api-version.js';
import userRoute from './controllers/user-controller.js';
import categoryRoute from './controllers/categories-controller.js';
import parcelRoute from './controllers/parcels-controller.js';
import { globalRouter } from './controllers/global-controller.js';
import { warehousesRouter } from './controllers/warehouses-controller.js';
import { shipmentsRouter } from './controllers/shipments-controller.js';

const app = express();
const PORT = process.env.EXPRESS_PORT;

// third-party middlewares
passport.use(jwtStrategy);
app.use(express.json(), cors(), helmet(), morgan('dev'));

// self-written middlewares
app.use(apiVersion);
app.use('/global', globalRouter);
app.use('/warehouses', warehousesRouter);
app.use('/shipments', shipmentsRouter);
app.use('/users', userRoute);
app.use('/categories', categoryRoute);
app.use('/parcels', parcelRoute);

app.all('*', async (_, res) => {
  return res.status(404).json({ msg: 'Resource not found.' });
});

app.listen(PORT, console.log(`Server listening on port ${PORT}`));
