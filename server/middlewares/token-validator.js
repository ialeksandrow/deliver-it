import { checkTokenExists } from '../data/user-data.js';

/**
 * Check token exists in black list
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { express.Request } req 
 * @param { express.Response } res 
 * @param { express.NextFunction } next 
 * @returns 
 */
const tokenValidator = async (req, res, next) => {
  const token = req.headers.authorization.replace('Bearer ', '');
  const checkToken = await checkTokenExists(token);

  if (checkToken.length) {
    return res.status(403).send({ message: `invalid token` });
  }

  next();
}

export default tokenValidator;