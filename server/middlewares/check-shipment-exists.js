import shipmentsData from '../data/shipments-data.js';

/**
 * Check shipment exists by ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } object where to check (params, body, query)
 * @param {*} key key name for shipment ID in object
 * @returns { function(express.Request, express.Response, express.NextFunction):void }
 */
const checkShipmentExists = (object, key) => async (req, res, next) => {
  const shipmentId = parseInt(req[object][key]);
  const checkShipment = await shipmentsData.getShipmentById(shipmentId);
  
  if (!checkShipment) {
    return res.status(404).send({ message: `shipment with id '${shipmentId}' don't exists` })
  }

  next();
}

export default checkShipmentExists;