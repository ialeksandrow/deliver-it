/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {express.Request} _
 * @param {express.Response} res
 * @param {express.NextFunction} next
 * @param {string} id
 */

export default async (_, res, next, id) => {
  if (!parseInt(id)) {
    return res.status(400).json({
      msg: 'Ids given in the URL as parameters must be integers above 0.'
    });
  }

  return next();
};
