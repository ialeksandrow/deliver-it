/**
 * Check data in request query
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } validator 
 * @returns { function(express.Request, express.Response, express.NextFunction):object | void }
 */
const queryValidator = (validator) => (req, res, next) => {
  const params = Object.keys(req.query).reduce((acc, el) => {
    if (req.query[el] && !(el in acc)) {
      acc[el.toLowerCase()] = req.query[el];
    }
    return acc;
  }, {});

  const receivedParams = Object.keys(params);
  const availableKeys = Object.keys(validator).map(x => x.toLowerCase());

  const checkParams = receivedParams.map(x => availableKeys.includes(x) ? true : x).filter(x => x !== true);

  if (checkParams.length > 0) {
    return res.status(400).send( { message: `invalid params ${checkParams.join(';')}` } );
  }

  const newValidator = Object.keys(validator).reduce((acc, el) => {
    if (receivedParams.includes(el.toLowerCase())) {
      acc[el.toLowerCase()] = validator[el];
    }
    return acc;
  }, {});


  let errorAlert = false;

  Object.keys(newValidator).forEach(x => {
    if(newValidator[x](params[x]) !== true) {
      console.log(newValidator[x](params[x]));
      errorAlert = true;
    }
  });
    
  if (errorAlert) {
    return res.status(400).json({ message: `invalid query parameter value` });
  }
  
  next();
}

export default queryValidator;