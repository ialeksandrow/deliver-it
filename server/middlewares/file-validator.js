import dotenv from 'dotenv';

/**
 * Check attached files validity
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { express.Request } req 
 * @param { express.Response } res 
 * @param { express.NextFunction } next 
 * @returns 
 */
const fileValidator = async (req, res, next) => {
  if (!req.files) {
    return next();
  }

  if (!req.files.avatar) {
    return res.status(400).send( { message: 'invalid file field' } );
  }

  if (Object.keys(req.files).length > 1) {
    return res.status(400).send( { message: 'must attach only one file' } );
  }

  const filesAvailable = (dotenv.config().parsed.FILES_AVAILABLE).split(',').map(x => x.trim());
  const fileExtension = (req.files.avatar.name).slice((req.files.avatar.name).lastIndexOf('.') + 1);

  if (!filesAvailable.includes(fileExtension)) {
    return res.status(400).send( { message: 'invalid file format' } );
  }
  
  const fileSize = req.files.avatar.size / 1000; // Transform file size to KB
  const FILE_MAX_SIZE = +dotenv.config().parsed.FILE_MAX_SIZE;
  
  if (fileSize > FILE_MAX_SIZE) {
    return res.status(400).send( { message: `file max size must be ${FILE_MAX_SIZE} KB` } );
  }

  next();
};

export default fileValidator;