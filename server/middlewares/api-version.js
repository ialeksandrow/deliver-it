/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {express.Request} _
 * @param {express.Response} res
 * @param {express.NextFunction} next
 */

export default (_, res, next) => {
  res.set('X-API-Version', '1.0.0');

  return next();
};
