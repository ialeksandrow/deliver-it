import userRoles from '../common/user-roles.js';
import { getParcelById } from '../data/parcels-data.js';

/**
 * Allow only parcel owner or operator to continue
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { express.Request } req 
 * @param { express.Response } res 
 * @param { express.NextFunction } next 
 * @returns 
 */
const parcelPermission = async (req, res, next) => {
  const parcel = await getParcelById(req.params.id);
  
  if (parcel === null) {
    return res.status(503).send({ message: `try again later` });
  }

  if (!parcel.length) {
    return res.status(404).send({ message: `parcel with id '${req.params.id}' not found` });
  }

  if (req.user.id !== parcel[0].userId && req.user.role !== userRoles.OPERATOR) {
    return res.status(403).send({ message: `You don't have permission for this operation! `});
  }

  next();
}

export default parcelPermission;