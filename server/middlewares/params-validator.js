import { validateNumber } from '../validators/validate-functions.js';

/**
 * Check param is number
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { express.Request } req 
 * @param { express.Response } res 
 * @param { express.NextFunction } next 
 * @returns 
 */
const paramsValidator = async (req, res, next) => {
  if (validateNumber(req.params.id) !== true) {
    return res.status(400).send( { message: 'invalid param' } );
  }

  next();
}

export default paramsValidator;