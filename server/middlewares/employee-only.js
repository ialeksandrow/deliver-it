import userRoles from '../common/user-roles.js';

/**
 * Allow only operator to continue
 * @param { express.Request } req 
 * @param { express.Response } res 
 * @param { express.NextFunction } next 
 */
const employeeOnly = async (req, res, next) => {
  const userRole = req.user.role;
  
  if (userRole !== userRoles.OPERATOR) {
    return res.status(401).send({ message: `you don't have permission for this operation` });
  }
  
  next();
}

export default employeeOnly;