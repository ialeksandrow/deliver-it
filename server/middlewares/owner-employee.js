import userRoles from '../common/user-roles.js';
import { getUserInfo } from '../data/user-data.js';

/**
 * Allow only right user or operator to continue
 * @param { boolean } fullControl operator have access to other operator data
 * @returns 
 */
const ownerOrEmployee = (fullControl = false) => async (req, res, next) => {
  if (+req.params.id === +req.user.id) {
    return next();
  }

  const checkUser = (await getUserInfo(req.params.id))[0];

  if (!checkUser) {
    return res.status(404).send({ message: `this user don't exists` });
  }
  
  if ((checkUser.roleId === userRoles.OPERATOR && !fullControl) || req.user.role !== userRoles.OPERATOR) {
    return res.status(403).send({ message: `you don't have permission for this operation` });
  }

  next();
};

export default ownerOrEmployee;