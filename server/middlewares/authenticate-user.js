import passport from 'passport';

/**
 * Authentication middleware
 */
const authMiddleware = (req, res, next) => passport.authenticate('jwt', {session: false})(req,res,next);

export default authMiddleware;