import errors from './../common/errors.js';

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} globalData
 * @returns {function():array|number}
 */

const getCountries = async globalData => {
  const dataResult = await globalData.getCountries();

  if (!dataResult.length) {
    return errors.NOT_FOUND;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} globalData
 * @returns {function(number):object|number}
 */

const getCountryById = globalData => async id => {
  const dataResult = await globalData.getCountryById(id);

  if (!dataResult) {
    return errors.NOT_FOUND;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} globalData
 * @returns {function():array|number}
 */

const getCities = async globalData => {
  const dataResult = await globalData.getCities();

  if (!dataResult.length) {
    return errors.NOT_FOUND;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} globalData
 * @returns {function(number):object|number}
 */

const getCityById = globalData => async id => {
  const dataResult = await globalData.getCityById(id);

  if (!dataResult) {
    return errors.NOT_FOUND;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} globalData
 * @returns {function(string):object|number}
 */

const getCityByName = globalData => async name => {
  const dataResult = await globalData.getCityByName(name);

  if (!dataResult) {
    return errors.NOT_FOUND;
  }

  return dataResult;
};

export default {
  getCountries,
  getCountryById,
  getCities,
  getCityById,
  getCityByName
};
