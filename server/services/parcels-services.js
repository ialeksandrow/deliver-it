import errors from '../common/errors.js';
import columnAliases from '../common/column-aliases.js';
import searchParcelValidator from '../validators/parcels-search-validator.js';
import userRoles from '../common/user-roles.js';
import shipmentStatuses from '../common/shipment-statuses.js';
import parcelStatuses from '../common/parcel-statuses.js';
import deliveryTypes from '../common/delivery-types.js';
import { objectKeysToLowerCase } from '../common/help-functions.js';


/**
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } parcelData 
 * @returns { function(number):object }
 */
export const getParcel = (parcelData) => async (parcelId) => {
  const checkParcel = await parcelData.getParcelById(parcelId);

  if (!checkParcel) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  if (!checkParcel.length) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    }
  }

  return {
    error: null,
    data: checkParcel,
  }
}


/**
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } parcelData 
 * @returns { function(number):object }
 */
export const getUserParcels = (parcelData) => async (userId) => {
  const criteria = `WHERE p.user_id = ? AND p.is_deleted = 0`;
  const result = await parcelData.getParcelById(userId, criteria);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
}


/**
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } parcelData 
 * @returns { function(object):object }
 */
export const searchParcels = (parcelData) => async (parameters, user) => {
  const sqlColumns = objectKeysToLowerCase(columnAliases);
  parameters = objectKeysToLowerCase(parameters);

  if (parseInt(user.role) === parseInt(userRoles.CUSTOMER)) {
    parameters['customerId'.toLowerCase()] = user.id;
  }

  let orderString = 'ORDER BY p.parcel_id DESC';
  let queryString = '';

  const keyParameters = Object.keys(parameters).filter(x => x !== 'sortBy'.toLowerCase() && x !== 'desc');

  if (keyParameters.length) {
    queryString = `WHERE ` +
      keyParameters
      .filter(x => x !== 'sortBy'.toLowerCase() && x !== 'desc')
      .map(x => {
        if (x === 'customerId'.toLowerCase() && parseInt(user.role) === parseInt(userRoles.CUSTOMER)) {
          return `${sqlColumns[x]} = '${parameters[x]}'`;
        }

        if (x === 'categoryId'.toLowerCase() || x === 'warehouseId'.toLowerCase()) {
          return `${sqlColumns[x]} = '${parameters[x]}'`;
        }

        if (x === 'weight') {
          return `${sqlColumns[x]} >= '${parameters[x]}'`;
        }

        return `${sqlColumns[x]} LIKE '%${parameters[x]}%'`
        })
      .join(' AND ');
  }

  if ('sortBy'.toLowerCase() in parameters) {
    const orderArray = parameters['sortBy'.toLowerCase()].split(',').map(x => x.trim().toLowerCase());
    const validFields = Object.keys(objectKeysToLowerCase(searchParcelValidator));
    if (!orderArray.every(x => validFields.includes(x))) {
      return {
        error: errors.INVALID_PARAM,
        data: null,
      }
    }

    let typeOrder = 'DESC';
    if ('desc' in parameters) {
      parameters.desc === 'true'
        ? typeOrder = 'DESC'
        : typeOrder = 'ASC'
    }

    orderString = `ORDER BY ` + orderArray.map(x => `${sqlColumns[x]} ${typeOrder}`).join(', ');
  }

  if (queryString) {
    queryString = queryString + ' AND p.is_deleted=0';
  } else {
    queryString = queryString + 'WHERE p.is_deleted=0';
  }
  
  const result = await parcelData.getParcelById(null, queryString, orderString);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
}


/**
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } parcelData 
 * @returns { function(number): Promise <boolean> }
 */
export const setNotified = (parcelData) => async (parcelId) => {
  const result = await parcelData.updateNotifiedUser(parcelId);

  if (!result) {
    return false;
  }

  return true;
}


/**
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } postData 
 * @returns { function(object, number):object }
 */
export const createParcel = (parcelData) => async (postData, userId) => {

  if (postData.warehouseId && !await parcelData.checkWarehouseExistsById(postData.warehouseId)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    }
  }

  const result = await parcelData.createNewParcel(postData, userId);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
}


/**
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } parcelData 
 * @returns { function(number, number):object }
 */
export const updateParcelStatus = (parcelData) => async (statusId, parcelId) => {
  const checkParcel = await parcelData.getParcelById(parcelId);
  
  if (!checkParcel) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }
  
  if (!checkParcel.length) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    }
  }

  if (checkParcel[0].parcelStatusId === parseInt(statusId)) {
    return {
      error: null,
      data: checkParcel,
    }
  }

  const result = await parcelData.updateParcelStatusById(statusId, parcelId);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
}


/**
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } parcelData 
 * @returns { function(number, number):object }
 */
export const setShipmentToParcel = (parcelData) => async (parcelId, shipmentId) => {
  const checkParcel = await parcelData.getParcelById(parcelId);
  
  if (!checkParcel) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }
  
  if (!checkParcel.length) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    }
  }

  if (parseInt(checkParcel[0].shipmentId) === parseInt(shipmentId)) {
    return {
      error: null,
      data: checkParcel,
    }
  }

  const result = await parcelData.updateParcelShipmentById(parcelId, shipmentId);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
}


/**
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } parcelData 
 * @returns { function(number):object }
 */
export const getUnseenParcels = (parcelData) => async (userId) => {
  const result = await parcelData.getUnseenParcelsByUserId(userId);
  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
}


/**
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } parcelData 
 * @returns { function(number, boolean):object }
 */
export const deleteParcel = (parcelData) => async (parcelId, isOperator) => {
  const checkParcel = await parcelData.getParcelById(parcelId);
  
  if (checkParcel[0].parcelStatusId !== parcelStatuses.AWAITING && !isOperator) {
    return {
      error: errors.OPERATION_NOT_ALLOWED,
      data: null,
    }
  }

  const result = await parcelData.deleteParcelById(parcelId);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: checkParcel,
  }
}


/**
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } postData 
 * @returns { function(number, object):object }
 */
export const updateParcel = (parcelData) => async (parcelId, postData) => {
  const checkParcel = (await parcelData.getParcelById(parcelId))[0];

  const deliveryTypeChanged = +checkParcel.deliveryTypeId !== +postData.deliveryTypeId;
  const warehouseChanged = +checkParcel.warehouseId !== +postData.warehouseId;
  
  if ((deliveryTypeChanged || warehouseChanged) && checkParcel.shipment && checkParcel.shipment.statusId > shipmentStatuses.PREPARING) {
    return {
      error: errors.OPERATION_NOT_ALLOWED,
      data: null,
    }
  }
  
  if (+postData.deliveryTypeId === deliveryTypes.ADDRESS) {
    postData.warehouseId = null;
  } else {
    const checkWarehouse = await parcelData.checkWarehouseExistsById(postData.warehouseId);
  
    if (!checkWarehouse) {
      return {
        error: errors.NOT_FOUND,
        data: null,
      }
    }
  }

  const result = await parcelData.updateParcelById(postData, parcelId);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
};


/**
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } postData 
 * @returns { function(number):object }
 */
export const removeFromShipment = (parcelData) => async (parcelId) => {
  const checkParcel = await parcelData.getParcelById(parcelId);

  if (!checkParcel.length) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    }
  }

  if (checkParcel[0].shipmentId === null) {
    return {
      error: null,
      data: checkParcel,
    }
  }

  const result = await parcelData.updateParcelShipmentById(parcelId, null);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
}