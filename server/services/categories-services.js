import errors from '../common/errors.js';

/**
 * Get all categories
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } categoryData
 * @returns { object }
 */
export const getAllCategories = async (categoryData) => {
  const result = await categoryData.AllCategories();

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
};


/**
 * Get info for single category
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } categoryData
 * @returns { function(number):object }
 */
export const getCategory = (categoryData) => async (categoryId) => {
  const result = await categoryData.getCategoryById(categoryId);  

  if (result === null) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  if (!result.length) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
};


/**
 * Create new category
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } categoryData 
 * @returns { function(string):object }
 */
export const createCategory = (categoryData) => async (categoryName) => {
  const checkCategory = await categoryData.getCategoryByName(categoryName);

  if (checkCategory.length) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    }
  }

  const result = await categoryData.createNewCategory(categoryName);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
}


/**
 * Update category name
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } categoryData 
 * @returns { function(number, string):object }
 */
export const updateCategory = (categoryData) => async (categoryId, newName) => {
  const checkCategory = await categoryData.getCategoryById(categoryId);
  
  if (!checkCategory.length) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    }
  }

  const checkName = await categoryData.getCategoryByName(newName);
  if (checkName.length) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    }
  }

  const result = await categoryData.updateCategoryById(categoryId, newName);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
}


/**
 * Delete category
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } categoryData 
 * @returns { function(number):object }
 */
export const deleteCategory = (categoryData) => async (categoryId) => {
  const checkCategory = await categoryData.getCategoryById(categoryId);

  if (!checkCategory.length) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    }
  }

  const result = await categoryData.deleteCategoryById(categoryId);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: checkCategory,
  }
}