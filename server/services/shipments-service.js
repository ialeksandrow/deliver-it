import { v4 as uuidv4 } from 'uuid';
import errors from './../common/errors.js';

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} shipmentsData
 * @returns {function():array|number}
 */

const getShipments = async shipmentsData => {
  const dataResult = await shipmentsData.getShipments();

  if (!dataResult.length) {
    return errors.NOT_FOUND;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} shipmentsData
 * @returns {function(number):object|number}
 */

const getShipmentById = shipmentsData => async id => {
  const dataResult = await shipmentsData.getShipmentById(id);

  if (!dataResult) {
    return errors.NOT_FOUND;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} shipmentsData
 * @returns {function(string):object|number}
 */

const getShipmentByName = shipmentsData => async name => {
  const dataResult = await shipmentsData.getShipmentByName(name);

  if (!dataResult) {
    return errors.NOT_FOUND;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} shipmentsData
 * @returns {function(number, array):object|number}
 */

const updateShipment = shipmentsData => async (id, updateData) => {
  const existingResult = await shipmentsData.getShipmentById(id);

  if (!existingResult) {
    return errors.NOT_FOUND;
  }

  const dataResult = await shipmentsData.updateShipment(id, updateData);

  if (!dataResult) {
    return errors.SERVER_PROBLEM;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} shipmentsData
 * @returns {function(object):object|number}
 */

const addShipment = shipmentsData => async addData => {
  const name = uuidv4();
  const dataResult = await shipmentsData.addShipment(
    name,
    Object.values(addData)
  );

  if (!dataResult) {
    return errors.SERVER_PROBLEM;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} shipmentsData
 * @returns {function(number):object|number}
 */

const deleteShipment = shipmentsData => async id => {
  const existingResult = await shipmentsData.getShipmentById(id);

  if (!existingResult) {
    return errors.NOT_FOUND;
  }

  const dataResult = await shipmentsData.deleteShipment(id);

  if (!dataResult) {
    return errors.SERVER_PROBLEM;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} shipmentsData
 * @param {object} warehousesData
 * @returns {function(string, string):array|number}
 */

// prettier-ignore
const filterShipment = (shipmentsData, warehousesData) => async (warehouseId, userEmail) => {
  if (warehouseId) {
    const existingResult = await warehousesData.getWarehouseById(parseInt(warehouseId));

    if (!existingResult) {
      return errors.NOT_FOUND;
    }
  }

  const dataResult = await shipmentsData.filterShipment(
    warehouseId,
    userEmail
  );

  if(!dataResult.length) {
    return errors.NOT_MATCH;
  }

  if (!dataResult) {
    return errors.SERVER_PROBLEM;
  }

  return dataResult;
};

export default {
  getShipments,
  getShipmentById,
  getShipmentByName,
  updateShipment,
  addShipment,
  deleteShipment,
  filterShipment
};
