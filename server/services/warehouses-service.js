import errors from './../common/errors.js';

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} warehousesData
 * @returns {function():array|number}
 */

const getWarehouses = async warehousesData => {
  const dataResult = await warehousesData.getWarehouses();

  if (!dataResult.length) {
    return errors.NOT_FOUND;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} warehousesData
 * @returns {function(number):object|number}
 */

const getWarehouseById = warehousesData => async id => {
  const dataResult = await warehousesData.getWarehouseById(id);

  if (!dataResult) {
    return errors.NOT_FOUND;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} warehousesData
 * @returns {function(number, array):object|number}
 */

const updateWarehouse = warehousesData => async (id, updateData) => {
  const existingResult = await warehousesData.getWarehouseById(id);

  if (!existingResult) {
    return errors.NOT_FOUND;
  }

  const dataResult = await warehousesData.updateWarehouse(id, updateData);

  if (!dataResult) {
    return errors.SERVER_PROBLEM;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} warehousesData
 * @returns {function(object):object|number}
 */

const addWarehouse = warehousesData => async addData => {
  const existingResult = await warehousesData.getWarehouses();

  if (
    existingResult.some(
      warehouse =>
        warehouse.name.toLowerCase() === addData.name.toLowerCase() ||
        warehouse.address.toLowerCase() === addData.address.toLowerCase()
    )
  ) {
    return errors.DUPLICATE_RECORD;
  }

  const dataResult = await warehousesData.addWarehouse(Object.values(addData));

  if (!dataResult) {
    return errors.SERVER_PROBLEM;
  }

  return dataResult;
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {object} warehousesData
 * @returns {function(number):object|number}
 */

const deleteWarehouse = warehousesData => async id => {
  const existingResult = await warehousesData.getWarehouseById(id);

  if (!existingResult) {
    return errors.NOT_FOUND;
  }

  const dataResult = await warehousesData.deleteWarehouse(id);

  if (!dataResult) {
    return errors.SERVER_PROBLEM;
  }

  return dataResult;
};

export default {
  getWarehouses,
  getWarehouseById,
  updateWarehouse,
  addWarehouse,
  deleteWarehouse
};
