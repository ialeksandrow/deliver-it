import errors from '../common/errors.js';
import bcrypt from 'bcrypt';
import createToken from '../authentication/create-token.js';
import userRoles from '../common/user-roles.js';
import dotenv from 'dotenv';
import columnAliases from '../common/column-aliases.js';
import { objectKeysToLowerCase } from '../common/help-functions.js';

const AVATARS_FOLDER = dotenv.config().parsed.AVATARS_FOLDER;
const SERVER_URL = dotenv.config().parsed.SERVER_URL;
const EXPRESS_PORT = dotenv.config().parsed.EXPRESS_PORT;


/**
 * User login
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } userData 
 * @returns { function(string, string):object }
 */
export const userLogin = (userData) => async (userMail, userPass) => {
  const checkUser = await userData.checkEmailExists(userMail);

  if (!checkUser && checkUser === false) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    }
  }

  if (!checkUser && checkUser === null) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }
  
  if (!await bcrypt.compare(userPass, checkUser.password)) {
    return {
      error: errors.WRONG_CREDENTIALS,
      data: null,
    }
  }

  const token = createToken({
    id: checkUser.user_id,
    firstName: checkUser.first_name,
    lastName: checkUser.last_name,
    avatar: checkUser.avatar,
    role: checkUser.role_id,
    eMail: checkUser.email,
    city: checkUser.cityName,
    country: checkUser.countryName,
    associatedParcels: checkUser.associatedParcels,
    associatedShipments: checkUser.associatedShipments,
  })

  return {
    error: null,
    data: { token },
  }
};


/**
 * Add new user to database
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } userData 
 * @returns { function(string, string):object }
 */
export const userRegister = (userData) => async (userInfo, uploadFiles) => {
  if (await userData.checkEmailExists(userInfo.email)) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    }
  }

  if (!await userData.checkCityExists(userInfo.city)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    }
  }

  // Hash password and add default user role
  userInfo.password = await bcrypt.hash(userInfo.password, 10);
  userInfo.role_id = userRoles.CUSTOMER;

  const newUser = await userData.registerUser(userInfo);  

  if (!newUser) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }
  
  if (uploadFiles) {
    const newAvatar = uploadFiles.avatar;
    let filename = newAvatar.name.split('.');
    filename = `${newUser}.${filename[filename.length - 1]}`;

    try {
      await newAvatar.mv(`${AVATARS_FOLDER}/${filename}`);
      await userData.setAvatar(`http://${SERVER_URL}:${EXPRESS_PORT}/users/avatar/${filename}`, newUser);
    } catch (error) {
      return null;
    }
  }

  return {
    error: null,
    data: await userData.getUserInfo(newUser),
  }
};


/**
 * Get count of all customers
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } userData 
 * @returns { function():object }
 */
export const customersCount = (userData) => async () => {
  const result = await userData.getCustomersCount();
  const totalCountries = await userData.getCountriesNumber();
  
  if (!result || !totalCountries) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  const totalCustomers = result.reduce((acc, el) => acc = acc + el.customersCount, 0);
  
  return {
    error: null,
    data: { totalCustomers, totalCountries , byCountry: result },
  }
}


/**
 * Change user password
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } userData 
 * @returns { function(number, object):object }
 */
export const changeUserPassword = (userData) => async (userId, passData) => {
  if (passData.newPassword !== passData.newPasswordConfirm) {
    return {
      error: errors.NOT_MATCH,
      data: null,
    }
  }

  const { password: checkPass } = (await userData.getUserPasswordById(userId))[0];  

  if (!await bcrypt.compare(passData.oldPassword, checkPass)) {
    return {
      error: errors.WRONG_CREDENTIALS,
      data: null,
    }
  }

  const newPassword = await bcrypt.hash(passData.newPassword, 10);
  const result = await userData.updateUserPasswordById(newPassword, userId);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: await userData.getUserInfo(userId),
  }
}


/**
 * Update user personal info by ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } userData 
 * @returns { function(number, object, string):object }
 */
export const updateUserInfo = (userData) => async (userId, userDetails, userAvatar) => {

  userDetails.avatar = '';

  if (userAvatar) {
    const newAvatar = userAvatar.avatar;
    let filename = newAvatar.name.split('.');
    filename = `${userId}.${filename[filename.length - 1]}`;
    try {
      await newAvatar.mv(`${AVATARS_FOLDER}/${filename}`);
      userDetails.avatar = `http://${SERVER_URL}:${EXPRESS_PORT}/users/avatar/${filename}`;
    } catch (error) {
      return null;
    }
  }

  const result = await userData.updateUserInfoById(userId, userDetails);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: await userData.getUserInfo(userId),
  }

};

/**
 * Delete user
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } userData 
 * @returns { function(number):object }
 */
export const deleteUser = (userData) => async (userId) => {
  const checkUser = await userData.getUserInfo(userId);

  if (!checkUser.length) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    }
  }

  const result = await (userData.deleteUserById(userId));

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: checkUser,
  }
}


/**
 * Search my multiple criteria
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } userData 
 * @returns { function(object):object }
 */
export const searchByCriteria = (userData) => async (parameters) => {  
  const sqlColumns = objectKeysToLowerCase(columnAliases);
  parameters = objectKeysToLowerCase(parameters);

  const queryString = `WHERE ` +
    Object.keys(parameters)
    .map(x => `${sqlColumns[x]} LIKE '%${parameters[x]}%'`)
    .join(' AND ');
  
  const result = await userData.getUserInfo(null, queryString);
  
  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
}


/**
 * Search by keyword in multiple fields
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } userData 
 * @returns { function(object):object }
 */
export const searchByKeyWord = (userData) => async (words) => {
  const keyWord = objectKeysToLowerCase(words).keyword;

  const availableFields = [
    columnAliases.email,
    columnAliases.firstName,
    columnAliases.lastName,
    columnAliases.country,
    columnAliases.city,
    columnAliases.address
  ];

  const queryString = `WHERE ` +
    availableFields
    .map(x => `${x} LIKE '%${keyWord}%'`)
    .join(' OR ');

  const result = await userData.getUserInfo(null, queryString);
  
  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
}


/**
 * Get user info by Id
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } userData 
 * @returns { function(number):object }
 */
export const getUser = (userData) => async (userId) => {
  const result = await userData.getUserInfo(userId);
  
  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
}


/**
 * Add user token to black list tokens
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } userData 
 * @returns { function(string):object } 
 */
export const logOutUser = (userData) => async (token) => {
  const result = await userData.addTokenToBlackList(token);

  if (!result) {
    return {
      error: errors.SERVER_PROBLEM,
      data: null,
    }
  }

  return {
    error: null,
    data: result,
  }
}
