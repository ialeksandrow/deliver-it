export default {
  PREPARING: 1,
  COMPLETED: 2,
  ON_WAY: 3,
  DELIVERED: 4,
};