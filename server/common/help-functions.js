/**
 * Convert all object keys to lowerCase, remove keys duplicates and keys without value
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } object 
 * @returns { object }
 */
export const objectKeysToLowerCase = (object) => {
  const newObject =  Object.keys(object).reduce((acc, el) => {
    if (object[el] && !(el in acc)) {
      acc[el.toLowerCase()] = object[el];
    }
    return acc;
  }, {});

  return newObject;
}