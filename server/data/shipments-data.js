import pool from './pool.js';

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {array}
 */

const getShipments = async () => {
  const sql = `
  SELECT s.name, s.departure_date AS departureDate, s.arrival_date AS arrivalDate, u.email AS operatorEmail, w.name AS originWarehouse, w2.name AS destinationWarehouse, ss.name AS status
  FROM shipments AS s
  JOIN users AS u ON s.user_id = u.user_id
  JOIN warehouses AS w ON s.origin_id = w.warehouse_id
  JOIN warehouses AS w2 ON s.destination_id = w2.warehouse_id
  JOIN shipment_statuses AS ss ON s.shipment_status_id = ss.shipment_status_id
  WHERE s.is_deleted = 0;
  `;

  return await pool.query(sql);
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number} id
 * @returns {object|undefined}
 */

const getShipmentById = async id => {
  const sql = `
  SELECT s.shipment_id AS id, s.name, s.departure_date AS departureDate, s.arrival_date AS arrivalDate, u.user_id AS userId, u.email AS operatorEmail, u.first_name AS operatorFirstName, u.last_name AS operatorLastName, w.name AS originWarehouse, w2.name AS destinationWarehouse, ss.name AS status
  FROM shipments AS s
  JOIN users AS u ON s.user_id = u.user_id
  JOIN warehouses AS w ON s.origin_id = w.warehouse_id
  JOIN warehouses AS w2 ON s.destination_id = w2.warehouse_id
  JOIN shipment_statuses AS ss ON s.shipment_status_id = ss.shipment_status_id
  WHERE s.shipment_id = ? AND s.is_deleted = 0;
  `;

  return (await pool.query(sql, [id]))[0];
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string} name
 * @returns {object|undefined}
 */

const getShipmentByName = async name => {
  const sql = `
  SELECT s.shipment_id AS id, s.name, s.departure_date AS departureDate, s.arrival_date AS arrivalDate, u.user_id AS userId, u.email AS operatorEmail, u.first_name AS operatorFirstName, u.last_name AS operatorLastName, w.warehouse_id AS originId, w.name AS originWarehouse, w2.warehouse_id AS destinationId, w2.name AS destinationWarehouse, ss.shipment_status_id AS shipmentStatusId, ss.name AS status
  FROM shipments AS s
  JOIN users AS u ON s.user_id = u.user_id
  JOIN warehouses AS w ON s.origin_id = w.warehouse_id
  JOIN warehouses AS w2 ON s.destination_id = w2.warehouse_id
  JOIN shipment_statuses AS ss ON s.shipment_status_id = ss.shipment_status_id
  WHERE s.name = ? AND s.is_deleted = 0;
  `;

  return (await pool.query(sql, [name]))[0];
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number} id
 * @param {array} updateData
 * @returns {object|null}
 */

const updateShipment = async (id, updateData) => {
  try {
    const sql = `
    UPDATE shipments
    SET departure_date = ?, arrival_date = ?, user_id = ?, origin_id = ?, destination_id = ?, shipment_status_id = ?
    WHERE shipment_id = ? AND is_deleted = 0;
    `;

    await pool.query(sql, [...updateData, id]);

    return getShipmentById(id);
  } catch (error) {
    return null;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string} name
 * @param {array} addData
 * @returns {object|null}
 */

const addShipment = async (name, addData) => {
  try {
    const sql = `
    INSERT INTO shipments (name, departure_date, arrival_date, user_id, origin_id, destination_id, shipment_status_id)
    VALUES (?, ?, ?, ?, ?, ?, ?);
    `;

    const addedDataId = (await pool.query(sql, [name, ...addData])).insertId;

    return getShipmentById(addedDataId);
  } catch (error) {
    return null;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number} id
 * @returns {object|null}
 */

const deleteShipment = async id => {
  try {
    const sql = `
    UPDATE shipments
    SET is_deleted = 1
    WHERE shipment_id = ?;
    `;

    const deletedData = getShipmentById(id);

    await pool.query(sql, [id]);

    return deletedData;
  } catch (error) {
    return null;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string} [warehouseId=null]
 * @param {string} [userEmail=null]
 * @returns {array|null}
 * @description Filter shipment by warehouseId, userEmail or both. Parameter warehouseId is associated with the 'warehouses' database table, while userEmail is related to 'users'. Each parameter should be in the appropriate foreign key range of its' table. Parameter userEmail is considered while filtering, only if there is at least one relation between a parcel and a shipment.
 */

const filterShipment = async (warehouseId = null, userEmail = null) => {
  try {
    const sql = `
    SELECT s.name, s.departure_date AS departureDate, s.arrival_date AS arrivalDate, u.email AS operatorEmail, w.name AS originWarehouse, w2.name AS destinationWarehouse, ss.name AS status
    FROM shipments AS s
    JOIN users AS u ON s.user_id = u.user_id
    JOIN warehouses AS w ON s.origin_id = w.warehouse_id
    JOIN warehouses AS w2 ON s.destination_id = w2.warehouse_id
    JOIN shipment_statuses AS ss ON s.shipment_status_id = ss.shipment_status_id
    WHERE s.origin_id = ? OR s.destination_id = ? OR s.shipment_id IN (
      SELECT p.shipment_id
      FROM parcels AS p
      JOIN users AS u ON p.user_id = u.user_id
      WHERE u.email LIKE '%${userEmail}%'
    ) AND s.is_deleted = 0;
    `;

    return await pool.query(sql, [warehouseId, warehouseId]);
  } catch (error) {
    return null;
  }
};

export default {
  getShipments,
  getShipmentById,
  getShipmentByName,
  updateShipment,
  addShipment,
  deleteShipment,
  filterShipment
};
