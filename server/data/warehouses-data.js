import pool from './pool.js';

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {array}
 */

const getWarehouses = async () => {
  const sql = `
  SELECT w.warehouse_id AS id, w.name, w.address, w.latitude, w.longitude, c.name AS cityName, (
    SELECT COUNT(s.shipment_id)
    FROM shipments AS s
    WHERE is_deleted = 0 AND s.origin_id = w.warehouse_id OR s.destination_id = w.warehouse_id
  ) AS shipmentsCount, (
    SELECT COUNT(p.parcel_id)
    FROM parcels AS p
    JOIN shipments AS s2 ON s2.origin_id = w.warehouse_id OR s2.destination_id = w.warehouse_id
    WHERE p.is_deleted = 0 AND p.shipment_id = s2.shipment_id
  ) AS parcelsCount
  FROM warehouses AS w
  JOIN cities AS c ON w.city_id = c.city_id
  WHERE w.is_deleted = 0;
  `;

  return await pool.query(sql);
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number} id
 * @returns {object|undefined}
 */

const getWarehouseById = async id => {
  const sql = `
  SELECT w.warehouse_id AS id, w.name, w.address, w.latitude, w.longitude, c.name AS cityName
  FROM warehouses AS w
  JOIN cities AS c ON w.city_id = c.city_id
  WHERE w.warehouse_id = ? AND w.is_deleted = 0;
  `;

  return (await pool.query(sql, [id]))[0];
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number} id
 * @param {array} updateData
 * @returns {object|null}
 */

const updateWarehouse = async (id, updateData) => {
  try {
    const sql = `
    UPDATE warehouses
    SET name = ?, address = ?, latitude = ?, longitude = ?, city_id = ?
    WHERE warehouse_id = ?;
    `;

    await pool.query(sql, [...updateData, id]);

    return getWarehouseById(id);
  } catch (error) {
    return null;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {array} addData
 * @returns {object|null}
 */

const addWarehouse = async addData => {
  try {
    const sql = `
    INSERT INTO warehouses (name, address, latitude, longitude, city_id)
    VALUES (?, ?, ?, ?, ?);
    `;

    const addedDataId = (await pool.query(sql, [...addData])).insertId;

    return getWarehouseById(addedDataId);
  } catch (error) {
    return null;
  }
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number} id
 * @returns {object|null}
 */

const deleteWarehouse = async id => {
  try {
    const sql = `
    UPDATE warehouses
    SET is_deleted = 1
    WHERE warehouse_id = ?;
    `;

    const deletedData = getWarehouseById(id);

    await pool.query(sql, [id]);

    return deletedData;
  } catch (error) {
    return null;
  }
};

export default {
  getWarehouses,
  getWarehouseById,
  updateWarehouse,
  addWarehouse,
  deleteWarehouse
};
