import pool from './pool.js';
import warehousesData from './warehouses-data.js';

/**
 * Get parcel info by ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } parcelId 
 * @returns { object | null }
 */
export const getParcelById = async (parcelId, criteria = `WHERE p.parcel_id = ? AND p.is_deleted = 0`, orderBy =`ORDER BY p.parcel_id DESC`) => {
  const sqlQuery = `
    SELECT
    p.parcel_id as parcelId,
    p.name as parcelName,
    p.weight as parcelWeight,
    p.parcel_category_id as parcelCategoryId,
    pc.name as parcelCategory,
    d.delivery_type_id as deliveryTypeId,
    d.name as deliveryType,
    p.user_id as userId,
    u.first_name as userFirstName,
    u.last_name as userLastName,
    u.email as userEmail,
    p.parcel_delivery_status_id as parcelStatusId,
    pds.name as parcelStatus,
    u.address userAddress,
    (SELECT ct2.name FROM users as us2 JOIN cities as ct2 ON us2.city_id = ct2.city_id WHERE us2.user_id = p.user_id) as userCity,
    (SELECT co3.name FROM users as us3 JOIN cities as ct3 ON us3.city_id = ct3.city_id JOIN countries as co3 ON ct3.country_id = co3.country_id WHERE us3.user_id = p.user_id) as userCountry,
    IF (p.delivery_type_id = 1,
      (SELECT sc.name FROM users as su JOIN cities as sc ON su.city_id = sc.city_id WHERE su.user_id = p.user_id),
      (SELECT sc.name FROM warehouses as sw JOIN cities as sc ON sw.city_id = sc.city_id WHERE sw.warehouse_id = p.destination_id)
    ) as deliveryCity,
    IF (p.delivery_type_id = 1,
      (SELECT ssc.name FROM users as su JOIN cities as sc ON su.city_id = sc.city_id JOIN countries as ssc ON sc.country_id = ssc.country_id WHERE su.user_id = p.user_id),
      (SELECT ssc.name FROM warehouses as sw JOIN cities as sc ON sw.city_id = sc.city_id JOIN countries as ssc ON sc.country_id = ssc.country_id WHERE sw.warehouse_id = p.destination_id)
    ) as deliveryCountry,
    w.warehouse_id as warehouseId,
    w.name as warehouseName,
    p.user_notified as ownerIsNotified,
    p.shipment_id as shipment
    FROM parcels as p
    JOIN parcel_categories as pc ON p.parcel_category_id = pc.parcel_category_id
    JOIN delivery_types as d ON d.delivery_type_id = p.delivery_type_id
    JOIN users as u ON u.user_id = p.user_id
    JOIN parcel_delivery_statuses as pds ON p.parcel_delivery_status_id = pds.parcel_delivery_status_id
    LEFT JOIN warehouses as w ON IF(p.delivery_type_id = 2, p.destination_id = w.warehouse_id, null)
    LEFT JOIN shipments as sh ON IF(p.shipment_id != null, p.shipment_id = sh.shipment_id, null)
    ${criteria}
    ${orderBy}`;
  
  try {
    const result = await pool.query(sqlQuery, [parcelId]);
    return await Promise.all (result.map(async x => {
      if (x.shipment) {
        x.shipment = (await getInfoForShipment(x.shipment))[0];
      }
      return x;
    }));
  } catch (error) {
    return null;
  }
}


/**
 * Create new parcel and return it
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } parcelData 
 * @returns { object | null }
 */
export const createNewParcel = async (parcelData, userId) => {
  const sqlQuery = `
    INSERT
    INTO parcels(name, weight, user_id, parcel_category_id, delivery_type_id, destination_id)
    VALUES (?, ?, ?, ?, ?, ?)`;
  
  const data = [
    parcelData.parcelName,
    parcelData.parcelWeight,
    userId,
    parcelData.parcelCategory,
    parcelData.deliveryTypeId,
    parcelData.warehouseId || null,
  ];
  
  try {
    const newParcel = await pool.query(sqlQuery, data);
    return await getParcelById(newParcel.insertId);
  } catch (error) {
    return null;
  }
}


/**
 * Update user notified field for by parcel ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } parcelId 
 * @returns { object | null }
 */
export const updateNotifiedUser = async (parcelId) => {
  const sqlQuery = `UPDATE parcels SET user_notified = 1 WHERE parcel_id = ?`;

  try {
    return await pool.query(sqlQuery, [parcelId]);
  } catch (error) {
    return null;
  }
}


/**
 * Change parcel status by parcel ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } statusId 
 * @param { number } parcelId 
 * @returns { object | null }
 */
export const updateParcelStatusById = async (statusId, parcelId) => {
  const sqlQuery = `UPDATE parcels SET parcel_delivery_status_id = ? WHERE parcel_id = ?`;

  try {
    await pool.query(sqlQuery, [statusId, parcelId]);
    return await getParcelById(parcelId);
  } catch (error) {
    return null;
  }
}


/**
 * Change parcel shipment ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } parcelId 
 * @param { number } shipmentId 
 * @returns { object | null }
 */
export const updateParcelShipmentById = async (parcelId, shipmentId) => {
  const sqlQuery = `UPDATE parcels SET shipment_id = ? WHERE parcel_id = ?`;

  try {
    await pool.query(sqlQuery, [shipmentId, parcelId]);
    return await getParcelById(parcelId);
  } catch (error) {
    return null;
  }
}


/**
 * Get count for parcel what be in shipment and owner don't know yet
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } userId 
 * @returns { object | null }
 */
export const getUnseenParcelsByUserId = async (userId) => {
  const sqlQuery = `SELECT COUNT(*) as unSeenParcels FROM parcels WHERE user_id = ? AND shipment_id is NOT NULL AND user_notified = 0`;

  try {
    return await pool.query(sqlQuery, [userId]);
  } catch (error) {
    return null;
  }
}


/**
 * Get information about the shipment of which the parcel is a part
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } shipmentId 
 * @returns { object | null }
 */
export const getInfoForShipment = async (shipmentId) => {
  const sqlQuery = `
  SELECT
    s.shipment_id as shipmentId,
    s.name,
    s.departure_date AS departureDate,
    s.arrival_date AS arrivalDate,
    w.warehouse_id as originWarehouseId,
    w.name AS originWarehouse,
    w2.warehouse_id as destinationWarehouseId,
    w2.name AS destinationWarehouse,
    ss.shipment_status_id as statusId,
    ss.name AS status
    FROM shipments AS s  
    JOIN warehouses AS w ON s.origin_id = w.warehouse_id
    JOIN warehouses AS w2 ON s.destination_id = w2.warehouse_id
    JOIN shipment_statuses AS ss ON s.shipment_status_id = ss.shipment_status_id
    WHERE s.shipment_id = ? AND s.is_deleted = 0`

  try {
    return await pool.query(sqlQuery, [shipmentId]);
  } catch (error) {
    return null;
  }
}


/**
 * Delete parcel by ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } parcelId 
 * @returns { object | null }
 */
export const deleteParcelById = async (parcelId) => {
  const sqlQuery = `UPDATE parcels SET is_deleted = 1 WHERE parcel_id = ?`;

  try {
    await pool.query(sqlQuery, [parcelId]);
    return true;
  } catch (error) {
    return null;
  }
}


/**
 * Update parcel by ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } postData 
 * @param { number } parcelId 
 * @returns { object | null }
 */
export const updateParcelById = async (postData, parcelId) => {
  const sqlQuery = `
    UPDATE parcels SET
    name = ?,
    weight = ?,
    parcel_category_id = ?,
    delivery_type_id = ?,
    destination_id = ?
    WHERE parcel_id = ?`;

  const data = [
    postData.parcelName,
    postData.parcelWeight,
    postData.parcelCategory,
    postData.deliveryTypeId,
    postData.warehouseId,
    parcelId
  ];

  try {
    await pool.query(sqlQuery, data);
    return await getParcelById(parcelId);
  } catch (error) {
    return null;
  }
}


/**
 * Check warehouse exists by warehouse ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } warehouseId 
 * @returns { boolean }
 */
export const checkWarehouseExistsById = async (warehouseId) => {
  try {
    const result = await warehousesData.getWarehouseById(warehouseId);
    return result ? true : false;
  } catch (error) {
    return null;
  }
}