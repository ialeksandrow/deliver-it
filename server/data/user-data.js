import pool from './pool.js';
import globalData from './global-data.js';


/**
 * Check email exists
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { string } userEmail 
 * @returns { object | false | null }
 */
export const checkEmailExists = async (userEmail) => {
  const sqlQuery =
  `SELECT
    u.*,
    ct.name as cityName,
    co.name as countryName,
    (SELECT COUNT(*) FROM parcels WHERE user_id = u.user_id) as associatedParcels
    FROM users as u
    JOIN cities as ct ON u.city_id = ct.city_id
    JOIN countries as co ON ct.country_id = co.country_id
    WHERE email = ?`;
    
  const sqlQuerySecond = `
    SELECT COUNT(count) as associatedShipments
    FROM (
      SELECT COUNT(*) as count
      FROM parcels as p
      JOIN users as u ON u.user_id = p.user_id
      WHERE u.user_id = ? GROUP BY p.shipment_id) as temp`;

  try {
    const result = await pool.query(sqlQuery, [userEmail]);
    if (result.length) {
      const additionalData = await pool.query(sqlQuerySecond, [result[0].user_id]);
      result[0].associatedShipments = additionalData[0].associatedShipments;
    }
    return result.length > 0 ? result[0] : false;
  } catch (error) {
    return null;
  }
}


/**
 * Check city exists by city Id
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param {*} cityId 
 * @returns { boolean | null }
 */
export const checkCityExists = async (cityId) => {
  try {
    const result = await globalData.getCityById(cityId);
    return result ? true : false;
  } catch (error) {
    return null;
  }
}


/**
 * Create new user in database
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { object } userInfo
 * @returns { object | null }
 */
export const registerUser = async (userInfo) => {
  const sqlQuery = `
    INSERT
    INTO users(email, password, first_name, last_name, address, city_id, role_id)
    VALUES (?, ?, ?, ?, ?, ?, ?)`;

  const args = [
    userInfo.email,
    userInfo.password,
    userInfo.firstName,
    userInfo.lastName,
    userInfo.address,
    userInfo.city,
    userInfo.role_id
  ];

  try {
    return (await pool.query(sqlQuery, args)).insertId;
  } catch (error) {
    return null;
  }
}


/**
 * Add avatar filename to user
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { string } avatar filename
 * @param { number } userId user id number
 * @returns { boolean }
 */
export const setAvatar = async (avatar, userId) => {
  const sqlQuery = `UPDATE users SET avatar = ? WHERE user_id = ?`;
  try {
    await pool.query(sqlQuery, [avatar, userId]);
    return true;
  } catch (error) {
    return null;
  }
}


/**
 * Get user info by ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } userId
 * @returns { object | null }
 */
export const getUserInfo = async (userId, criteria = `WHERE u.user_id = ?`) => {
  const sqlQuery = `
  SELECT
  user_id as userId,
  email,
  first_name as firstName,
  last_name as lastName,
  avatar as avatarURL,
  cs.country_id as countryId,
  cs.name as country,
  ct.city_id as cityId,
  ct.name as city,
  address,
  r.role_id as roleId,
  r.name as role,
  (SELECT COUNT(*) FROM parcels WHERE user_id = u.user_id) as associatedParcels
  ${userId
    ? `,(SELECT COUNT(count) as associatedShipments FROM (SELECT COUNT(*) as count FROM parcels as p JOIN users as u ON u.user_id = p.user_id WHERE u.user_id = ${userId} GROUP BY p.shipment_id) as temp) as associatedShipments`
    : ''}
  FROM users as u
  JOIN cities as ct ON u.city_id = ct.city_id
  JOIN countries as cs ON ct.country_id = cs.country_id
  JOIN roles as r ON u.role_id = r.role_id
  ${criteria}`;

  try {
    return await pool.query(sqlQuery, [userId]);
  } catch (error) {
    return null;
  }
}


/**
 * Get count of customers
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @returns { object | null }
 */
export const getCustomersCount = async () => {
  const sqlQuery = `
    SELECT	
    cs.country_id AS countryId,
    cs.name AS countryName,
    COUNT(*) AS customersCount
    FROM users AS u
    JOIN cities AS ct ON u.city_id = ct.city_id
    JOIN countries AS cs ON ct.country_id = cs.country_id
    JOIN roles AS r ON u.role_id = r.role_id
    WHERE r.name = 'customer'
    GROUP BY cs.country_id`

    try {
      return await pool.query(sqlQuery);
    } catch (error) {
      return null;
    }
}


/**
 * Get user password byt user ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } userId 
 * @returns { object | null }
 */
export const getUserPasswordById = async (userId) => {
  const sqlQuery = `SELECT password FROM users WHERE user_id = ?`;

  try {
    return await pool.query(sqlQuery, [userId]);
  } catch (error) {
    return null;
  }
};


/**
 * Update user password by user ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { string } newPassword
 * @param { number } userId 
 * @returns { object | null }
 */
export const updateUserPasswordById = async (newPassword, userId) => {
  const sqlQuery = `UPDATE users SET password = ? WHERE user_id = ?`;

  try {
    return await pool.query(sqlQuery, [newPassword, userId]);
  } catch (error) {
    return null;
  }
}


/**
 * Update personal info for user by Id
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } userId 
 * @param { object } userDetails 
 * @returns { object | null }
 */
export const updateUserInfoById = async (userId, userDetails) => {
  const sqlQuery = `
    UPDATE users SET
    first_name = ?,
    last_name = ?,
    address = ?,
    city_id = ? ${userDetails.avatar ? ', avatar = ?' : '' }
    WHERE user_id = ?`;

  const args = [
    userDetails.firstName,
    userDetails.lastName,
    userDetails.address,
    userDetails.city,
  ];
  
  if (userDetails.avatar) {
    args.push(userDetails.avatar);
  }

  try {
    return await pool.query(sqlQuery, [...args, userId]);
  } catch (error) {
    return null;
  }
}


/**
 * Delete user by ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } userId 
 * @returns { object | null }
 */
export const deleteUserById = async (userId) => {
  const sqlQuery = `DELETE FROM users WHERE user_id = ?`;

  try {
    return await pool.query(sqlQuery, [userId]);
  } catch (error) {
    return null;
  }
}


/**
 * Get token from black list
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { string } token 
 * @returns { object | null }
 */
export const checkTokenExists = async (token) => {
  const sqlQuery = `SELECT token FROM tokens WHERE token = ?`;

  try {
    return await pool.query(sqlQuery, [token]);
  } catch (error) {
    return null;
  }
}


/**
 * Add token to black list
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { string } token 
 * @returns { object | null }
 */
export const addTokenToBlackList = async (token) => {
  const sqlQuery = `INSERT INTO tokens(token) VALUES (?)`;

  try {
    return await pool.query(sqlQuery, [token]);
  } catch (error) {
    return null;
  }
}


/**
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * Get number of countries
 * @returns { object | null } 
 */
export const getCountriesNumber = async () => {
  const sqlQuery = `SELECT COUNT(*) as countriesCount FROM countries`;
  try {
    return (await pool.query(sqlQuery))[0].countriesCount;
  } catch (error) {
    return null;
  }
}
