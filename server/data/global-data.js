import pool from './pool.js';

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {array}
 */

const getCountries = async () => {
  const sql = `
  SELECT name, (
    SELECT COUNT(city_id)
    FROM cities AS c2
    WHERE c2.country_id = c.country_id
  ) AS citiesCount
  FROM countries AS c;
  `;

  return await pool.query(sql);
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number} id
 * @returns {object|undefined}
 */

const getCountryById = async id => {
  const sql = `
  SELECT name, (
    SELECT COUNT(city_id)
    FROM cities AS c2
    WHERE c2.country_id = c.country_id
  ) AS citiesCount
  FROM countries AS c
  WHERE c.country_id = ?;
  `;

  return (await pool.query(sql, [id]))[0];
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @returns {array}
 */

const getCities = async () => {
  const sql = `
  SELECT
  c.city_id as cityId,
  c.name as name, (
    SELECT COUNT(user_id)
    FROM users AS u
    WHERE u.city_id = c.city_id
  ) AS usersCount, (
    SELECT COUNT(warehouse_id)
    FROM warehouses AS w
    WHERE w.city_id = c.city_id
  ) AS warehousesCount,
  c2.name AS country
  FROM cities AS c
  JOIN countries AS c2 ON c2.country_id = c.country_id;
  `;

  return await pool.query(sql);
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {number} id
 * @returns {object|undefined}
 */

const getCityById = async id => {
  const sql = `
  SELECT name, (
    SELECT COUNT(user_id)
    FROM users AS u
    WHERE u.city_id = c.city_id
  ) AS usersCount, (
    SELECT COUNT(warehouse_id)
    FROM warehouses AS w
    WHERE w.city_id = c.city_id
  ) AS warehousesCount
  FROM cities AS c
  WHERE c.city_id = ?;
  `;

  return (await pool.query(sql, [id]))[0];
};

/**
 * @author Ivan Aleksandrov <ivan.aleksandrov.a29@learn.telerikacademy.com>
 * @param {string} name
 * @param {boolean} [strictSearch=false]
 * @returns {object|array}
 */

const getCityByName = async (name = null, strictSearch = false) => {
  const sql = `
  SELECT name, (
    SELECT COUNT(user_id)
    FROM users AS u
    WHERE u.city_id = c.city_id
  ) AS usersCount, (
    SELECT COUNT(warehouse_id)
    FROM warehouses AS w
    WHERE w.city_id = c.city_id
  ) AS warehousesCount
  FROM cities AS c
  WHERE ${strictSearch ? `c.name = ?` : `c.name LIKE '%${name}%'`};
  `;

  return strictSearch
    ? (await pool.query(sql, [name]))[0]
    : await pool.query(sql, [name]);
};

export default {
  getCountries,
  getCountryById,
  getCities,
  getCityById,
  getCityByName
};
