import pool from './pool.js';

/**
 * Get category by name
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { string } categoryName
 * @returns { object | null } 
 */
const getCategoryByName = async (categoryName) => {
  const sqlQuery = `
    SELECT
    parcel_category_id as categoryId,
    name as categoryName
    FROM parcel_categories
    WHERE name = ? AND is_deleted = 0`;

  try {
    return await pool.query(sqlQuery, [categoryName]);
  } catch (error) {
    return null;
  }
}


/**
 * Get all categories from database
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @returns { object | null }
 */
const AllCategories = async () => {
  const sqlQuery = `SELECT parcel_category_id as categoryId, name as categoryName FROM parcel_categories WHERE is_deleted = 0`;

  try {
    return await pool.query(sqlQuery);
  } catch (error) {
    return null;
  }
}


/**
 * Get single category by ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } categoryId
 * @returns { object | null }
 */
const getCategoryById = async (categoryId) => {
  const sqlQuery = `
    SELECT
    parcel_category_id as categoryId,
    name as categoryName
    FROM parcel_categories
    WHERE parcel_category_id = ? AND is_deleted = 0`;

  try {
    return await pool.query(sqlQuery, [categoryId]);
  } catch (error) {
    return null;
  }
}


/**
 * Create new category and return it
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { string } categoryName 
 * @returns { object | null }
 */
const createNewCategory = async (categoryName) => {
  const sqlQuery = `INSERT INTO parcel_categories(name) VALUES (?)`;

  try {
    const newRecord = await pool.query(sqlQuery, [categoryName]);
    return await getCategoryById(newRecord.insertId);
  } catch (error) {
    return null;
  }
}


/**
 * Update category name by ID
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } categoryId 
 * @param { string } newName 
 * @returns { object | null }
 */
const updateCategoryById = async (categoryId, newName) => {
  const sqlQuery = `UPDATE parcel_categories SET name = ? WHERE parcel_category_id = ?`;

  try {
    await pool.query(sqlQuery, [newName, categoryId]);
    return await getCategoryById(categoryId);
  } catch (error) {
    return null;
  }
}


/**
 * Delete category by Id
 * @author Simeon Mladenov <simeon.mladenov.a29@learn.telerikacademy.com>
 * @param { number } categoryId 
 * @returns { object | null }
 */
const deleteCategoryById = async (categoryId) => {
  const sqlQuery = `UPDATE parcel_categories SET is_deleted = 1 WHERE parcel_category_id = ?`;

  try {
    await pool.query(sqlQuery, [categoryId]);
    return true;
  } catch (error) {
    return null;
  }
}


export default {
  getCategoryByName,
  AllCategories,
  getCategoryById,
  createNewCategory,
  updateCategoryById,
  deleteCategoryById
};