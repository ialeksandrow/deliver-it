import * as validators from '../validators/validate-functions.js'
import validateMessages from '../validators/validate-messages.js'

describe('Email validator', () => {
  test('E-mail can\'t be empty', () => {
    expect(validators.validateEmail()).toBe(validateMessages.invalidEmail);
    expect(validators.validateEmail('      ')).toBe(validateMessages.invalidEmail);
    expect(validators.validateEmail(null)).toBe(validateMessages.invalidEmail);
    expect(validators.validateEmail(undefined)).toBe(validateMessages.invalidEmail);
  });
  
  test('E-mail can\'t include special symbols or spaces', () => {
    expect(validators.validateEmail('tem^p@temp.com')).toBe(validateMessages.invalidEmail);
    expect(validators.validateEmail('temp@tem@p.com')).toBe(validateMessages.invalidEmail);
    expect(validators.validateEmail('temp @temp.com')).toBe(validateMessages.invalidEmail);
  });

  test('E-mail can\'t be short from 8 symbols', () => {
    expect(validators.validateEmail('t@t.com')).toBe(validateMessages.invalidEmail);
  });

  test('E-mail must be string', () => {
    expect(validators.validateEmail(1010)).toBe(validateMessages.invalidEmail);
  });

  test('E-mail must be valid', () => {
    expect(validators.validateEmail('temp@temp.com')).toBe(true)
    expect(validators.validateEmail('temp_temp.com')).toBe(validateMessages.invalidEmail)
  });

  test('E-mail must be in range 8-200 symbols', () => {
    expect(validators.validateEmail('ta@t.com')).toBe(true);
  });
});


describe('First name validator', () => {
  test('First name can\'t be empty', () => {
    expect(validators.validateFirstName('')).toBe(validateMessages.invalidFirstName);
    expect(validators.validateFirstName('      ')).toBe(validateMessages.invalidFirstName);
  });

  test('First name must include only latin or cyrillic symbols', () => {
    expect(validators.validateFirstName('Иван Иванов')).toBe(validateMessages.invalidFirstName);
    expect(validators.validateFirstName('Иван_Иванов')).toBe(validateMessages.invalidFirstName);
    expect(validators.validateFirstName('J0hn')).toBe(validateMessages.invalidFirstName);
    expect(validators.validateFirstName('Петър')).toBe(true);
    expect(validators.validateFirstName('John')).toBe(true);
  });

  test('First name must be string', () => {
    expect(validators.validateFirstName(123456789)).toBe(validateMessages.invalidFirstName);
    expect(validators.validateFirstName(['Петър'])).toBe(validateMessages.invalidFirstName);
  });
});


describe('Last name validator', () => {
  test('Last name can\'t be empty', () => {
    expect(validators.validateLastName('')).toBe(validateMessages.invalidLastName);
    expect(validators.validateLastName('      ')).toBe(validateMessages.invalidLastName);
  });

  test('Last name must include only latin or cyrillic symbols', () => {
    expect(validators.validateLastName('Иван Иванов')).toBe(validateMessages.invalidLastName);
    expect(validators.validateLastName('Иван_Иванов')).toBe(validateMessages.invalidLastName);
    expect(validators.validateLastName('Sm1th')).toBe(validateMessages.invalidLastName);
    expect(validators.validateLastName('Smith')).toBe(true);
    expect(validators.validateLastName('Иванов')).toBe(true);
  });

  test('Last name must be string', () => {
    expect(validators.validateLastName(123456789)).toBe(validateMessages.invalidLastName);
    expect(validators.validateLastName(['Иванов'])).toBe(validateMessages.invalidLastName);
  });
});


describe('Address validator', () => {
  test('Address can\'t be empty', () => {
    expect(validators.validateAddress('')).toBe(validateMessages.invalidAddress);
    expect(validators.validateAddress('      ')).toBe(validateMessages.invalidAddress);
  });

  test('Address name must include only latin, cyrillic symbols, numbers, spaces or . (dot)', () => {
    expect(validators.validateAddress('ул. Кокиче_12')).toBe(validateMessages.invalidAddress);
    expect(validators.validateAddress('ул. Кокиче №12')).toBe(validateMessages.invalidAddress);
    expect(validators.validateAddress('ул. Кокиче 12')).toBe(true);
    expect(validators.validateAddress('13 Roscoe Str.')).toBe(true);
  });

  test('Address must be string', () => {
    expect(validators.validateAddress(123456789)).toBe(validateMessages.invalidAddress);
    expect(validators.validateAddress(['13 Roscoe Str.'])).toBe(validateMessages.invalidAddress);
  });
});


describe('Country name validator', () => {
  test('Country name can\'t be empty', () => {
    expect(validators.validateCountry('')).toBe(validateMessages.invalidCountry);
    expect(validators.validateCountry('      ')).toBe(validateMessages.invalidCountry);
  });

  test('Country name must include only latin symbols', () => {
    expect(validators.validateCountry('R0mania')).toBe(validateMessages.invalidCountry);
    expect(validators.validateCountry('България')).toBe(validateMessages.invalidCountry);
    expect(validators.validateCountry('Bulgaria')).toBe(true);
  });

  test('Country name must be string', () => {
    expect(validators.validateCountry(123456789)).toBe(validateMessages.invalidCountry);
    expect(validators.validateCountry(['Bulgaria'])).toBe(validateMessages.invalidCountry);
  });
});


describe('City name validator', () => {
  test('City name can\'t be empty', () => {
    expect(validators.validateCity('')).toBe(validateMessages.invalidCity);
    expect(validators.validateCity('      ')).toBe(validateMessages.invalidCity);
  });

  test('City name must include only latin symbols', () => {
    expect(validators.validateCity('P1even')).toBe(validateMessages.invalidCity);
    expect(validators.validateCity('Плевен')).toBe(validateMessages.invalidCity);
    expect(validators.validateCity('Pleven ')).toBe(true);
  });

  test('City name must be string', () => {
    expect(validators.validateCity(123456789)).toBe(validateMessages.invalidCity);
    expect(validators.validateCity(['Pleven'])).toBe(validateMessages.invalidCity);
  });
});


describe('Password validator', () => {
  test('Password can\'t be empty', () => {
    expect(validators.validatePassword('')).toBe(validateMessages.invalidPassword);
    expect(validators.validatePassword('      ')).toBe(validateMessages.invalidPassword);
  });

  test('Password must be minimum 6 symbols', () => {
    expect(validators.validatePassword('Pass')).toBe(validateMessages.invalidPassword);
    expect(validators.validatePassword('myPassword')).toBe(true);
  });

  test('Password must include only latin symbols, numbers or _', () => {
    expect(validators.validatePassword('Pas$word')).toBe(validateMessages.invalidPassword);
    expect(validators.validatePassword('P as$word')).toBe(validateMessages.invalidPassword);
    expect(validators.validatePassword('PassW0rd   ')).toBe(true);
    expect(validators.validatePassword('P4ss_W0rd')).toBe(true);
  });

  test('Password must be string', () => {
    expect(validators.validatePassword(123456789)).toBe(validateMessages.invalidPassword);
    expect(validators.validatePassword(null)).toBe(validateMessages.invalidPassword);
  });
});


describe('Weight validator', () => {
  test('Weight can\'t be empty', () => {
    expect(validators.validateWeight('')).toBe(validateMessages.invalidWeight);
    expect(validators.validateWeight('      ')).toBe(validateMessages.invalidWeight);
  });

  test('Weight myst be valid number', () => {
    expect(validators.validateWeight('I9')).toBe(validateMessages.invalidWeight);
    expect(validators.validateWeight('0.2')).toBe(true);
    expect(validators.validateWeight(3)).toBe(true);
  });

  test('Weight myst be bigger from zero', () => {
    expect(validators.validateWeight('0')).toBe(validateMessages.invalidWeight);
    expect(validators.validateWeight(-1)).toBe(validateMessages.invalidWeight);
    expect(validators.validateWeight(4)).toBe(true);
  });
});


describe('Latitude validator', () => {
  test('Latitude can\'t be empty', () => {
    expect(validators.validateLatitude('')).toBe(validateMessages.invalidLatitude);
    expect(validators.validateLatitude('      ')).toBe(validateMessages.invalidLatitude);
  });

  test('Latitude myst be valid number', () => {
    expect(validators.validateLatitude('I9')).toBe(validateMessages.invalidLatitude);
    expect(validators.validateLatitude('0.24344')).toBe(true);
    expect(validators.validateLatitude(3.7883)).toBe(true);
  });

  test('Latitude must be in range -90 - +90', () => {
    expect(validators.validateLatitude('100')).toBe(validateMessages.invalidLatitude);
    expect(validators.validateLatitude(-91)).toBe(validateMessages.invalidLatitude);
    expect(validators.validateLatitude(4)).toBe(true);
  });
});


describe('Longitude validator', () => {
  test('Longitude can\'t be empty', () => {
    expect(validators.validateLongitude('')).toBe(validateMessages.invalidLongitude);
    expect(validators.validateLongitude('      ')).toBe(validateMessages.invalidLongitude);
  });

  test('Longitude myst be valid number', () => {
    expect(validators.validateLongitude('I9')).toBe(validateMessages.invalidLongitude);
    expect(validators.validateLongitude('0.24344')).toBe(true);
    expect(validators.validateLongitude(3.7883)).toBe(true);
  });

  test('Longitude must be in range -180 - +180', () => {
    expect(validators.validateLongitude('181')).toBe(validateMessages.invalidLongitude);
    expect(validators.validateLongitude(-201)).toBe(validateMessages.invalidLongitude);
    expect(validators.validateLongitude(180)).toBe(true);
  });
});


describe('Date validator', () => {
  test('Date can\'t be empty', () => {
    expect(validators.validateDate('')).toBe(validateMessages.invalidDate);
    expect(validators.validateDate('      ')).toBe(validateMessages.invalidDate);
    expect(validators.validateDate(null)).toBe(validateMessages.invalidDate);
  });

  test('Date must be valid', () => {
    expect(validators.validateDate('123sep9870')).toBe(validateMessages.invalidDate);
    expect(validators.validateDate('2000-15-28')).toBe(validateMessages.invalidDate);
    expect(validators.validateDate('2021-10-10')).toBe(true);
    expect(validators.validateDate('2021/1/13')).toBe(true);
    expect(validators.validateDate('12sep2020')).toBe(true);
  });
});

describe('ID validator', () => {
  test('ID can\'t be empty', () => {
    expect(validators.validateNumber('')).toBe(validateMessages.invalidNumber);
    expect(validators.validateNumber('      ')).toBe(validateMessages.invalidNumber);
    expect(validators.validateNumber(null)).toBe(validateMessages.invalidNumber);
  });

  test('ID must be integer number', () => {
    expect(validators.validateNumber('test')).toBe(validateMessages.invalidNumber);
    expect(validators.validateNumber(1.2)).toBe(validateMessages.invalidNumber);
    expect(validators.validateNumber('13')).toBe(true);
    expect(validators.validateNumber(187)).toBe(true);
  });

  test('ID must be bigger from zero', () => {
    expect(validators.validateNumber('-2')).toBe(validateMessages.invalidNumber);
    expect(validators.validateNumber(187)).toBe(true);
  });
});