import { objectKeysToLowerCase } from '../common/help-functions.js'

describe('objectKeysToLowerCase', () => {
  const testObject = { One: 1, TWO: 2, tHREe: 3};
  const doublesObject = { One: 1, one: 4, TWO: 2, tHREe: 3};
  const emptyKeyObject = { One: 1, one: null, TWO: 2, tHREe: 3, four: ''};

  const compareObject = { one: 1, two: 2, three: 3};

  test('Expect all keys to be in lowerCase', () => {
    expect(objectKeysToLowerCase(testObject)).toMatchObject(compareObject);
  });

  test('Expect remove duplicates', () => {
    expect(objectKeysToLowerCase(doublesObject)).toMatchObject(compareObject);
  });

  test('Expect remove keys without values', () => {
    expect(objectKeysToLowerCase(emptyKeyObject)).toMatchObject(compareObject);
  });
});