# DeliverIt

## Table of Contents

- [Description](#description)
- [Project Information](#project-information)
- [Back-End Setup](#back-end-setup)
  - [Prerequisites](#prerequisites)
  - [.env Configuration](#.env-configuration)
  - [Database Setup](#database-setup)
  - [First Time Setup and Available Scripts](#first-time-setup-and-available-scripts)
- [Front-End Setup](#front-end-setup)
- [Project Structure](#project-structure)
- [Postman Workflow and Postman Collections](#postman-workflow-and-postman-collections)
- [Demo Showcase](#demo-showcase)

### Description

Freight Forwarding Management System to easily control Your supply chain and logistics scenarious. Ship and track anything, anywhere with our versatile variety of parcel types, international warehouse locations and real-time shipment tracking.

### Project Information

- Language and version: JavaScript ES2020
- Platform and version: Node v14.0+

#### Core Back-End Technologies

- NodeJS
- Express
- MariaDB

#### Core Front-End Technologies

- React
- Material UI

#### Authors

- Ivan Aleksandrov @ialeksandrow
- Simeon Mladenov @Sea_Flanker

### Back-End Setup

#### Prerequisites

- [NodeJS](https://nodejs.org/en/download)
- [MariaDB](https://mariadb.org/download)
- [Git](https://git-scm.com/download)

After successfull install of everything mentioned in the links above, set up Your `Git` credentials and clone the project repository.

> For the purposes of this documentation, we are going to use Git Bash as a default terminal.

``` bash
#SSH
git clone git@gitlab.com:ialeksandrow/deliver-it.git

#HTTPS
git clone https://gitlab.com/ialeksandrow/deliver-it.git
```

- [Visual Studio Code (SSH)](vscode://vscode.git/clone?url=git%40gitlab.com%3AMarshmallow21%2Fridepal-playlist-generator.git)
- [Visual Studio Code (HTTPS)](vscode://vscode.git/clone?url=https%3A%2F%2Fgitlab.com%2FMarshmallow21%2Fridepal-playlist-generator.git)

#### .env Configuration

The project requires creation and configuration of a `.env` file in order to establish a connection to Your local database.

- Simply run the following command in the terminal of the cloned repository folder:

``` bash
cd ./deliver-it/server && touch .env
```

- Manual configuration of the `.env` file is required. Copy the options given below and change `DB_USER` and `DB_PASS` to your personal ones.

``` bash
EXPRESS_PORT=5000
DB_HOST=localhost
DB_PORT=3306
DB_NAME=wilson
DB_USER=?
DB_PASS=?
DB_LIMIT=10
SECRET_KEY=W1LS0N
SERVER_URL=localhost
TOKEN_LIFE=24h
FILE_MAX_SIZE=1024
FILES_AVAILABLE=jpg, png, gif
AVATARS_FOLDER=avatars
```

| ENVIRONMENT VARIABLE | VALUE | DESCRIPTION |
| -------------------- | ----- | ----------- |
| EXPRESS_PORT | 5000 | Server Port |
| DB_HOST | localhost | Database Host |
| DB_PORT | 3306 | Database Port |
| DB_NAME | wilson | Database Name |
| DB_USER | ? | Database Username |
| DB_PASS | ? | Database Password |
| DB_LIMIT | 10 | Database Connection Limit |
| SECRET_KEY | W1LS0N | JWT Secret Key |
| SERVER_URL | localhost | Server URL |
| TOKEN_LIFE | 24h | Token Expiration Duration |
| FILE_MAX_SIZE | 1024 | Maximum Allowed Avatar Size |
| FILES_AVAILABLE | jpg, png, gif | Allowed Avatar File Formats |
| AVATARS_FOLDER | avatars | Avatar Storage Location Folder |

#### Database Setup

Setup of Your local database can be done in two ways:

- By using Your preferred SQL workbench and importing the files from `./deliver-it/database` folder. The order of import should be as follows:
  1. `build-database.sql`
  2. `dump-database.sql`

- By using our intuitive script. Simply run the following command in the terminal:

``` bash
cd ./deliver-it/server && npm run build:db
```

> To get more used to the database, You can check the following diagram:

![database diagram](./database/diagram/db-diagram.png)

#### First Time Setup and Available Scripts

- Navigate to the `server` folder and run the following command in the terminal:

``` bash
# Takes care of all the necessary dependencies
npm install
```

``` bash
# Runs Express with Nodemon
npm run start:dev
```

- After successfull install of the required dependencies, You will be able to run the following scripts:
  - `start` - runs the current state of the code. Real-time changes do not reflect automatically.
  - `start:dev` - runs the current state of the code. Takes care of all real-time changes and visualizes them.
  - `build:db` - creates the database, its tables and inserts mockup data automatically.
  - `lint` - checks for linting errors in all `.js` files.
  - `test` - runs tests with `Jest`.

### Front-End Setup

- Navigate to the `client` folder and run the following command in the terminal:

``` bash
# Takes care of all the necessary dependencies
npm install
```

``` bash
# Runs React
npm start
```

- After successfull install of the required dependencies, You will be able to run the following scripts:
  - [React Scripts](https://create-react-app.dev/docs/available-scripts)

### Project Structure

#### Back-End Structure

<details>
  <summary>Click to expose structure</summary>

  ```
  server
  ┣ authentication (contains authentication logic)
  ┃ ┣ create-token.js
  ┃ ┗ strategy.js
  ┣ common (contains enumerables and helper files)
  ┃ ┣ color-log.js
  ┃ ┣ column-aliases.js
  ┃ ┣ delivery-types.js
  ┃ ┣ errors.js
  ┃ ┣ help-functions.js
  ┃ ┣ middleware-constants.js
  ┃ ┣ parcel-statuses.js
  ┃ ┣ shipment-statuses.js
  ┃ ┗ user-roles.js
  ┣ controllers (contains controller layer logic)
  ┃ ┣ categories-controller.js
  ┃ ┣ global-controller.js
  ┃ ┣ parcels-controller.js
  ┃ ┣ shipments-controller.js
  ┃ ┣ user-controller.js
  ┃ ┗ warehouses-controller.js
  ┣ data (contains data layer logic)
  ┃ ┣ categories-data.js
  ┃ ┣ global-data.js
  ┃ ┣ parcels-data.js
  ┃ ┣ pool.js
  ┃ ┣ shipments-data.js
  ┃ ┣ user-data.js
  ┃ ┗ warehouses-data.js
  ┣ database (contains database script file)
  ┃ ┗ database-initialization.js
  ┣ middlewares (contains custom middlewares)
  ┃ ┣ api-version.js
  ┃ ┣ authenticate-user.js
  ┃ ┣ body-validator.js
  ┃ ┣ check-shipment-exists.js
  ┃ ┣ employee-only.js
  ┃ ┣ file-validator.js
  ┃ ┣ ids-checker.js
  ┃ ┣ owner-employee.js
  ┃ ┣ params-validator.js
  ┃ ┣ parcel-permission.js
  ┃ ┣ query-validator.js
  ┃ ┗ token-validator.js
  ┣ services (contains service layer logic)
  ┃ ┣ categories-services.js
  ┃ ┣ global-service.js
  ┃ ┣ parcels-services.js
  ┃ ┣ shipments-service.js
  ┃ ┣ user-services.js
  ┃ ┗ warehouses-service.js
  ┣ tests (contains logic tests)
  ┃ ┣ test-help-functions.spec.js
  ┃ ┗ test-validators.spec.js
  ┣ validators (contains request body/query validation logic)
  ┃ ┣ category-validator.js
  ┃ ┣ login-validator.js
  ┃ ┣ new-parcel-validator.js
  ┃ ┣ new-password-validator.js
  ┃ ┣ parcel-to-shipment-validator.js
  ┃ ┣ parcels-search-validator.js
  ┃ ┣ register-validator.js
  ┃ ┣ search-keyword-validator.js
  ┃ ┣ search-user-validator.js
  ┃ ┣ shipment.schema.js
  ┃ ┣ update-status-validator.js
  ┃ ┣ update-user-validator.js
  ┃ ┣ validate-functions.js
  ┃ ┣ validate-messages.js
  ┃ ┗ warehouse.schema.js
  ┣  jest.config.js
  ┣ .babelrc
  ┣ .env
  ┣ .eslintrc.json
  ┣ app.js (entry point)
  ┣ config.js
  ┣ package-lock.json
  ┗ package.json
  ```
</details>

#### Front-End Structure

<details>
  <summary>Click to expose structure</summary>
  
  ```
  client
  ┣ public
  ┃ ┗ index.html
  ┣ src
  ┃ ┣ api (contains fetch requests to api)
  ┃ ┃ ┣ categories-requests.js
  ┃ ┃ ┣ global-requests.js
  ┃ ┃ ┣ headers-functions.js
  ┃ ┃ ┣ parcel-requests.js
  ┃ ┃ ┣ user-requests.js
  ┃ ┃ ┗ warehouse-request.js
  ┃ ┣ common (contains enumerables)
  ┃ ┃ ┣ constants.js
  ┃ ┃ ┣ delivery-types.js
  ┃ ┃ ┣ filters.js
  ┃ ┃ ┣ navigation-links.js
  ┃ ┃ ┣ parcel-statuses.js
  ┃ ┃ ┣ router-paths.js
  ┃ ┃ ┣ shipment-statuses.js
  ┃ ┃ ┣ sort-functions.js
  ┃ ┃ ┣ user-roles.js
  ┃ ┃ ┗ validators.js
  ┃ ┣ components (contains separate components, mainly used in different page views)
  ┃ ┃ ┣ AuthenticationBlock
  ┃ ┃ ┃ ┣ AuthenticationBlock.jsx
  ┃ ┃ ┃ ┗ AuthenticationBlock.styles.js
  ┃ ┃ ┣ Brand
  ┃ ┃ ┃ ┣ Brand.jsx
  ┃ ┃ ┃ ┗ Brand.styles.js
  ┃ ┃ ┣ ChangePasswordDialog
  ┃ ┃ ┃ ┣ ChangePasswordDialog.jsx
  ┃ ┃ ┃ ┗ ChangePasswordDialog.styles.js
  ┃ ┃ ┣ CreateParcel
  ┃ ┃ ┃ ┣ CreateParcel.jsx
  ┃ ┃ ┃ ┗ CreateParcel.styles.js
  ┃ ┃ ┣ FreightTypes
  ┃ ┃ ┃ ┣ FreightTypes.jsx
  ┃ ┃ ┃ ┗ FreightTypes.styles.js
  ┃ ┃ ┣ Header
  ┃ ┃ ┃ ┣ Header.jsx
  ┃ ┃ ┃ ┗ Header.styles.js
  ┃ ┃ ┣ HeaderSearch
  ┃ ┃ ┃ ┣ HeaderSearch.jsx
  ┃ ┃ ┃ ┗ HeaderSearch.styles.js
  ┃ ┃ ┣ InfoDialog
  ┃ ┃ ┃ ┣ InfoDialog.jsx
  ┃ ┃ ┃ ┗ InfoDialog.styles.js
  ┃ ┃ ┣ LogOutButton
  ┃ ┃ ┃ ┗ LogOutButton.jsx
  ┃ ┃ ┣ Navigation
  ┃ ┃ ┃ ┣ Navigation.jsx
  ┃ ┃ ┃ ┣ Navigation.styles.js
  ┃ ┃ ┃ ┗ NavigationLink.jsx
  ┃ ┃ ┣ ParcelOperatorView
  ┃ ┃ ┃ ┣ ParcelOperatorView.jsx
  ┃ ┃ ┃ ┣ ParcelOperatorView.styles.js
  ┃ ┃ ┃ ┗ ParcelTableRow.jsx
  ┃ ┃ ┣ ParcelUserView
  ┃ ┃ ┃ ┣ ParcelTableUserRow.jsx
  ┃ ┃ ┃ ┣ ParcelUserView.jsx
  ┃ ┃ ┃ ┗ ParcelUserView.styles.js
  ┃ ┃ ┣ ParcelView
  ┃ ┃ ┃ ┣ ParcelView.jsx
  ┃ ┃ ┃ ┗ ParcelView.styles.js
  ┃ ┃ ┣ SectionHalfImage
  ┃ ┃ ┃ ┣ SectionHalfImage.jsx
  ┃ ┃ ┃ ┗ SectionHalfImage.styles.js
  ┃ ┃ ┣ ShipmentDocument
  ┃ ┃ ┃ ┣ ShipmentDocument.jsx
  ┃ ┃ ┃ ┗ ShipmentDocument.styles.js
  ┃ ┃ ┣ ShipmentsEdit
  ┃ ┃ ┃ ┣ ShipmentsEdit.jsx
  ┃ ┃ ┃ ┗ ShipmentsEdit.styles.js
  ┃ ┃ ┣ ShipmentsParcelManagement
  ┃ ┃ ┃ ┣ ShipmentsParcelManagement.jsx
  ┃ ┃ ┃ ┗ ShipmentsParcelManagement.styles.js
  ┃ ┃ ┣ ShipmentsView
  ┃ ┃ ┃ ┣ ShipmentsView.jsx
  ┃ ┃ ┃ ┗ ShipmentsView.styles.js
  ┃ ┃ ┣ ShowCategories
  ┃ ┃ ┃ ┣ ShowCategories.jsx
  ┃ ┃ ┃ ┗ ShowCategories.styles.js
  ┃ ┃ ┣ TrackShipment
  ┃ ┃ ┃ ┣ TrackShipment.jsx
  ┃ ┃ ┃ ┗ TrackShipment.styles.js
  ┃ ┃ ┣ UserAvatar
  ┃ ┃ ┃ ┣ UserAvatar.jsx
  ┃ ┃ ┃ ┗ UserAvatar.styles.js
  ┃ ┃ ┣ UserRegister
  ┃ ┃ ┃ ┣ UserRegister.jsx
  ┃ ┃ ┃ ┗ UserRegister.styles.js
  ┃ ┃ ┗ WarehousesMap
  ┃ ┃ ┃ ┗ WarehousesMap.jsx
  ┃ ┣ contexts (contains authentication context)
  ┃ ┃ ┗ AuthenticationContext.js
  ┃ ┣ providers (contains global theme style)
  ┃ ┃ ┗ Theme.js
  ┃ ┣ utils (contains helper files)
  ┃ ┃ ┣ token-decoder.js
  ┃ ┃ ┗ use-design.styles.js
  ┃ ┣ views (contains end user page views)
  ┃ ┃ ┣ Homepage
  ┃ ┃ ┃ ┣ Homepage.jsx
  ┃ ┃ ┃ ┗ Homepage.styles.js
  ┃ ┃ ┣ NewParcel
  ┃ ┃ ┃ ┗ NewParcel.jsx
  ┃ ┃ ┣ ParcelDetails
  ┃ ┃ ┃ ┗ ParcelDetails.jsx
  ┃ ┃ ┣ Parcels
  ┃ ┃ ┃ ┣ Parcels.jsx
  ┃ ┃ ┃ ┗ Parcels.styles.js
  ┃ ┃ ┣ Shipments
  ┃ ┃ ┃ ┣ Shipments.jsx
  ┃ ┃ ┃ ┗ Shipments.styles.js
  ┃ ┃ ┣ SignIn
  ┃ ┃ ┃ ┣ SignIn.jsx
  ┃ ┃ ┃ ┗ SignIn.styles.js
  ┃ ┃ ┣ SignUp
  ┃ ┃ ┃ ┣ SignUp.jsx
  ┃ ┃ ┃ ┗ SignUp.styles.js
  ┃ ┃ ┣ UserProfile
  ┃ ┃ ┃ ┣ UserProfile.jsx
  ┃ ┃ ┃ ┗ UserProfile.styles.js
  ┃ ┃ ┗ UsersList
  ┃ ┃ ┃ ┣ UsersList.jsx
  ┃ ┃ ┃ ┗ UsersList.styles.js
  ┃ ┣ App.jsx (entry point)
  ┃ ┗ index.js
  ┣ package-lock.json
  ┗ package.json
  ```
</details>

### Postman Workflow and Postman Collections

- Download and install [Postman](https://www.postman.com/downloads/).
- Click on the shortcut icon in order to open the application.
- Click the `Import` button and select the `.json` collection files, located in `./deliver-it/postman` of the cloned repository folder.

### Demo Showcase

<details>
  <summary>Click to expose showcase</summary>

  ![homepage view](./screenshots/homepage.png)
  ![sign-in view](./screenshots/sign-in.png)
  ![sign-up view](./screenshots/sign-up.png)
  ![parcels customer view](./screenshots/parcels-customer.png)
  ![parcels operator view](./screenshots/parcels-operator.png)
  ![parcel create view](./screenshots/parcel-create.png)
  ![parcel edit view](./screenshots/parcel-edit.png)
  ![shipments operator view](./screenshots/shipments-operator.png)
  ![shipments edit operator view](./screenshots/shipments-edit-operator.png)
  ![shipment status view](./screenshots/shipment-status.png)
  ![user profile view](./screenshots/user-profile.png)
  ![users list view](./screenshots/users-list.png)
</details>
